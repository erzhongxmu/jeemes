package org.jeecg.modules.mes.storage.service;

import org.jeecg.modules.mes.storage.entity.MesShelfIpConfig;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: mes_shelf_ip_config
 * @Author: jeecg-boot
 * @Date:   2021-04-22
 * @Version: V1.0
 */
public interface IMesShelfIpConfigService extends IService<MesShelfIpConfig> {

}
