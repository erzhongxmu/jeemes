package org.jeecg.modules.mes.storage.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import org.jeecg.common.api.vo.Result;
import org.jeecg.modules.mes.storage.entity.MesStorageWholesale;
import org.jeecg.modules.mes.storage.vo.ReportVo;
import org.jeecg.modules.mes.storage.vo.YclJxcReportVO;
import org.jeecg.modules.mes.storage.vo.YclRkdReport;
import org.jeecg.modules.mes.storage.vo.YclRkdReportVO;

import java.util.List;

/**
 * @Description: 仓库管理—批号交易
 * @Author: jeecg-boot
 * @Date: 2020-09-14
 * @Version: V1.0
 */
public interface IMesStorageWholesaleService extends IService<MesStorageWholesale> {

    /**
     * 通过id查询追踪记录
     *
     * @param id
     * @return
     */
    List<MesStorageWholesale> findByIdTraceBack(String id);

    /**
     * 批量扫描入库
     *
     * @param mesStorageWholesale
     * @return
     */
    public boolean addScanStoreAll(MesStorageWholesale mesStorageWholesale);

    /**
     * 扫描入库
     *
     * @param mesStorageWholesale
     * @return
     */
    public boolean addScanStore(MesStorageWholesale mesStorageWholesale);

    /**
     * 修改收货状态和未收货数量
     *
     * @param unreceiveNum 未收货数量
     * @param temId        采购订单子表id
     * @return
     */
    public boolean updateNumStatePurchaseItemId(String unreceiveNum, String temId);

    /**
     * 修改未收货数量
     *
     * @param unreceiveNum 未收货数量
     * @param temId        采购订单子表id
     * @return
     */
    public boolean updateNumPurchaseItemId(String unreceiveNum, String temId);

    /**
     * app扫描发料
     *
     * @param mesStorageWholesale
     * @return
     */
    public Result<?> scanSendMateriel(MesStorageWholesale mesStorageWholesale);

    /**
     * app扫描上料
     *
     * @param mesStorageWholesale
     * @return
     */
    public void addUnglaze(MesStorageWholesale mesStorageWholesale);

    /**
     * app 扫描上料
     *
     * @param id        批号交易id,二维码id
     * @param commandId 制令单id
     * @param prekId    物料凭证id
     * @param passage   通道
     * @param feeder    菲达
     * @return
     */
    public MesStorageWholesale addUnglaze(String id, String commandId, String prekId, String passage, String feeder);

    /**
     * app 扫描收货
     *
     * @param id         批号交易id,二维码id
     * @param baseCode   采购订单子表id
     * @param baseRownum 物料凭证项目id
     * @return
     */
    public MesStorageWholesale addTakeDelivery(String id, String baseCode, String baseRownum);

    /**
     * app 一键收货
     *
     * @param baseCode   采购订单子表id
     * @param baseRownum 物料凭证项目id
     * @return
     */
    public void oneTakeDelivery(String baseCode, String baseRownum);

    /**
     * 根据query4查询该制令单的上料记录 并根据原有上料记录去转产数量记录 远程调用
     *
     * @param query4   原制令单id
     * @param synum    已使用数量
     * @param mcode    物料料号
     * @param basecode 转产的制令单bom的id
     * @param newid    转产的制令单id
     * @return
     */
    public String updateCommandbill(String query4, String synum, String mcode, String basecode, String newid);

    public List<MesStorageWholesale> selectQuery4SL(String query4);

    //原材料入库单
    public Page<ReportVo> selectYclRkd(Page<ReportVo> page, String begindate, List<String> ids, String attr1);

    //成品入库单
    public Page<ReportVo> selectCpRkd(Page<ReportVo> page, String begindate, List<String> attr1, String attr2, String attr4);

    //成品出库单
    public Page<ReportVo> selectCpChd(Page<ReportVo> page, String begindate,  List<String> attr1, String attr2, String attr4);

    //生产领料单
    public Page<ReportVo> selectLld(Page<ReportVo> page, String commadid);

    //退料单
    public Page<ReportVo> selectTld(Page<ReportVo> page, String commadid);

    //原材料进销存
    public Page<ReportVo> selectYclJxc(Page<ReportVo> page, String materid, String areaCode, String locationCode);

    //导出原材料进销存
    public List<ReportVo> exportXlsYclJxc(YclJxcReportVO vo);

    //原材料流水报表
    public Page<ReportVo> selectLsbb(Page<ReportVo> page, String materid, String areaCode, String locationCode);

    //成品进销存
    public Page<ReportVo> selectCpJxc(Page<ReportVo> page, String materid, String areaCode, String locationCode);

    Result<?> findSplitBoard(String id);

    Result<?> checkStockInById(String id);

    //入库单报表导出
    List<YclRkdReport> exportYclRkd(YclRkdReportVO vo);

    /**
     * 查询该采购子表id的收货数量
     *
     * @param id
     * @return
     */
    public int queryPurchaseItemSum(String id);

    /**
     * 查询出还未收货的已打印物料的列表
     *
     * @param baseCode 采购订单子表id
     * @return
     */
    public List<MesStorageWholesale> selectPrintUnreceivelist(String baseCode);

    /**
     * 打印了还未收货的数量
     *
     * @param baseCode 采购订单子表id
     * @return
     */
    public int queryPrintUnreceiveNum(String baseCode);

    String getSumByInWareNum(String id);

    /**
     * 获取扫描上料记录 根据旧料盘id查询
     */
    public List<MesStorageWholesale> getshangliao(String id);
}
