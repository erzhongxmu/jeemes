package org.jeecg.modules.mes.storage.controller;

import java.util.Arrays;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.modules.mes.storage.entity.MesErrorOperationRecord;
import org.jeecg.modules.mes.storage.service.IMesErrorOperationRecordService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

 /**
 * @Description: 错误操作记录
 * @Author: jeecg-boot
 * @Date:   2021-05-31
 * @Version: V1.0
 */
@Api(tags="错误操作记录")
@RestController
@RequestMapping("/storage/mesErrorOperationRecord")
@Slf4j
public class MesErrorOperationRecordController extends JeecgController<MesErrorOperationRecord, IMesErrorOperationRecordService> {
	@Autowired
	private IMesErrorOperationRecordService mesErrorOperationRecordService;
	
	/**
	 * 分页列表查询
	 *
	 * @param mesErrorOperationRecord
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "错误操作记录-分页列表查询")
	@ApiOperation(value="错误操作记录-分页列表查询", notes="错误操作记录-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(MesErrorOperationRecord mesErrorOperationRecord,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<MesErrorOperationRecord> queryWrapper = QueryGenerator.initQueryWrapper(mesErrorOperationRecord, req.getParameterMap());
		Page<MesErrorOperationRecord> page = new Page<MesErrorOperationRecord>(pageNo, pageSize);
		IPage<MesErrorOperationRecord> pageList = mesErrorOperationRecordService.page(page, queryWrapper);
		return Result.ok(pageList);
	}
	
	/**
	 *   添加
	 *
	 * @param mesErrorOperationRecord
	 * @return
	 */
	@AutoLog(value = "错误操作记录-添加")
	@ApiOperation(value="错误操作记录-添加", notes="错误操作记录-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody MesErrorOperationRecord mesErrorOperationRecord) {
		return mesErrorOperationRecordService.add(mesErrorOperationRecord);
	}
	
	/**
	 *  编辑
	 *
	 * @param mesErrorOperationRecord
	 * @return
	 */
	@AutoLog(value = "错误操作记录-编辑")
	@ApiOperation(value="错误操作记录-编辑", notes="错误操作记录-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody MesErrorOperationRecord mesErrorOperationRecord) {
		mesErrorOperationRecordService.updateById(mesErrorOperationRecord);
		return Result.ok("编辑成功!");
	}
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "错误操作记录-通过id删除")
	@ApiOperation(value="错误操作记录-通过id删除", notes="错误操作记录-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		mesErrorOperationRecordService.removeById(id);
		return Result.ok("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "错误操作记录-批量删除")
	@ApiOperation(value="错误操作记录-批量删除", notes="错误操作记录-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.mesErrorOperationRecordService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.ok("批量删除成功!");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "错误操作记录-通过id查询")
	@ApiOperation(value="错误操作记录-通过id查询", notes="错误操作记录-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		MesErrorOperationRecord mesErrorOperationRecord = mesErrorOperationRecordService.getById(id);
		if(mesErrorOperationRecord==null) {
			return Result.error("未找到对应数据");
		}
		return Result.ok(mesErrorOperationRecord);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param mesErrorOperationRecord
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, MesErrorOperationRecord mesErrorOperationRecord) {
        return super.exportXls(request, mesErrorOperationRecord, MesErrorOperationRecord.class, "错误操作记录");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, MesErrorOperationRecord.class);
    }

}
