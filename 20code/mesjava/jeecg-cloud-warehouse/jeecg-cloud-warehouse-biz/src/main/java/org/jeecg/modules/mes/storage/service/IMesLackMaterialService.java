package org.jeecg.modules.mes.storage.service;

import org.jeecg.modules.mes.storage.entity.MesLackMaterial;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 仓库管理-缺料管理
 * @Author: jeecg-boot
 * @Date:   2020-11-11
 * @Version: V1.0
 */
public interface IMesLackMaterialService extends IService<MesLackMaterial> {

}
