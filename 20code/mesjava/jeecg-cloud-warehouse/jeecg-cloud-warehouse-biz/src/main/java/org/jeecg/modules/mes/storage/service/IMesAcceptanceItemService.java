package org.jeecg.modules.mes.storage.service;

import org.jeecg.modules.mes.storage.entity.MesAcceptanceItem;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.List;

/**
 * @Description: 仓库管理—验收入库子表
 * @Author: jeecg-boot
 * @Date:   2020-09-14
 * @Version: V1.0
 */
public interface IMesAcceptanceItemService extends IService<MesAcceptanceItem> {

	public List<MesAcceptanceItem> selectByMainId(String mainId);
}
