package org.jeecg.modules.mes.storage.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.lang3.StringUtils;
import org.jeecg.modules.mes.client.TransactionClient;
import org.jeecg.modules.mes.order.entity.MesOrderProduce;
import org.jeecg.modules.mes.order.entity.MesProduceItem;
import org.jeecg.modules.mes.storage.entity.MesMaterielOccupy;
import org.jeecg.modules.mes.storage.mapper.MesMaterielOccupyMapper;
import org.jeecg.modules.mes.storage.service.IMesMaterielOccupyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

/**
 * @Description: 仓库管理-领料清单
 * @Author: jeecg-boot
 * @Date: 2020-11-16
 * @Version: V1.0
 */
@Service
public class MesMaterielOccupyServiceImpl extends ServiceImpl<MesMaterielOccupyMapper, MesMaterielOccupy> implements IMesMaterielOccupyService {

    @Autowired
    private MesMaterielOccupyMapper mesMaterielOccupyMapper;
    @Autowired
    private TransactionClient transactionClient;

    @Override
    public String addMaterielOccupyByProduce(MesOrderProduce mesOrderProduce) {
        QueryWrapper<MesMaterielOccupy> queryWrapper = new QueryWrapper();
        queryWrapper.eq("order_id",mesOrderProduce.getId());
        if(com.epms.util.ObjectHelper.isNotEmpty(mesMaterielOccupyMapper.selectList(queryWrapper))){
            return "领料清单以生成，无需重复生成";
        }
        String result = "生成领料清单成功";
        MesMaterielOccupy mesMaterielOccupy = new MesMaterielOccupy();
        mesMaterielOccupy.setCreateBy(mesOrderProduce.getCreateBy());//创建人
        mesMaterielOccupy.setCreateTime(mesOrderProduce.getCreateTime());//创建日期
        mesMaterielOccupy.setSysOrgCode(mesOrderProduce.getSysOrgCode());//所属部门
        mesMaterielOccupy.setOrderId(mesOrderProduce.getId());//生产订单id
        mesMaterielOccupy.setOrderCode(mesOrderProduce.getOrderCode());//订单编号
        mesMaterielOccupy.setOrderName(mesOrderProduce.getOrderName());//订单名称
        for (MesProduceItem item : mesOrderProduce.getProduceItems()) {
            mesMaterielOccupy.setId(null);
            mesMaterielOccupy.setMaterielId(item.getMaterielId());//物料id
            mesMaterielOccupy.setMaterielCode(item.getMaterielCode());//物料料号
            mesMaterielOccupy.setMaterielName(item.getMaterielName());//物料名称
            mesMaterielOccupy.setMaterielGague(item.getMaterielGauge());//物料规格
            mesMaterielOccupy.setUnit(item.getCountUnit());//单位
            mesMaterielOccupy.setOccupyPerson(mesOrderProduce.getCreateBy());//领用人
            mesMaterielOccupy.setOccupyTime(mesOrderProduce.getCreateTime());//领用时间
            mesMaterielOccupy.setOccupyNum(item.getRequireNum());//领用数量
            mesMaterielOccupy.setUnusedNum("0");//未使用数量
            mesMaterielOccupy.setSendNum("0");//发料数量
            mesMaterielOccupy.setWithdrawNum("0");//退料数量
            mesMaterielOccupy.setTransformNum("0");//转产数量
            //mesMaterielOccupy.getQuery6();//制令单号
            if (mesMaterielOccupyMapper.insert(mesMaterielOccupy) != 1) {
                result = "生成领料清单失败";
                break;
            }
        }
        return result;
    }

    /**
     * 根据生产订单id和物料料号查询领料清单 并且更新领料表的退料数量
     *
     * @param orderId 生产订单id
     * @param mCode 物料料号
     * @param withdrawNum 需退料数量
     * @return
     */
    public MesMaterielOccupy queryByIdAndmCodeUpdatewithdrawNum(String orderId,String mCode,String withdrawNum) {
        QueryWrapper<MesMaterielOccupy> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("order_id", orderId).eq("materiel_code", mCode);
        MesMaterielOccupy mesMaterielOccupy = this.getOne(queryWrapper);
        if (mesMaterielOccupy==null){
            MesOrderProduce mesOrderProduce = transactionClient.queryMesProduceItemByOrderId(orderId);
            this.addMaterielOccupyByProduce(mesOrderProduce);
            mesMaterielOccupy = this.getOne(queryWrapper);
        }
        if (StringUtils.isBlank(mesMaterielOccupy.getSendNum())) {
            mesMaterielOccupy.setSendNum("0");
        }
        BigDecimal sendNum1 = new BigDecimal(withdrawNum);//需退料数量
        BigDecimal sendformNum = new BigDecimal(mesMaterielOccupy.getWithdrawNum());//已退料数量
        BigDecimal sendNewnum = sendformNum.add(sendNum1);//新的退料数量 = 已退料数量 + 需退料数量
        //更新领料表的退料数量
        mesMaterielOccupy.setWithdrawNum(sendNewnum.toString());
        this.updateById(mesMaterielOccupy);
        return mesMaterielOccupy;
    }

    /**
     * 根据生产订单id和物料料号查询领料清单 并且更新领料表的发料数量  远程调用
     *
     * @param orderId
     * @param mCode
     * @param sendNum 发料数量
     * @return
     */
    public MesMaterielOccupy queryByIdAndmCodeUpdateSendNum(String orderId,String mCode,String sendNum) {
        QueryWrapper<MesMaterielOccupy> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("order_id", orderId).eq("materiel_code", mCode);
        MesMaterielOccupy mesMaterielOccupy = this.getOne(queryWrapper);
        if (mesMaterielOccupy==null){
            MesOrderProduce mesOrderProduce = transactionClient.queryMesProduceItemByOrderId(orderId);
            this.addMaterielOccupyByProduce(mesOrderProduce);
            mesMaterielOccupy = this.getOne(queryWrapper);
        }
        if (StringUtils.isBlank(mesMaterielOccupy.getSendNum())) {
            mesMaterielOccupy.setSendNum("0");
        }
        BigDecimal sendNum1 = new BigDecimal(sendNum);//需发料数量
        BigDecimal sendformNum = new BigDecimal(mesMaterielOccupy.getSendNum());//已发数量
        BigDecimal sendNewnum = sendformNum.add(sendNum1);//新的发料数量 = 已发料数量 + 发料数量
        System.out.println("需发料数量" + sendNum1 + "已发数量" + sendformNum + "新的发料数量" + sendNewnum);
        //更新领料表的发料数量
        mesMaterielOccupy.setSendNum(sendNewnum.toString());
        this.updateById(mesMaterielOccupy);
        return mesMaterielOccupy;
    }

    /**
     * 根据生产订单id和物料料号查询领料清单 并且更新领料表的转产数量  远程调用
     *
     * @param orderId
     * @param mCode
     * @param realNum 转产数量
     * @return
     */
    public MesMaterielOccupy queryByIdAndmCodeUpdateTransformNum(String orderId,String mCode,String realNum) {
        QueryWrapper<MesMaterielOccupy> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("order_id", orderId).eq("materiel_code", mCode);
        MesMaterielOccupy mesMaterielOccupy = this.getOne(queryWrapper);
        if (mesMaterielOccupy==null){
            MesOrderProduce mesOrderProduce = transactionClient.queryMesProduceItemByOrderId(orderId);
            this.addMaterielOccupyByProduce(mesOrderProduce);
            mesMaterielOccupy = this.getOne(queryWrapper);
        }
        if (StringUtils.isBlank(mesMaterielOccupy.getTransformNum())) {
            mesMaterielOccupy.setTransformNum("0");
        }
        BigDecimal realNum1 = new BigDecimal(realNum);//需转产数量
        BigDecimal transformNum = new BigDecimal(mesMaterielOccupy.getTransformNum());//已转产数量
        BigDecimal transNewnum = transformNum.add(realNum1);//新的转产数量 = 已转产数量 + 转产数量
        System.out.println("需转产数量:" + realNum1.toString() + " 已转产数量:" + transformNum + " 新的转产数量:" + transNewnum);
        //更新领料表的转产数量
        mesMaterielOccupy.setTransformNum(transNewnum.toString());
        this.updateById(mesMaterielOccupy);
        return mesMaterielOccupy;
    }
}
