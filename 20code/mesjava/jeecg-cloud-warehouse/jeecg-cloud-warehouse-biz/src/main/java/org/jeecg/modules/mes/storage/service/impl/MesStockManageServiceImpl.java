package org.jeecg.modules.mes.storage.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.jeecg.modules.mes.storage.entity.MesStockManage;
import org.jeecg.modules.mes.storage.entity.MesWarehouseArea;
import com.epms.util.ObjectHelper;
import org.jeecg.modules.mes.storage.mapper.MesStockManageMapper;
import org.jeecg.modules.mes.storage.service.IMesStockManageService;
import org.jeecg.modules.mes.storage.service.IMesWarehouseAreaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import java.math.BigDecimal;
import java.util.List;

/**
 * @Description: 仓库管理-库存管理
 * @Author: jeecg-boot
 * @Date: 2020-11-11
 * @Version: V1.0
 */
@Service
public class MesStockManageServiceImpl extends ServiceImpl<MesStockManageMapper, MesStockManage> implements IMesStockManageService {

    @Autowired
    private MesStockManageMapper mesStockManageMapper;

    @Autowired
    private IMesWarehouseAreaService iMesWarehouseAreaService;

    /**
     * 根据物料编号查询库存信息
     */
    @Override
    public MesStockManage getStockManageByMaterielCode(String materielCode) {
        QueryWrapper<MesStockManage> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("materiel_code", materielCode).orderByDesc("create_time");
        List<MesStockManage> list = mesStockManageMapper.selectList(queryWrapper);
        if (ObjectHelper.isNotEmpty(list)) {
            return list.get(0);
        }
        return null;
    }

    /**
     * 根据物料料号和客户名称查询库存
     */
    @Override
    public MesStockManage queryByMcodeAndClient(String mCode, String clientName) {
        MesWarehouseArea mesWarehouseArea = iMesWarehouseAreaService.getMesWarehouseAreaByClientName(clientName);
        if (ObjectHelper.isEmpty(mesWarehouseArea)) {
            return null;
        }
        QueryWrapper<MesStockManage> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("materiel_code", mCode);
        queryWrapper.eq("area_code", mesWarehouseArea.getAreaCode());
        queryWrapper.ne("store_name", "不良品仓");
        queryWrapper.eq("location_code", mesWarehouseArea.getAreaLocations().get(0).getLocationCode());
        queryWrapper.orderByDesc("create_time");
        List<MesStockManage> mesStockManages = mesStockManageMapper.selectList(queryWrapper);
        if (mesStockManages.size()==0){
            throw new RuntimeException("未找到该客户:"+clientName+" 该物料"+mCode+"的信息！");
        }
        //有两条数据，合并
        if (mesStockManages.size()>1){
            BigDecimal stock= BigDecimal.ZERO;
            for (int i = 0; i < mesStockManages.size(); i++) {
                BigDecimal newstock= new BigDecimal(mesStockManages.get(i).getStockNum());
                stock=stock.add(newstock);
                if (i>0){
                    mesStockManageMapper.deleteById(mesStockManages.get(i).getId());
                }
            }
            mesStockManages.get(0).setStockNum(stock.toString());
            mesStockManageMapper.updateById(mesStockManages.get(0));
        }
        return mesStockManages.get(0);
    }

    /**
     * 根据物料料号和客户名称查询库存不良品
     */
    @Override
    public MesStockManage queryByMcodeAndClientblp(String mCode, String clientName) {
        MesWarehouseArea mesWarehouseArea = iMesWarehouseAreaService.getMesWarehouseAreaByClientName(clientName);
        if (ObjectHelper.isEmpty(mesWarehouseArea)) {
            return null;
        }
        QueryWrapper<MesStockManage> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("materiel_code", mCode);
        queryWrapper.eq("area_code", mesWarehouseArea.getAreaCode());
        queryWrapper.eq("store_name", "不良品仓");
        queryWrapper.eq("location_code", mesWarehouseArea.getAreaLocations().get(0).getLocationCode());
        queryWrapper.orderByDesc("create_time");
        List<MesStockManage> mesStockManages = mesStockManageMapper.selectList(queryWrapper);
        if (mesStockManages.size()==0){
//            MesStockManage stockManage = queryByMcodeAndClient(mCode, clientName);
            MesStockManage stockManage = getStockManageByMaterielCode(mCode);
            stockManage.setStockNum("0");
            stockManage.setStoreName("不良品仓");
            stockManage.setId(null);
            mesStockManageMapper.insert(stockManage);
            mesStockManages.add(stockManage);
        }
        //有两条数据，合并
        if (mesStockManages.size()>1){
            BigDecimal stock= BigDecimal.ZERO;
            for (int i = 0; i < mesStockManages.size(); i++) {
                BigDecimal newstock= new BigDecimal(mesStockManages.get(i).getStockNum());
                stock=stock.add(newstock);
                if (i>0){
                    mesStockManageMapper.deleteById(mesStockManages.get(i).getId());
                }
            }
            mesStockManages.get(0).setStockNum(stock.toString());
            mesStockManageMapper.updateById(mesStockManages.get(0));
        }
        return mesStockManages.get(0);
    }
}
