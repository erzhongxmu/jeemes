package org.jeecg.modules.mes.storage.controller;

import java.io.UnsupportedEncodingException;
import java.io.IOException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.jeecg.common.system.vo.LoginUser;
import org.apache.shiro.SecurityUtils;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.mes.storage.entity.MesTransferItem;
import org.jeecg.modules.mes.storage.entity.MesStorageTransfer;
import org.jeecg.modules.mes.storage.vo.MesStorageTransferPage;
import org.jeecg.modules.mes.storage.service.IMesStorageTransferService;
import org.jeecg.modules.mes.storage.service.IMesTransferItemService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

 /**
 * @Description: 仓库管理—调拨移动
 * @Author: jeecg-boot
 * @Date:   2020-09-14
 * @Version: V1.0
 */
@Api(tags="仓库管理—调拨移动")
@RestController
@RequestMapping("/storage/mesStorageTransfer")
@Slf4j
public class MesStorageTransferController {
	@Autowired
	private IMesStorageTransferService mesStorageTransferService;
	@Autowired
	private IMesTransferItemService mesTransferItemService;
	
	/**
	 * 分页列表查询
	 *
	 * @param mesStorageTransfer
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "仓库管理—调拨移动-分页列表查询")
	@ApiOperation(value="仓库管理—调拨移动-分页列表查询", notes="仓库管理—调拨移动-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(MesStorageTransfer mesStorageTransfer,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<MesStorageTransfer> queryWrapper = QueryGenerator.initQueryWrapper(mesStorageTransfer, req.getParameterMap());
		Page<MesStorageTransfer> page = new Page<MesStorageTransfer>(pageNo, pageSize);
		IPage<MesStorageTransfer> pageList = mesStorageTransferService.page(page, queryWrapper);
		return Result.ok(pageList);
	}
	
	/**
	 *   添加
	 *
	 * @param mesStorageTransferPage
	 * @return
	 */
	@AutoLog(value = "仓库管理—调拨移动-添加")
	@ApiOperation(value="仓库管理—调拨移动-添加", notes="仓库管理—调拨移动-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody MesStorageTransferPage mesStorageTransferPage) {
		MesStorageTransfer mesStorageTransfer = new MesStorageTransfer();
		BeanUtils.copyProperties(mesStorageTransferPage, mesStorageTransfer);
		mesStorageTransferService.saveMain(mesStorageTransfer, mesStorageTransferPage.getMesTransferItemList());
		return Result.ok("添加成功！");
	}
	
	/**
	 *  编辑
	 *
	 * @param mesStorageTransferPage
	 * @return
	 */
	@AutoLog(value = "仓库管理—调拨移动-编辑")
	@ApiOperation(value="仓库管理—调拨移动-编辑", notes="仓库管理—调拨移动-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody MesStorageTransferPage mesStorageTransferPage) {
		MesStorageTransfer mesStorageTransfer = new MesStorageTransfer();
		BeanUtils.copyProperties(mesStorageTransferPage, mesStorageTransfer);
		MesStorageTransfer mesStorageTransferEntity = mesStorageTransferService.getById(mesStorageTransfer.getId());
		if(mesStorageTransferEntity==null) {
			return Result.error("未找到对应数据");
		}
		mesStorageTransferService.updateMain(mesStorageTransfer, mesStorageTransferPage.getMesTransferItemList());
		return Result.ok("编辑成功!");
	}
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "仓库管理—调拨移动-通过id删除")
	@ApiOperation(value="仓库管理—调拨移动-通过id删除", notes="仓库管理—调拨移动-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		mesStorageTransferService.delMain(id);
		return Result.ok("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "仓库管理—调拨移动-批量删除")
	@ApiOperation(value="仓库管理—调拨移动-批量删除", notes="仓库管理—调拨移动-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.mesStorageTransferService.delBatchMain(Arrays.asList(ids.split(",")));
		return Result.ok("批量删除成功！");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "仓库管理—调拨移动-通过id查询")
	@ApiOperation(value="仓库管理—调拨移动-通过id查询", notes="仓库管理—调拨移动-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		MesStorageTransfer mesStorageTransfer = mesStorageTransferService.getById(id);
		if(mesStorageTransfer==null) {
			return Result.error("未找到对应数据");
		}
		return Result.ok(mesStorageTransfer);

	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "仓库管理—调拨移动子表通过主表ID查询")
	@ApiOperation(value="仓库管理—调拨移动子表主表ID查询", notes="仓库管理—调拨移动子表-通主表ID查询")
	@GetMapping(value = "/queryMesTransferItemByMainId")
	public Result<?> queryMesTransferItemListByMainId(@RequestParam(name="id",required=true) String id) {
		List<MesTransferItem> mesTransferItemList = mesTransferItemService.selectByMainId(id);
		return Result.ok(mesTransferItemList);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param mesStorageTransfer
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, MesStorageTransfer mesStorageTransfer) {
      // Step.1 组装查询条件查询数据
      QueryWrapper<MesStorageTransfer> queryWrapper = QueryGenerator.initQueryWrapper(mesStorageTransfer, request.getParameterMap());
      LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();

      //Step.2 获取导出数据
      List<MesStorageTransfer> queryList = mesStorageTransferService.list(queryWrapper);
      // 过滤选中数据
      String selections = request.getParameter("selections");
      List<MesStorageTransfer> mesStorageTransferList = new ArrayList<MesStorageTransfer>();
      if(oConvertUtils.isEmpty(selections)) {
          mesStorageTransferList = queryList;
      }else {
          List<String> selectionList = Arrays.asList(selections.split(","));
          mesStorageTransferList = queryList.stream().filter(item -> selectionList.contains(item.getId())).collect(Collectors.toList());
      }

      // Step.3 组装pageList
      List<MesStorageTransferPage> pageList = new ArrayList<MesStorageTransferPage>();
      for (MesStorageTransfer main : mesStorageTransferList) {
          MesStorageTransferPage vo = new MesStorageTransferPage();
          BeanUtils.copyProperties(main, vo);
          List<MesTransferItem> mesTransferItemList = mesTransferItemService.selectByMainId(main.getId());
          vo.setMesTransferItemList(mesTransferItemList);
          pageList.add(vo);
      }

      // Step.4 AutoPoi 导出Excel
      ModelAndView mv = new ModelAndView(new JeecgEntityExcelView());
      mv.addObject(NormalExcelConstants.FILE_NAME, "仓库管理—调拨移动列表");
      mv.addObject(NormalExcelConstants.CLASS, MesStorageTransferPage.class);
      mv.addObject(NormalExcelConstants.PARAMS, new ExportParams("仓库管理—调拨移动数据", "导出人:"+sysUser.getRealname(), "仓库管理—调拨移动"));
      mv.addObject(NormalExcelConstants.DATA_LIST, pageList);
      return mv;
    }

    /**
    * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
      MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
      Map<String, MultipartFile> fileMap = multipartRequest.getFileMap();
      for (Map.Entry<String, MultipartFile> entity : fileMap.entrySet()) {
          MultipartFile file = entity.getValue();// 获取上传文件对象
          ImportParams params = new ImportParams();
          params.setTitleRows(2);
          params.setHeadRows(1);
          params.setNeedSave(true);
          try {
              List<MesStorageTransferPage> list = ExcelImportUtil.importExcel(file.getInputStream(), MesStorageTransferPage.class, params);
              for (MesStorageTransferPage page : list) {
                  MesStorageTransfer po = new MesStorageTransfer();
                  BeanUtils.copyProperties(page, po);
                  mesStorageTransferService.saveMain(po, page.getMesTransferItemList());
              }
              return Result.ok("文件导入成功！数据行数:" + list.size());
          } catch (Exception e) {
              log.error(e.getMessage(),e);
              return Result.error("文件导入失败:"+e.getMessage());
          } finally {
              try {
                  file.getInputStream().close();
              } catch (IOException e) {
                  e.printStackTrace();
              }
          }
      }
      return Result.ok("文件导入失败！");
    }

}
