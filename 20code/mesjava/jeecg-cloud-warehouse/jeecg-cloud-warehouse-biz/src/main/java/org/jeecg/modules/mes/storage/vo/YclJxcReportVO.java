package org.jeecg.modules.mes.storage.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class YclJxcReportVO implements Serializable {

    @ApiModelProperty(value = "物料料号")
    private String materid;

    @ApiModelProperty(value = "区域编码")
    private String areaCode;

    @ApiModelProperty(value = "位置编码")
    private String locationCode;

    @ApiModelProperty(value = "ids")
    private List<String> ids;
}
