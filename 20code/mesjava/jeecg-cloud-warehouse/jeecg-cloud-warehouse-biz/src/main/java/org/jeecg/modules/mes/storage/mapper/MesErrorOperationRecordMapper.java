package org.jeecg.modules.mes.storage.mapper;

import org.jeecg.modules.mes.storage.entity.MesErrorOperationRecord;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 错误操作记录
 * @Author: jeecg-boot
 * @Date:   2021-05-31
 * @Version: V1.0
 */
public interface MesErrorOperationRecordMapper extends BaseMapper<MesErrorOperationRecord> {

}
