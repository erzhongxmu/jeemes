package org.jeecg.modules.mes.storage.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.jeecg.common.api.vo.Result;
import org.jeecg.modules.mes.storage.entity.MesWarehouseArea;
import org.jeecg.modules.mes.storage.entity.MesWarehouseAreaLocation;
import org.jeecg.modules.mes.storage.mapper.MesWarehouseAreaLocationMapper;
import org.jeecg.modules.mes.storage.mapper.MesWarehouseAreaMapper;
import org.jeecg.modules.mes.storage.service.IMesWarehouseAreaService;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.util.List;
import java.util.Collection;

import com.epms.util.ObjectHelper;

/**
 * @Description: 仓库区域表
 * @Author: jeecg-boot
 * @Date: 2021-05-13
 * @Version: V1.0
 */
@Service
public class MesWarehouseAreaServiceImpl extends ServiceImpl<MesWarehouseAreaMapper, MesWarehouseArea> implements IMesWarehouseAreaService {

    @Autowired
    private MesWarehouseAreaMapper mesWarehouseAreaMapper;
    @Autowired
    private MesWarehouseAreaLocationMapper mesWarehouseAreaLocationMapper;

    @Override
    @Transactional
    public void delMain(String id) {
        mesWarehouseAreaLocationMapper.deleteByMainId(id);
        mesWarehouseAreaMapper.deleteById(id);
    }

    @Override
    @Transactional
    public void delBatchMain(Collection<? extends Serializable> idList) {
        for (Serializable id : idList) {
            mesWarehouseAreaLocationMapper.deleteByMainId(id.toString());
            mesWarehouseAreaMapper.deleteById(id);
        }
    }

    @Override
    @Transactional
    public Result<?> add(MesWarehouseArea mesWarehouseArea) {
        //验证区域编码是否重复
        if (ObjectHelper.isEmpty(mesWarehouseArea.getAreaCode())) {
            return Result.error("区域编码不能为空！");
        }

        List<MesWarehouseArea> newMesWarehouseArea = mesWarehouseAreaMapper.selectByAreaCode(mesWarehouseArea.getAreaCode());

        if (ObjectHelper.isNotEmpty(newMesWarehouseArea)) {
            return Result.error("区域编码【" + mesWarehouseArea.getAreaCode() + "】以存在，请修改为其他区域编码！");
        }

        mesWarehouseAreaMapper.insert(mesWarehouseArea);
        return Result.ok("添加成功！");
    }

    @Override
    @Transactional
    public Result<?> edit(MesWarehouseArea mesWarehouseArea) {
        //验证区域编码是否重复
        if (ObjectHelper.isEmpty(mesWarehouseArea.getAreaCode())) {
            return Result.error("区域编码不能为空！");
        }

        MesWarehouseArea newMesWarehouseArea = mesWarehouseAreaMapper.selectById(mesWarehouseArea.getId());

        if (!mesWarehouseArea.getAreaCode().equals(newMesWarehouseArea.getAreaCode())) {//新区域编码和原区域编码不一样需要验证是否有重复编码
            List<MesWarehouseArea> entity = mesWarehouseAreaMapper.selectByAreaCode(mesWarehouseArea.getAreaCode());
            if (null != entity && entity.size() > 0) {
                return Result.error("区域编码【" + mesWarehouseArea.getAreaCode() + "】以存在，请修改为其他区域编码！");
            }
        }

        mesWarehouseAreaMapper.updateById(mesWarehouseArea);
        return Result.ok("编辑成功!");
    }

    @Override
    public MesWarehouseArea getMesWarehouseAreaByClientName(String clientName) {
        QueryWrapper<MesWarehouseAreaLocation> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("client_name", clientName);
        List<MesWarehouseAreaLocation> warehouseAreaLocations = mesWarehouseAreaLocationMapper.selectList(queryWrapper);
        if (ObjectHelper.isEmpty(warehouseAreaLocations)) {
            return null;
        }
        MesWarehouseArea warehouseArea = mesWarehouseAreaMapper.selectById(warehouseAreaLocations.get(0).getAreaId());
        if (ObjectHelper.isEmpty(warehouseArea)) {
            return null;
        }
        warehouseArea.setAreaLocations(warehouseAreaLocations);
        return warehouseArea;
    }

    @Override
    public Result<?> queryListByAreaCode(String areaCode) {
        QueryWrapper<MesWarehouseArea> queryWrapper = new QueryWrapper<>();
        queryWrapper.like("area_code", areaCode);
        List<MesWarehouseArea> list = mesWarehouseAreaMapper.selectList(queryWrapper);
        return Result.ok(list);
    }

    @Override
    public Result<?> queryListByLocationCode(String areaId, String locationCode) {
        QueryWrapper<MesWarehouseAreaLocation> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("area_id", areaId);
        queryWrapper.like("location_code", locationCode);
        List<MesWarehouseAreaLocation> list = mesWarehouseAreaLocationMapper.selectList(queryWrapper);
        return Result.ok(list);
    }
}
