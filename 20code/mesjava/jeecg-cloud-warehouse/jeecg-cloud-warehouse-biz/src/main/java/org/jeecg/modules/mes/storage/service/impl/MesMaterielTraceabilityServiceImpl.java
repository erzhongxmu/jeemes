package org.jeecg.modules.mes.storage.service.impl;

import org.jeecg.common.api.vo.Result;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.jeecg.modules.mes.storage.entity.MesMaterielTraceability;
import org.jeecg.modules.mes.storage.mapper.MesMaterielTraceabilityMapper;
import org.jeecg.modules.mes.storage.service.IMesMaterielTraceabilityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 物料追溯表
 * @Author: jeecg-boot
 * @Date:   2021-06-02
 * @Version: V1.0
 */
@Service
public class MesMaterielTraceabilityServiceImpl extends ServiceImpl<MesMaterielTraceabilityMapper, MesMaterielTraceability> implements IMesMaterielTraceabilityService {

    @Autowired
    private MesMaterielTraceabilityMapper mesMaterielTraceabilityMapper;

    @Override
    public Result<?> queryPageList(MesMaterielTraceability mesMaterielTraceability, Integer pageNo, Integer pageSize) {
        Page<MesMaterielTraceability> page = new Page<>(pageNo, pageSize);
        page.setRecords(mesMaterielTraceabilityMapper.queryPageList(page, mesMaterielTraceability));
        return Result.ok(page);
    }
}
