package org.jeecg.modules.mes.storage.vo;

import lombok.Data;

import java.io.Serializable;

@Data
public class ReportVo implements Serializable {
    private String id;
    private String attr1;//订单编号
    private String attr2;//料号
    private String attr3;//客户料号
    private String attr4;//名称
    private String attr5;//规格
    private String attr6;//伏数
    private String attr7;//误差
    private String attr8;//单位
    private String attr9;//申购数量
    private String attr10;//实收数量
    private String attr11;//日期
    private String attr12;//供应商
    private String attr13;//入库数量
    private String attr14;//区域编码
    private String attr15;//位置编码
    private String areaCode;
    private String locationCode;
}
