package org.jeecg.modules.mes.storage.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.jeecg.modules.mes.storage.entity.MesCertificateItem;

import java.util.List;

/**
 * @Description: 物料凭证项目
 * @Author: jeecg-boot
 * @Date:   2020-10-15
 * @Version: V1.0
 */
public interface MesCertificateItemMapper extends BaseMapper<MesCertificateItem> {

	public boolean deleteByMainId(@Param("mainId") String mainId);
    
	public List<MesCertificateItem> selectByMainId(@Param("mainId") String mainId);

	/**
	 * 根据打印的标签二维码获取他的item质检信息
	 * @param query4
	 * @return
	 */
	@Select("SELECT *  " +
			"from mes_certificate_item, " +
			"(select query3,product_code from mes_storage_wholesale " +
			"where base_dockettype='扫描收货' " +
			"and query4=#{query4}) s " +
			"WHERE perk_id=s.query3 " +
			"and materiel_code=s.product_code " +
			"and mobile_type='扫描收货' and mobile_type='扫描收货' and if_inspect='否'")
	public List<MesCertificateItem> selectbqByItem(@Param("query4")String query4);

}
