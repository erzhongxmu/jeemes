package org.jeecg.modules.mes.storage.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.jeecg.modules.mes.storage.entity.MesCertificateItem;
import org.jeecg.modules.mes.storage.entity.MesCertificatePerk;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

/**
 * @Description: 物料凭证抬头
 * @Author: jeecg-boot
 * @Date:   2020-10-15
 * @Version: V1.0
 */
public interface IMesCertificatePerkService extends IService<MesCertificatePerk> {

	/**
	 * 删除一对多
	 */
	public void delMain (String id);
	
	/**
	 * 批量删除一对多
	 */
	public void delBatchMain (Collection<? extends Serializable> idList);

	/**
	 * 根据原制令单号和新制令单号，查询凭证抬头
	 * @param oldCode
	 * @param nowCode
	 * @return
	 */
	public List<MesCertificatePerk> getChangeProduce(String oldCode,String nowCode);

	/**
	 * 没有该物料的凭证项目，就新增物料凭证项目，否则修改
	 * @param mesCertificateItem
	 * @return
	 */
	public MesCertificateItem addReceiveItem(MesCertificateItem mesCertificateItem);
	/**
	 * 没有该物料的凭证项目，就新增物料凭证项目，否则修改 扫描收货使用
	 * @param mesCertificateItem
	 * @return
	 */
	public MesCertificateItem addReceiveItem2(MesCertificateItem mesCertificateItem);
}
