package org.jeecg.modules.mes.storage.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.mes.storage.entity.MesStorageAssign;
import org.jeecg.modules.mes.storage.service.IMesStorageAssignService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

 /**
 * @Description: 仓库管理—拣配单
 * @Author: jeecg-boot
 * @Date:   2020-09-14
 * @Version: V1.0
 */
@Api(tags="仓库管理—拣配单")
@RestController
@RequestMapping("/storage/mesStorageAssign")
@Slf4j
public class MesStorageAssignController extends JeecgController<MesStorageAssign, IMesStorageAssignService> {
	@Autowired
	private IMesStorageAssignService mesStorageAssignService;
	
	/**
	 * 分页列表查询
	 *
	 * @param mesStorageAssign
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "仓库管理—拣配单-分页列表查询")
	@ApiOperation(value="仓库管理—拣配单-分页列表查询", notes="仓库管理—拣配单-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(MesStorageAssign mesStorageAssign,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<MesStorageAssign> queryWrapper = QueryGenerator.initQueryWrapper(mesStorageAssign, req.getParameterMap());
		Page<MesStorageAssign> page = new Page<MesStorageAssign>(pageNo, pageSize);
		IPage<MesStorageAssign> pageList = mesStorageAssignService.page(page, queryWrapper);
		return Result.ok(pageList);
	}
	
	/**
	 *   添加
	 *
	 * @param mesStorageAssign
	 * @return
	 */
	@AutoLog(value = "仓库管理—拣配单-添加")
	@ApiOperation(value="仓库管理—拣配单-添加", notes="仓库管理—拣配单-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody MesStorageAssign mesStorageAssign) {
		mesStorageAssignService.save(mesStorageAssign);
		return Result.ok("添加成功！");
	}
	
	/**
	 *  编辑
	 *
	 * @param mesStorageAssign
	 * @return
	 */
	@AutoLog(value = "仓库管理—拣配单-编辑")
	@ApiOperation(value="仓库管理—拣配单-编辑", notes="仓库管理—拣配单-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody MesStorageAssign mesStorageAssign) {
		mesStorageAssignService.updateById(mesStorageAssign);
		return Result.ok("编辑成功!");
	}
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "仓库管理—拣配单-通过id删除")
	@ApiOperation(value="仓库管理—拣配单-通过id删除", notes="仓库管理—拣配单-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		mesStorageAssignService.removeById(id);
		return Result.ok("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "仓库管理—拣配单-批量删除")
	@ApiOperation(value="仓库管理—拣配单-批量删除", notes="仓库管理—拣配单-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.mesStorageAssignService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.ok("批量删除成功!");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "仓库管理—拣配单-通过id查询")
	@ApiOperation(value="仓库管理—拣配单-通过id查询", notes="仓库管理—拣配单-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		MesStorageAssign mesStorageAssign = mesStorageAssignService.getById(id);
		if(mesStorageAssign==null) {
			return Result.error("未找到对应数据");
		}
		return Result.ok(mesStorageAssign);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param mesStorageAssign
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, MesStorageAssign mesStorageAssign) {
        return super.exportXls(request, mesStorageAssign, MesStorageAssign.class, "仓库管理—拣配单");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, MesStorageAssign.class);
    }

}
