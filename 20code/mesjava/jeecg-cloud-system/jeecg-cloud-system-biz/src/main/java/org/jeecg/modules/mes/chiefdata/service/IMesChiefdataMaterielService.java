package org.jeecg.modules.mes.chiefdata.service;

import org.jeecg.modules.mes.chiefdata.entity.MesMaterielPcb;
import org.jeecg.modules.mes.chiefdata.entity.MesMaterielNetknife;
import org.jeecg.modules.mes.chiefdata.entity.MesMaterielProduce;
import org.jeecg.modules.mes.chiefdata.entity.MesMaterielRedgum;
import org.jeecg.modules.mes.chiefdata.entity.MesMaterielQuality;
import org.jeecg.modules.mes.chiefdata.entity.MesMaterielStorage;
import org.jeecg.modules.mes.chiefdata.entity.MesMaterielOther;
import org.jeecg.modules.mes.chiefdata.entity.MesChiefdataMateriel;
import com.baomidou.mybatisplus.extension.service.IService;
import java.io.Serializable;
import java.util.Collection;
import java.util.List;

/**
 * @Description: 主数据—物料
 * @Author: jeecg-boot
 * @Date:   2020-10-16
 * @Version: V1.0
 */
public interface IMesChiefdataMaterielService extends IService<MesChiefdataMateriel> {

	/**
	 * 添加一对多
	 * 
	 */
	public void saveMain(MesChiefdataMateriel mesChiefdataMateriel,List<MesMaterielPcb> mesMaterielPcbList,List<MesMaterielNetknife> mesMaterielNetknifeList,List<MesMaterielProduce> mesMaterielProduceList,List<MesMaterielRedgum> mesMaterielRedgumList,List<MesMaterielQuality> mesMaterielQualityList,List<MesMaterielStorage> mesMaterielStorageList,List<MesMaterielOther> mesMaterielOtherList) ;
	
	/**
	 * 修改一对多
	 * 
	 */
	public void updateMain(MesChiefdataMateriel mesChiefdataMateriel,List<MesMaterielPcb> mesMaterielPcbList,List<MesMaterielNetknife> mesMaterielNetknifeList,List<MesMaterielProduce> mesMaterielProduceList,List<MesMaterielRedgum> mesMaterielRedgumList,List<MesMaterielQuality> mesMaterielQualityList,List<MesMaterielStorage> mesMaterielStorageList,List<MesMaterielOther> mesMaterielOtherList);
	
	/**
	 * 删除一对多
	 */
	public void delMain (String id);
	
	/**
	 * 批量删除一对多
	 */
	public void delBatchMain (Collection<? extends Serializable> idList);
	
}
