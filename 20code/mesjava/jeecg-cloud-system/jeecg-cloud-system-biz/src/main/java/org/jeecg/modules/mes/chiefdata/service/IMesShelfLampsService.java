package org.jeecg.modules.mes.chiefdata.service;

import org.jeecg.modules.mes.chiefdata.entity.MesShelfLamps;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 货架多色灯
 * @Author: jeecg-boot
 * @Date:   2020-10-12
 * @Version: V1.0
 */
public interface IMesShelfLampsService extends IService<MesShelfLamps> {

}
