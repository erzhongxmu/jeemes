package org.jeecg.modules.mes.chiefdata.service;

import org.jeecg.modules.mes.chiefdata.entity.MesChiefdataSupplieraddress;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 主数据—供应商地址
 * @Author: jeecg-boot
 * @Date:   2020-09-14
 * @Version: V1.0
 */
public interface IMesChiefdataSupplieraddressService extends IService<MesChiefdataSupplieraddress> {

}
