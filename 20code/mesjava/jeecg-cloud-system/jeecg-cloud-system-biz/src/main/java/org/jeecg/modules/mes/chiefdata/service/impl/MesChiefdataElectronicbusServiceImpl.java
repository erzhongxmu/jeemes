package org.jeecg.modules.mes.chiefdata.service.impl;

import org.jeecg.modules.mes.chiefdata.entity.MesChiefdataElectronicbus;
import org.jeecg.modules.mes.chiefdata.entity.MesElectronicbusBaseinfo;
import org.jeecg.modules.mes.chiefdata.mapper.MesElectronicbusBaseinfoMapper;
import org.jeecg.modules.mes.chiefdata.mapper.MesChiefdataElectronicbusMapper;
import org.jeecg.modules.mes.chiefdata.service.IMesChiefdataElectronicbusService;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import java.io.Serializable;
import java.util.List;
import java.util.Collection;

/**
 * @Description: 主数据—电子料车
 * @Author: jeecg-boot
 * @Date:   2020-09-17
 * @Version: V1.0
 */
@Service
public class MesChiefdataElectronicbusServiceImpl extends ServiceImpl<MesChiefdataElectronicbusMapper, MesChiefdataElectronicbus> implements IMesChiefdataElectronicbusService {

	@Autowired
	private MesChiefdataElectronicbusMapper mesChiefdataElectronicbusMapper;
	@Autowired
	private MesElectronicbusBaseinfoMapper mesElectronicbusBaseinfoMapper;
	
	@Override
	@Transactional
	public void saveMain(MesChiefdataElectronicbus mesChiefdataElectronicbus, List<MesElectronicbusBaseinfo> mesElectronicbusBaseinfoList) {
		mesChiefdataElectronicbusMapper.insert(mesChiefdataElectronicbus);
		if(mesElectronicbusBaseinfoList!=null && mesElectronicbusBaseinfoList.size()>0) {
			for(MesElectronicbusBaseinfo entity:mesElectronicbusBaseinfoList) {
				//外键设置
				entity.setBusId(mesChiefdataElectronicbus.getId());
				mesElectronicbusBaseinfoMapper.insert(entity);
			}
		}
	}

	@Override
	@Transactional
	public void updateMain(MesChiefdataElectronicbus mesChiefdataElectronicbus,List<MesElectronicbusBaseinfo> mesElectronicbusBaseinfoList) {
		mesChiefdataElectronicbusMapper.updateById(mesChiefdataElectronicbus);
		
		//1.先删除子表数据
		mesElectronicbusBaseinfoMapper.deleteByMainId(mesChiefdataElectronicbus.getId());
		
		//2.子表数据重新插入
		if(mesElectronicbusBaseinfoList!=null && mesElectronicbusBaseinfoList.size()>0) {
			for(MesElectronicbusBaseinfo entity:mesElectronicbusBaseinfoList) {
				//外键设置
				entity.setBusId(mesChiefdataElectronicbus.getId());
				mesElectronicbusBaseinfoMapper.insert(entity);
			}
		}
	}

	@Override
	@Transactional
	public void delMain(String id) {
		mesElectronicbusBaseinfoMapper.deleteByMainId(id);
		mesChiefdataElectronicbusMapper.deleteById(id);
	}

	@Override
	@Transactional
	public void delBatchMain(Collection<? extends Serializable> idList) {
		for(Serializable id:idList) {
			mesElectronicbusBaseinfoMapper.deleteByMainId(id.toString());
			mesChiefdataElectronicbusMapper.deleteById(id);
		}
	}
	
}
