package org.jeecg.modules.mes.chiefdata.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.jeecg.modules.mes.chiefdata.entity.MesChiefdataReplacematerial;
import org.jeecg.modules.mes.chiefdata.mapper.MesChiefdataReplacematerialMapper;
import org.jeecg.modules.mes.chiefdata.service.IMesChiefdataReplacematerialService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @Description: 主数据—替代料
 * @Author: jeecg-boot
 * @Date:   2020-09-17
 * @Version: V1.0
 */
@Service
public class MesChiefdataReplacematerialServiceImpl extends ServiceImpl<MesChiefdataReplacematerialMapper, MesChiefdataReplacematerial> implements IMesChiefdataReplacematerialService {

    @Autowired
    private MesChiefdataReplacematerialMapper mesChiefdataReplacematerialMapper;

    public boolean deleteMachineSort(String machineSort){
        return mesChiefdataReplacematerialMapper.deleteMachineSort(machineSort);
    }
}
