package org.jeecg.modules.mes.chiefdata.service;

import org.jeecg.modules.mes.chiefdata.entity.MesChiefdataSteelmesh;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 钢网建档
 * @Author: jeecg-boot
 * @Date:   2021-04-19
 * @Version: V1.0
 */
public interface IMesChiefdataSteelmeshService extends IService<MesChiefdataSteelmesh> {

    boolean increasePointUsageNum(String steelmeshSn);
}
