package org.jeecg.modules.mes.chiefdata.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.mes.chiefdata.entity.FacilityCheckTemp;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 设备检验项目模板
 * @Author: jeecg-boot
 * @Date:   2021-06-02
 * @Version: V1.0
 */
public interface FacilityCheckTempMapper extends BaseMapper<FacilityCheckTemp> {

}
