package org.jeecg.modules.mes.organize.controller;

import org.jeecg.common.system.query.QueryGenerator;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;
import org.jeecg.common.system.base.controller.JeecgController;
import org.jeecg.common.api.vo.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.web.servlet.ModelAndView;
import java.util.Arrays;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.mes.organize.entity.MesOrganizeHoliday;
import org.jeecg.modules.mes.organize.entity.MesOrganizeClasstype;
import org.jeecg.modules.mes.organize.entity.MesOrganizeCalendar;
import org.jeecg.modules.mes.organize.service.IMesOrganizeCalendarService;
import org.jeecg.modules.mes.organize.service.IMesOrganizeHolidayService;
import org.jeecg.modules.mes.organize.service.IMesOrganizeClasstypeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;
import org.apache.shiro.SecurityUtils;
import org.jeecg.common.system.vo.LoginUser;
import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

 /**
 * @Description: 组织—日历
 * @Author: jeecg-boot
 * @Date:   2020-10-19
 * @Version: V1.0
 */
@Api(tags="组织—日历")
@RestController
@RequestMapping("/organize/mesOrganizeCalendar")
@Slf4j
public class MesOrganizeCalendarController extends JeecgController<MesOrganizeCalendar, IMesOrganizeCalendarService> {

	@Autowired
	private IMesOrganizeCalendarService mesOrganizeCalendarService;

	@Autowired
	private IMesOrganizeHolidayService mesOrganizeHolidayService;

	@Autowired
	private IMesOrganizeClasstypeService mesOrganizeClasstypeService;


	/*---------------------------------主表处理-begin-------------------------------------*/

	/**
	 * 分页列表查询
	 * @param mesOrganizeCalendar
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "组织—日历-分页列表查询")
	@ApiOperation(value="组织—日历-分页列表查询", notes="组织—日历-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(MesOrganizeCalendar mesOrganizeCalendar,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<MesOrganizeCalendar> queryWrapper = QueryGenerator.initQueryWrapper(mesOrganizeCalendar, req.getParameterMap());
		Page<MesOrganizeCalendar> page = new Page<MesOrganizeCalendar>(pageNo, pageSize);
		IPage<MesOrganizeCalendar> pageList = mesOrganizeCalendarService.page(page, queryWrapper);
		return Result.ok(pageList);
	}

	/**
     *   添加
     * @param mesOrganizeCalendar
     * @return
     */
    @AutoLog(value = "组织—日历-添加")
    @ApiOperation(value="组织—日历-添加", notes="组织—日历-添加")
    @PostMapping(value = "/add")
    public Result<?> add(@RequestBody MesOrganizeCalendar mesOrganizeCalendar) {
        mesOrganizeCalendarService.save(mesOrganizeCalendar);
        return Result.ok("添加成功！");
    }

    /**
     *  编辑
     * @param mesOrganizeCalendar
     * @return
     */
    @AutoLog(value = "组织—日历-编辑")
    @ApiOperation(value="组织—日历-编辑", notes="组织—日历-编辑")
    @PutMapping(value = "/edit")
    public Result<?> edit(@RequestBody MesOrganizeCalendar mesOrganizeCalendar) {
        mesOrganizeCalendarService.updateById(mesOrganizeCalendar);
        return Result.ok("编辑成功!");
    }

    /**
     * 通过id删除
     * @param id
     * @return
     */
    @AutoLog(value = "组织—日历-通过id删除")
    @ApiOperation(value="组织—日历-通过id删除", notes="组织—日历-通过id删除")
    @DeleteMapping(value = "/delete")
    public Result<?> delete(@RequestParam(name="id",required=true) String id) {
        mesOrganizeCalendarService.delMain(id);
        return Result.ok("删除成功!");
    }

    /**
     * 批量删除
     * @param ids
     * @return
     */
    @AutoLog(value = "组织—日历-批量删除")
    @ApiOperation(value="组织—日历-批量删除", notes="组织—日历-批量删除")
    @DeleteMapping(value = "/deleteBatch")
    public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
        this.mesOrganizeCalendarService.delBatchMain(Arrays.asList(ids.split(",")));
        return Result.ok("批量删除成功!");
    }

    /**
     * 导出
     * @return
     */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, MesOrganizeCalendar mesOrganizeCalendar) {
        return super.exportXls(request, mesOrganizeCalendar, MesOrganizeCalendar.class, "组织—日历");
    }

    /**
     * 导入
     * @return
     */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, MesOrganizeCalendar.class);
    }
	/*---------------------------------主表处理-end-------------------------------------*/
	

    /*--------------------------------子表处理-组织—日历（假期）-begin----------------------------------------------*/
	/**
	 * 通过主表ID查询
	 * @return
	 */
	@AutoLog(value = "组织—日历（假期）-通过主表ID查询")
	@ApiOperation(value="组织—日历（假期）-通过主表ID查询", notes="组织—日历（假期）-通过主表ID查询")
	@GetMapping(value = "/listMesOrganizeHolidayByMainId")
    public Result<?> listMesOrganizeHolidayByMainId(MesOrganizeHoliday mesOrganizeHoliday,
                                                    @RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo,
                                                    @RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize,
                                                    HttpServletRequest req) {
        QueryWrapper<MesOrganizeHoliday> queryWrapper = QueryGenerator.initQueryWrapper(mesOrganizeHoliday, req.getParameterMap());
        Page<MesOrganizeHoliday> page = new Page<MesOrganizeHoliday>(pageNo, pageSize);
        IPage<MesOrganizeHoliday> pageList = mesOrganizeHolidayService.page(page, queryWrapper);
        return Result.ok(pageList);
    }

	/**
	 * 添加
	 * @param mesOrganizeHoliday
	 * @return
	 */
	@AutoLog(value = "组织—日历（假期）-添加")
	@ApiOperation(value="组织—日历（假期）-添加", notes="组织—日历（假期）-添加")
	@PostMapping(value = "/addMesOrganizeHoliday")
	public Result<?> addMesOrganizeHoliday(@RequestBody MesOrganizeHoliday mesOrganizeHoliday) {
		mesOrganizeHolidayService.save(mesOrganizeHoliday);
		return Result.ok("添加成功！");
	}

    /**
	 * 编辑
	 * @param mesOrganizeHoliday
	 * @return
	 */
	@AutoLog(value = "组织—日历（假期）-编辑")
	@ApiOperation(value="组织—日历（假期）-编辑", notes="组织—日历（假期）-编辑")
	@PutMapping(value = "/editMesOrganizeHoliday")
	public Result<?> editMesOrganizeHoliday(@RequestBody MesOrganizeHoliday mesOrganizeHoliday) {
		mesOrganizeHolidayService.updateById(mesOrganizeHoliday);
		return Result.ok("编辑成功!");
	}

	/**
	 * 通过id删除
	 * @param id
	 * @return
	 */
	@AutoLog(value = "组织—日历（假期）-通过id删除")
	@ApiOperation(value="组织—日历（假期）-通过id删除", notes="组织—日历（假期）-通过id删除")
	@DeleteMapping(value = "/deleteMesOrganizeHoliday")
	public Result<?> deleteMesOrganizeHoliday(@RequestParam(name="id",required=true) String id) {
		mesOrganizeHolidayService.removeById(id);
		return Result.ok("删除成功!");
	}

	/**
	 * 批量删除
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "组织—日历（假期）-批量删除")
	@ApiOperation(value="组织—日历（假期）-批量删除", notes="组织—日历（假期）-批量删除")
	@DeleteMapping(value = "/deleteBatchMesOrganizeHoliday")
	public Result<?> deleteBatchMesOrganizeHoliday(@RequestParam(name="ids",required=true) String ids) {
	    this.mesOrganizeHolidayService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.ok("批量删除成功!");
	}

    /**
     * 导出
     * @return
     */
    @RequestMapping(value = "/exportMesOrganizeHoliday")
    public ModelAndView exportMesOrganizeHoliday(HttpServletRequest request, MesOrganizeHoliday mesOrganizeHoliday) {
		 // Step.1 组装查询条件
		 QueryWrapper<MesOrganizeHoliday> queryWrapper = QueryGenerator.initQueryWrapper(mesOrganizeHoliday, request.getParameterMap());
		 LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();

		 // Step.2 获取导出数据
		 List<MesOrganizeHoliday> pageList = mesOrganizeHolidayService.list(queryWrapper);
		 List<MesOrganizeHoliday> exportList = null;

		 // 过滤选中数据
		 String selections = request.getParameter("selections");
		 if (oConvertUtils.isNotEmpty(selections)) {
			 List<String> selectionList = Arrays.asList(selections.split(","));
			 exportList = pageList.stream().filter(item -> selectionList.contains(item.getId())).collect(Collectors.toList());
		 } else {
			 exportList = pageList;
		 }

		 // Step.3 AutoPoi 导出Excel
		 ModelAndView mv = new ModelAndView(new JeecgEntityExcelView());
		 mv.addObject(NormalExcelConstants.FILE_NAME, "组织—日历（假期）"); //此处设置的filename无效 ,前端会重更新设置一下
		 mv.addObject(NormalExcelConstants.CLASS, MesOrganizeHoliday.class);
		 mv.addObject(NormalExcelConstants.PARAMS, new ExportParams("组织—日历（假期）报表", "导出人:" + sysUser.getRealname(), "组织—日历（假期）"));
		 mv.addObject(NormalExcelConstants.DATA_LIST, exportList);
		 return mv;
    }

    /**
     * 导入
     * @return
     */
    @RequestMapping(value = "/importMesOrganizeHoliday/{mainId}")
    public Result<?> importMesOrganizeHoliday(HttpServletRequest request, HttpServletResponse response, @PathVariable("mainId") String mainId) {
		 MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
		 Map<String, MultipartFile> fileMap = multipartRequest.getFileMap();
		 for (Map.Entry<String, MultipartFile> entity : fileMap.entrySet()) {
			 MultipartFile file = entity.getValue();// 获取上传文件对象
			 ImportParams params = new ImportParams();
			 params.setTitleRows(2);
			 params.setHeadRows(1);
			 params.setNeedSave(true);
			 try {
				 List<MesOrganizeHoliday> list = ExcelImportUtil.importExcel(file.getInputStream(), MesOrganizeHoliday.class, params);
				 for (MesOrganizeHoliday temp : list) {
                    temp.setCalId(mainId);
				 }
				 long start = System.currentTimeMillis();
				 mesOrganizeHolidayService.saveBatch(list);
				 log.info("消耗时间" + (System.currentTimeMillis() - start) + "毫秒");
				 return Result.ok("文件导入成功！数据行数：" + list.size());
			 } catch (Exception e) {
				 log.error(e.getMessage(), e);
				 return Result.error("文件导入失败:" + e.getMessage());
			 } finally {
				 try {
					 file.getInputStream().close();
				 } catch (IOException e) {
					 e.printStackTrace();
				 }
			 }
		 }
		 return Result.error("文件导入失败！");
    }

    /*--------------------------------子表处理-组织—日历（假期）-end----------------------------------------------*/

    /*--------------------------------子表处理-组织—日历（班别）-begin----------------------------------------------*/
	/**
	 * 通过主表ID查询
	 * @return
	 */
	@AutoLog(value = "组织—日历（班别）-通过主表ID查询")
	@ApiOperation(value="组织—日历（班别）-通过主表ID查询", notes="组织—日历（班别）-通过主表ID查询")
	@GetMapping(value = "/listMesOrganizeClasstypeByMainId")
    public Result<?> listMesOrganizeClasstypeByMainId(MesOrganizeClasstype mesOrganizeClasstype,
                                                    @RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo,
                                                    @RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize,
                                                    HttpServletRequest req) {
        QueryWrapper<MesOrganizeClasstype> queryWrapper = QueryGenerator.initQueryWrapper(mesOrganizeClasstype, req.getParameterMap());
        Page<MesOrganizeClasstype> page = new Page<MesOrganizeClasstype>(pageNo, pageSize);
        IPage<MesOrganizeClasstype> pageList = mesOrganizeClasstypeService.page(page, queryWrapper);
        return Result.ok(pageList);
    }

	/**
	 * 添加
	 * @param mesOrganizeClasstype
	 * @return
	 */
	@AutoLog(value = "组织—日历（班别）-添加")
	@ApiOperation(value="组织—日历（班别）-添加", notes="组织—日历（班别）-添加")
	@PostMapping(value = "/addMesOrganizeClasstype")
	public Result<?> addMesOrganizeClasstype(@RequestBody MesOrganizeClasstype mesOrganizeClasstype) {
		mesOrganizeClasstypeService.save(mesOrganizeClasstype);
		return Result.ok("添加成功！");
	}

    /**
	 * 编辑
	 * @param mesOrganizeClasstype
	 * @return
	 */
	@AutoLog(value = "组织—日历（班别）-编辑")
	@ApiOperation(value="组织—日历（班别）-编辑", notes="组织—日历（班别）-编辑")
	@PutMapping(value = "/editMesOrganizeClasstype")
	public Result<?> editMesOrganizeClasstype(@RequestBody MesOrganizeClasstype mesOrganizeClasstype) {
		mesOrganizeClasstypeService.updateById(mesOrganizeClasstype);
		return Result.ok("编辑成功!");
	}

	/**
	 * 通过id删除
	 * @param id
	 * @return
	 */
	@AutoLog(value = "组织—日历（班别）-通过id删除")
	@ApiOperation(value="组织—日历（班别）-通过id删除", notes="组织—日历（班别）-通过id删除")
	@DeleteMapping(value = "/deleteMesOrganizeClasstype")
	public Result<?> deleteMesOrganizeClasstype(@RequestParam(name="id",required=true) String id) {
		mesOrganizeClasstypeService.removeById(id);
		return Result.ok("删除成功!");
	}

	/**
	 * 批量删除
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "组织—日历（班别）-批量删除")
	@ApiOperation(value="组织—日历（班别）-批量删除", notes="组织—日历（班别）-批量删除")
	@DeleteMapping(value = "/deleteBatchMesOrganizeClasstype")
	public Result<?> deleteBatchMesOrganizeClasstype(@RequestParam(name="ids",required=true) String ids) {
	    this.mesOrganizeClasstypeService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.ok("批量删除成功!");
	}

    /**
     * 导出
     * @return
     */
    @RequestMapping(value = "/exportMesOrganizeClasstype")
    public ModelAndView exportMesOrganizeClasstype(HttpServletRequest request, MesOrganizeClasstype mesOrganizeClasstype) {
		 // Step.1 组装查询条件
		 QueryWrapper<MesOrganizeClasstype> queryWrapper = QueryGenerator.initQueryWrapper(mesOrganizeClasstype, request.getParameterMap());
		 LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();

		 // Step.2 获取导出数据
		 List<MesOrganizeClasstype> pageList = mesOrganizeClasstypeService.list(queryWrapper);
		 List<MesOrganizeClasstype> exportList = null;

		 // 过滤选中数据
		 String selections = request.getParameter("selections");
		 if (oConvertUtils.isNotEmpty(selections)) {
			 List<String> selectionList = Arrays.asList(selections.split(","));
			 exportList = pageList.stream().filter(item -> selectionList.contains(item.getId())).collect(Collectors.toList());
		 } else {
			 exportList = pageList;
		 }

		 // Step.3 AutoPoi 导出Excel
		 ModelAndView mv = new ModelAndView(new JeecgEntityExcelView());
		 mv.addObject(NormalExcelConstants.FILE_NAME, "组织—日历（班别）"); //此处设置的filename无效 ,前端会重更新设置一下
		 mv.addObject(NormalExcelConstants.CLASS, MesOrganizeClasstype.class);
		 mv.addObject(NormalExcelConstants.PARAMS, new ExportParams("组织—日历（班别）报表", "导出人:" + sysUser.getRealname(), "组织—日历（班别）"));
		 mv.addObject(NormalExcelConstants.DATA_LIST, exportList);
		 return mv;
    }

    /**
     * 导入
     * @return
     */
    @RequestMapping(value = "/importMesOrganizeClasstype/{mainId}")
    public Result<?> importMesOrganizeClasstype(HttpServletRequest request, HttpServletResponse response, @PathVariable("mainId") String mainId) {
		 MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
		 Map<String, MultipartFile> fileMap = multipartRequest.getFileMap();
		 for (Map.Entry<String, MultipartFile> entity : fileMap.entrySet()) {
			 MultipartFile file = entity.getValue();// 获取上传文件对象
			 ImportParams params = new ImportParams();
			 params.setTitleRows(2);
			 params.setHeadRows(1);
			 params.setNeedSave(true);
			 try {
				 List<MesOrganizeClasstype> list = ExcelImportUtil.importExcel(file.getInputStream(), MesOrganizeClasstype.class, params);
				 for (MesOrganizeClasstype temp : list) {
                    temp.setCalendarId(mainId);
				 }
				 long start = System.currentTimeMillis();
				 mesOrganizeClasstypeService.saveBatch(list);
				 log.info("消耗时间" + (System.currentTimeMillis() - start) + "毫秒");
				 return Result.ok("文件导入成功！数据行数：" + list.size());
			 } catch (Exception e) {
				 log.error(e.getMessage(), e);
				 return Result.error("文件导入失败:" + e.getMessage());
			 } finally {
				 try {
					 file.getInputStream().close();
				 } catch (IOException e) {
					 e.printStackTrace();
				 }
			 }
		 }
		 return Result.error("文件导入失败！");
    }

    /*--------------------------------子表处理-组织—日历（班别）-end----------------------------------------------*/




}
