package org.jeecg.modules.mes.chiefdata.service.impl;

import org.apache.logging.log4j.util.Strings;
import org.jeecg.modules.mes.chiefdata.entity.MesChiefdataMateriel;
import org.jeecg.modules.mes.chiefdata.entity.MesMaterielPcb;
import org.jeecg.modules.mes.chiefdata.entity.MesMaterielNetknife;
import org.jeecg.modules.mes.chiefdata.entity.MesMaterielProduce;
import org.jeecg.modules.mes.chiefdata.entity.MesMaterielRedgum;
import org.jeecg.modules.mes.chiefdata.entity.MesMaterielQuality;
import org.jeecg.modules.mes.chiefdata.entity.MesMaterielStorage;
import org.jeecg.modules.mes.chiefdata.entity.MesMaterielOther;
import org.jeecg.modules.mes.chiefdata.mapper.MesMaterielPcbMapper;
import org.jeecg.modules.mes.chiefdata.mapper.MesMaterielNetknifeMapper;
import org.jeecg.modules.mes.chiefdata.mapper.MesMaterielProduceMapper;
import org.jeecg.modules.mes.chiefdata.mapper.MesMaterielRedgumMapper;
import org.jeecg.modules.mes.chiefdata.mapper.MesMaterielQualityMapper;
import org.jeecg.modules.mes.chiefdata.mapper.MesMaterielStorageMapper;
import org.jeecg.modules.mes.chiefdata.mapper.MesMaterielOtherMapper;
import org.jeecg.modules.mes.chiefdata.mapper.MesChiefdataMaterielMapper;
import org.jeecg.modules.mes.chiefdata.service.IMesChiefdataMaterielService;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import java.io.Serializable;
import java.util.List;
import java.util.Collection;

/**
 * @Description: 主数据—物料
 * @Author: jeecg-boot
 * @Date:   2020-10-16
 * @Version: V1.0
 */
@Service
public class MesChiefdataMaterielServiceImpl extends ServiceImpl<MesChiefdataMaterielMapper, MesChiefdataMateriel> implements IMesChiefdataMaterielService {

	@Autowired
	private MesChiefdataMaterielMapper mesChiefdataMaterielMapper;
	@Autowired
	private MesMaterielPcbMapper mesMaterielPcbMapper;
	@Autowired
	private MesMaterielNetknifeMapper mesMaterielNetknifeMapper;
	@Autowired
	private MesMaterielProduceMapper mesMaterielProduceMapper;
	@Autowired
	private MesMaterielRedgumMapper mesMaterielRedgumMapper;
	@Autowired
	private MesMaterielQualityMapper mesMaterielQualityMapper;
	@Autowired
	private MesMaterielStorageMapper mesMaterielStorageMapper;
	@Autowired
	private MesMaterielOtherMapper mesMaterielOtherMapper;
	
	@Override
	@Transactional
	public void saveMain(MesChiefdataMateriel mesChiefdataMateriel, List<MesMaterielPcb> mesMaterielPcbList,List<MesMaterielNetknife> mesMaterielNetknifeList,List<MesMaterielProduce> mesMaterielProduceList,List<MesMaterielRedgum> mesMaterielRedgumList,List<MesMaterielQuality> mesMaterielQualityList,List<MesMaterielStorage> mesMaterielStorageList,List<MesMaterielOther> mesMaterielOtherList) {
		//处理料号首尾空格
		if (Strings.isNotBlank(mesChiefdataMateriel.getMaterielCode())) {
			String tmCode = mesChiefdataMateriel.getMaterielCode().trim();
			mesChiefdataMateriel.setMaterielCode(tmCode);
		}
		mesChiefdataMaterielMapper.insert(mesChiefdataMateriel);
		if (mesMaterielPcbList != null && mesMaterielPcbList.size() > 0) {
			for (MesMaterielPcb entity : mesMaterielPcbList) {
				//外键设置
				entity.setMaterielId(mesChiefdataMateriel.getId());
				mesMaterielPcbMapper.insert(entity);
			}
		}
		if (mesMaterielNetknifeList != null && mesMaterielNetknifeList.size() > 0) {
			for (MesMaterielNetknife entity : mesMaterielNetknifeList) {
				//外键设置
				entity.setMaterielId(mesChiefdataMateriel.getId());
				mesMaterielNetknifeMapper.insert(entity);
			}
		}
		if(mesMaterielProduceList!=null && mesMaterielProduceList.size()>0) {
			for(MesMaterielProduce entity:mesMaterielProduceList) {
				//外键设置
				entity.setMaterielId(mesChiefdataMateriel.getId());
				mesMaterielProduceMapper.insert(entity);
			}
		}
		if(mesMaterielRedgumList!=null && mesMaterielRedgumList.size()>0) {
			for(MesMaterielRedgum entity:mesMaterielRedgumList) {
				//外键设置
				entity.setMaterielId(mesChiefdataMateriel.getId());
				mesMaterielRedgumMapper.insert(entity);
			}
		}
		if(mesMaterielQualityList!=null && mesMaterielQualityList.size()>0) {
			for(MesMaterielQuality entity:mesMaterielQualityList) {
				//外键设置
				entity.setMaterielId(mesChiefdataMateriel.getId());
				mesMaterielQualityMapper.insert(entity);
			}
		}
		if(mesMaterielStorageList!=null && mesMaterielStorageList.size()>0) {
			for(MesMaterielStorage entity:mesMaterielStorageList) {
				//外键设置
				entity.setMaterielId(mesChiefdataMateriel.getId());
				mesMaterielStorageMapper.insert(entity);
			}
		}
		if(mesMaterielOtherList!=null && mesMaterielOtherList.size()>0) {
			for(MesMaterielOther entity:mesMaterielOtherList) {
				//外键设置
				entity.setMaterielId(mesChiefdataMateriel.getId());
				mesMaterielOtherMapper.insert(entity);
			}
		}
	}

	@Override
	@Transactional
	public void updateMain(MesChiefdataMateriel mesChiefdataMateriel,List<MesMaterielPcb> mesMaterielPcbList,List<MesMaterielNetknife> mesMaterielNetknifeList,List<MesMaterielProduce> mesMaterielProduceList,List<MesMaterielRedgum> mesMaterielRedgumList,List<MesMaterielQuality> mesMaterielQualityList,List<MesMaterielStorage> mesMaterielStorageList,List<MesMaterielOther> mesMaterielOtherList) {
		//处理料号首尾空格
		if (Strings.isNotBlank(mesChiefdataMateriel.getMaterielCode())) {
			String tmCode = mesChiefdataMateriel.getMaterielCode().trim();
			mesChiefdataMateriel.setMaterielCode(tmCode);
		}
		mesChiefdataMaterielMapper.updateById(mesChiefdataMateriel);

		//1.先删除子表数据
		mesMaterielPcbMapper.deleteByMainId(mesChiefdataMateriel.getId());
		mesMaterielNetknifeMapper.deleteByMainId(mesChiefdataMateriel.getId());
		mesMaterielProduceMapper.deleteByMainId(mesChiefdataMateriel.getId());
		mesMaterielRedgumMapper.deleteByMainId(mesChiefdataMateriel.getId());
		mesMaterielQualityMapper.deleteByMainId(mesChiefdataMateriel.getId());
		mesMaterielStorageMapper.deleteByMainId(mesChiefdataMateriel.getId());
		mesMaterielOtherMapper.deleteByMainId(mesChiefdataMateriel.getId());
		
		//2.子表数据重新插入
		if((mesMaterielPcbList!=null && mesMaterielPcbList.size()>0)) {
			for(MesMaterielPcb entity:mesMaterielPcbList) {
				//外键设置
				entity.setMaterielId(mesChiefdataMateriel.getId());
				mesMaterielPcbMapper.insert(entity);
			}
		}
		if(mesMaterielNetknifeList!=null && mesMaterielNetknifeList.size()>0) {
			for(MesMaterielNetknife entity:mesMaterielNetknifeList) {
				//外键设置
				entity.setMaterielId(mesChiefdataMateriel.getId());
				mesMaterielNetknifeMapper.insert(entity);
			}
		}
		if(mesMaterielProduceList!=null && mesMaterielProduceList.size()>0) {
			for(MesMaterielProduce entity:mesMaterielProduceList) {
				//外键设置
				entity.setMaterielId(mesChiefdataMateriel.getId());
				mesMaterielProduceMapper.insert(entity);
			}
		}
		if(mesMaterielRedgumList!=null && mesMaterielRedgumList.size()>0) {
			for(MesMaterielRedgum entity:mesMaterielRedgumList) {
				//外键设置
				entity.setMaterielId(mesChiefdataMateriel.getId());
				mesMaterielRedgumMapper.insert(entity);
			}
		}
		if(mesMaterielQualityList!=null && mesMaterielQualityList.size()>0) {
			for(MesMaterielQuality entity:mesMaterielQualityList) {
				//外键设置
				entity.setMaterielId(mesChiefdataMateriel.getId());
				mesMaterielQualityMapper.insert(entity);
			}
		}
		if(mesMaterielStorageList!=null && mesMaterielStorageList.size()>0) {
			for(MesMaterielStorage entity:mesMaterielStorageList) {
				//外键设置
				entity.setMaterielId(mesChiefdataMateriel.getId());
				mesMaterielStorageMapper.insert(entity);
			}
		}
		if(mesMaterielOtherList!=null && mesMaterielOtherList.size()>0) {
			for(MesMaterielOther entity:mesMaterielOtherList) {
				//外键设置
				entity.setMaterielId(mesChiefdataMateriel.getId());
				mesMaterielOtherMapper.insert(entity);
			}
		}
	}

	@Override
	@Transactional
	public void delMain(String id) {
		mesMaterielPcbMapper.deleteByMainId(id);
		mesMaterielNetknifeMapper.deleteByMainId(id);
		mesMaterielProduceMapper.deleteByMainId(id);
		mesMaterielRedgumMapper.deleteByMainId(id);
		mesMaterielQualityMapper.deleteByMainId(id);
		mesMaterielStorageMapper.deleteByMainId(id);
		mesMaterielOtherMapper.deleteByMainId(id);
		mesChiefdataMaterielMapper.deleteById(id);
	}

	@Override
	@Transactional
	public void delBatchMain(Collection<? extends Serializable> idList) {
		for(Serializable id:idList) {
			mesMaterielPcbMapper.deleteByMainId(id.toString());
			mesMaterielNetknifeMapper.deleteByMainId(id.toString());
			mesMaterielProduceMapper.deleteByMainId(id.toString());
			mesMaterielRedgumMapper.deleteByMainId(id.toString());
			mesMaterielQualityMapper.deleteByMainId(id.toString());
			mesMaterielStorageMapper.deleteByMainId(id.toString());
			mesMaterielOtherMapper.deleteByMainId(id.toString());
			mesChiefdataMaterielMapper.deleteById(id);
		}
	}
	
}
