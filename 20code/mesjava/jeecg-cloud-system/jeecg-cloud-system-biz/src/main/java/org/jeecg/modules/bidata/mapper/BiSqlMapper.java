package org.jeecg.modules.bidata.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.bidata.entity.BiSql;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 数据配置接口
 * @Author: jeecg-boot
 * @Date:   2020-09-09
 * @Version: V1.0
 */
public interface BiSqlMapper extends BaseMapper<BiSql> {

}
