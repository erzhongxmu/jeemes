package org.jeecg.modules.mes.chiefdata.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import net.sf.saxon.expr.Component;
import org.apache.logging.log4j.util.Strings;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.mes.chiefdata.entity.MesChiefdataAncillarytool;
import org.jeecg.modules.mes.chiefdata.entity.MesChiefdataMateriel;
import org.jeecg.modules.mes.chiefdata.entity.MesMaterielRedgum;
import org.jeecg.modules.mes.chiefdata.service.IMesChiefdataAncillarytoolService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecg.modules.mes.chiefdata.service.IMesChiefdataMaterielService;
import org.jeecg.modules.mes.chiefdata.service.IMesMaterielRedgumService;
import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

/**
 * @Description: 主数据—辅料信息
 * @Author: jeecg-boot
 * @Date: 2020-09-17
 * @Version: V1.0
 */
@Api(tags = "主数据—辅料信息")
@RestController
@RequestMapping("/chiefdata/mesChiefdataAncillarytool")
@Slf4j
public class MesChiefdataAncillarytoolController extends JeecgController<MesChiefdataAncillarytool, IMesChiefdataAncillarytoolService> {
    @Autowired
    private IMesChiefdataAncillarytoolService mesChiefdataAncillarytoolService;
    @Autowired
    private IMesChiefdataMaterielService mesChiefdataMaterielService;
    @Autowired
    private IMesMaterielRedgumService mesMaterielRedgumService;


    /**
     * @param materielId 辅料id
     * @return 物料信息
     * @description 根据批号查询物料信息
     * @Date 2021/3/6 11:08
     * @author liuhongshen
     **/
    @GetMapping("/getByProduceCode")
    @ApiOperation("根据辅料id查询物料信息")
    public Result<?> getByProduceCode(@RequestParam("materielId") String materielId) {
        if (Strings.isBlank(materielId)) {
            return Result.error("批号不能为空");
        }
        LambdaQueryWrapper<MesMaterielRedgum> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(MesMaterielRedgum::getMaterielId, materielId);
        MesMaterielRedgum mTool = mesMaterielRedgumService.getOne(wrapper);
        MesChiefdataAncillarytool byId = mesChiefdataAncillarytoolService.getById(materielId);
        mTool.setAncillaryName(byId.getAncillaryName());
        mTool.setAncillaryCode(byId.getAncillaryCode());
        mTool.setAncillaryType(byId.getAncillaryType());
        mTool.setAncillaryGague(byId.getAncillaryGague());
        return Result.ok(mTool);
    }

    /**
     * 分页列表查询
     *
     * @param mesChiefdataAncillarytool
     * @param pageNo
     * @param pageSize
     * @param req
     * @return
     */
    @AutoLog(value = "主数据—辅料信息-分页列表查询")
    @ApiOperation(value = "主数据—辅料信息-分页列表查询", notes = "主数据—辅料信息-分页列表查询")
    @GetMapping(value = "/list")
    public Result<?> queryPageList(MesChiefdataAncillarytool mesChiefdataAncillarytool,
                                   @RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo,
                                   @RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize,
                                   HttpServletRequest req) {
        QueryWrapper<MesChiefdataAncillarytool> queryWrapper = QueryGenerator.initQueryWrapper(mesChiefdataAncillarytool, req.getParameterMap());
        Page<MesChiefdataAncillarytool> page = new Page<MesChiefdataAncillarytool>(pageNo, pageSize);
        IPage<MesChiefdataAncillarytool> pageList = mesChiefdataAncillarytoolService.page(page, queryWrapper);
        return Result.ok(pageList);
    }


    @GetMapping(value = "/Ancillarytooladd")
    public String Ancillarytooladd(@RequestParam(name = "mCode", required = true) String mCode) {
        MesChiefdataAncillarytool mesChiefdataAncillarytool = new MesChiefdataAncillarytool();
        QueryWrapper<MesChiefdataMateriel> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("materiel_code", mCode);
        MesChiefdataMateriel materiel = mesChiefdataMaterielService.getOne(queryWrapper);

        QueryWrapper<MesMaterielRedgum> wrapper = new QueryWrapper<>();
        wrapper.eq("materiel_id", materiel.getId());
        MesMaterielRedgum redgum = mesMaterielRedgumService.getOne(wrapper);

        mesChiefdataAncillarytool.setAncillaryCode(mCode);
        mesChiefdataAncillarytool.setAncillaryName(materiel.getProductName());
        mesChiefdataAncillarytool.setAncillaryGague(materiel.getGauge());
        mesChiefdataAncillarytool.setSupplier(materiel.getSupplier());
        mesChiefdataAncillarytool.setAncillaryType(materiel.getMaterielType());
        mesChiefdataAncillarytool.setRohsToken(redgum.getRohs());
        mesChiefdataAncillarytool.setShelfLife(redgum.getShelfDay());
        mesChiefdataAncillarytool.setCrossStovetime(redgum.getCrossStove());
        mesChiefdataAncillarytool.setHeatTime(redgum.getHeatTime());
        mesChiefdataAncillarytool.setOpenShelflife(redgum.getOpenShelfday());
        mesChiefdataAncillarytool.setHeatTimelimit(redgum.getHeatLimit());
        mesChiefdataAncillarytool.setHeatNumlimit(redgum.getHeatNum());
        mesChiefdataAncillarytool.setHeatDecline(redgum.getHeatReduce());
        mesChiefdataAncillarytool.setStirTime(redgum.getStirTime());
        mesChiefdataAncillarytool.setStirTimelimit(redgum.getStirLimit());
        mesChiefdataAncillarytoolService.save(mesChiefdataAncillarytool);
        return "添加成功！";
    }

    /**
     * 添加
     *
     * @param mesChiefdataAncillarytool
     * @return
     */
    @AutoLog(value = "主数据—辅料信息-添加")
    @ApiOperation(value = "主数据—辅料信息-添加", notes = "主数据—辅料信息-添加")
    @PostMapping(value = "/add")
    public Result<?> add(@RequestBody MesChiefdataAncillarytool mesChiefdataAncillarytool) {
        mesChiefdataAncillarytoolService.save(mesChiefdataAncillarytool);
        return Result.ok("添加成功！");
    }

    /**
     * 编辑
     *
     * @param mesChiefdataAncillarytool
     * @return
     */
    @AutoLog(value = "主数据—辅料信息-编辑")
    @ApiOperation(value = "主数据—辅料信息-编辑", notes = "主数据—辅料信息-编辑")
    @PutMapping(value = "/edit")
    public Result<?> edit(@RequestBody MesChiefdataAncillarytool mesChiefdataAncillarytool) {
        mesChiefdataAncillarytoolService.updateById(mesChiefdataAncillarytool);
        return Result.ok("编辑成功!");
    }

    /**
     * 通过id删除
     *
     * @param id
     * @return
     */
    @AutoLog(value = "主数据—辅料信息-通过id删除")
    @ApiOperation(value = "主数据—辅料信息-通过id删除", notes = "主数据—辅料信息-通过id删除")
    @DeleteMapping(value = "/delete")
    public Result<?> delete(@RequestParam(name = "id", required = true) String id) {
        mesChiefdataAncillarytoolService.removeById(id);
        return Result.ok("删除成功!");
    }

    /**
     * 批量删除
     *
     * @param ids
     * @return
     */
    @AutoLog(value = "主数据—辅料信息-批量删除")
    @ApiOperation(value = "主数据—辅料信息-批量删除", notes = "主数据—辅料信息-批量删除")
    @DeleteMapping(value = "/deleteBatch")
    public Result<?> deleteBatch(@RequestParam(name = "ids", required = true) String ids) {
        this.mesChiefdataAncillarytoolService.removeByIds(Arrays.asList(ids.split(",")));
        return Result.ok("批量删除成功!");
    }

    /**
     * 通过id查询
     *
     * @param id
     * @return
     */
    @AutoLog(value = "主数据—辅料信息-通过id查询")
    @ApiOperation(value = "主数据—辅料信息-通过id查询", notes = "主数据—辅料信息-通过id查询")
    @GetMapping(value = "/queryById")
    public Result<?> queryById(@RequestParam(name = "id", required = true) String id) {
        MesChiefdataAncillarytool mesChiefdataAncillarytool = mesChiefdataAncillarytoolService.getById(id);
        if (mesChiefdataAncillarytool == null) {
            return Result.error("未找到对应数据");
        }
        return Result.ok(mesChiefdataAncillarytool);
    }

    /**
     * 通过pCode查询
     *
     * @param pCode
     * @return
     */
    @AutoLog(value = "主数据—辅料信息-通过料号查询")
    @ApiOperation(value = "主数据—辅料信息-通过料号查询", notes = "主数据—辅料信息-通过料号查询")
    @GetMapping(value = "/queryBypCode")
    public MesChiefdataAncillarytool queryBypCode(@RequestParam(name = "pCode") String pCode) {
        LambdaQueryWrapper<MesChiefdataAncillarytool> tools = new LambdaQueryWrapper<>();
        tools.eq(MesChiefdataAncillarytool::getAncillaryCode, pCode);
        MesChiefdataAncillarytool mesChiefdataAncillarytool = mesChiefdataAncillarytoolService.getOne(tools);
        if (mesChiefdataAncillarytool == null) {
            return null;
        }
        return mesChiefdataAncillarytool;
    }

    /**
     * 导出excel
     *
     * @param request
     * @param mesChiefdataAncillarytool
     */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, MesChiefdataAncillarytool mesChiefdataAncillarytool) {
        return super.exportXls(request, mesChiefdataAncillarytool, MesChiefdataAncillarytool.class, "主数据—辅料信息");
    }

    /**
     * 通过excel导入数据
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, MesChiefdataAncillarytool.class);
    }

}
