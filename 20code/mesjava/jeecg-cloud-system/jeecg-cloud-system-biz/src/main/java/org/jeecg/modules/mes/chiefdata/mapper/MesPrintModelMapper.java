package org.jeecg.modules.mes.chiefdata.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.mes.chiefdata.entity.MesPrintModel;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 主数据—打印模版
 * @Author: jeecg-boot
 * @Date:   2020-10-15
 * @Version: V1.0
 */
public interface MesPrintModelMapper extends BaseMapper<MesPrintModel> {

}
