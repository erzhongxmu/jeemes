package org.jeecg.modules.mes.client;

import org.jeecg.common.constant.ServiceNameConstants;
import org.jeecg.modules.mes.order.entity.MesOrderProduce;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

/**
 * @Package org.jeecg.modules.mes.client
 * @date 2021/4/22 22:39
 * @description
 */

@Component
@FeignClient(contextId = "TransactionServiceClient",value = ServiceNameConstants.TRANSACTION_SERVICE,qualifier = "transactionService")
public interface TransactionClient {

    @GetMapping("/order/mesOrderProduce/getPlanedOrderProduceList")
    public List<MesOrderProduce> getPlanedOrderProduceList();
}
