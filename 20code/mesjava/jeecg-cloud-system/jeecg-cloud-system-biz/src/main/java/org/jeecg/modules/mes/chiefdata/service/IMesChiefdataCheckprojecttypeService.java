package org.jeecg.modules.mes.chiefdata.service;

import org.jeecg.modules.mes.chiefdata.entity.MesChiefdataCheckprojecttype;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 主数据—检测项目类型
 * @Author: jeecg-boot
 * @Date:   2020-09-17
 * @Version: V1.0
 */
public interface IMesChiefdataCheckprojecttypeService extends IService<MesChiefdataCheckprojecttype> {

}
