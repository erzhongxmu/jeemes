package org.jeecg.modules.mes.chiefdata.service;

import org.jeecg.modules.mes.chiefdata.entity.MesChiefdataDevicearchive;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 主数据—设备建档
 * @Author: jeecg-boot
 * @Date:   2020-10-16
 * @Version: V1.0
 */
public interface IMesChiefdataDevicearchiveService extends IService<MesChiefdataDevicearchive> {

    MesChiefdataDevicearchive queryByDeviceSn(String deviceSn);
}
