package org.jeecg.modules.mes.chiefdata.mapper;

import java.util.List;
import org.jeecg.modules.mes.chiefdata.entity.MesMaterielNetknife;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * @Description: 主数据—辅料-钢网/刮刀
 * @Author: jeecg-boot
 * @Date:   2020-10-16
 * @Version: V1.0
 */
public interface MesMaterielNetknifeMapper extends BaseMapper<MesMaterielNetknife> {

	public boolean deleteByMainId(@Param("mainId") String mainId);
    
	public List<MesMaterielNetknife> selectByMainId(@Param("mainId") String mainId);
}
