package org.jeecg.modules.mes.chiefdata.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.aspect.annotation.AutoLog;
import org.jeecg.common.system.base.controller.JeecgController;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.modules.mes.chiefdata.entity.FacilityCheckTemp;
import org.jeecg.modules.mes.chiefdata.service.IFacilityCheckTempService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;
import java.util.List;

/**
 * @Description: 设备检验项目模板
 * @Author: jeecg-boot
 * @Date: 2021-06-02
 * @Version: V1.0
 */
@Api(tags = "设备检验项目模板")
@RestController
@RequestMapping("/chiefdata/facilityCheckTemp")
@Slf4j
public class FacilityCheckTempController extends JeecgController<FacilityCheckTemp, IFacilityCheckTempService> {
    @Autowired
    private IFacilityCheckTempService facilityCheckTempService;

    /**
     * 分页列表查询
     *
     * @param facilityCheckTemp
     * @param pageNo
     * @param pageSize
     * @param req
     * @return
     */
    @AutoLog(value = "设备检验项目模板-分页列表查询")
    @ApiOperation(value = "设备检验项目模板-分页列表查询", notes = "设备检验项目模板-分页列表查询")
    @GetMapping(value = "/list")
    public Result<?> queryPageList(FacilityCheckTemp facilityCheckTemp,
                                   @RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo,
                                   @RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize,
                                   HttpServletRequest req) {
        QueryWrapper<FacilityCheckTemp> queryWrapper = QueryGenerator.initQueryWrapper(facilityCheckTemp, req.getParameterMap());
        Page<FacilityCheckTemp> page = new Page<FacilityCheckTemp>(pageNo, pageSize);
        IPage<FacilityCheckTemp> pageList = facilityCheckTempService.page(page, queryWrapper);
        return Result.ok(pageList);
    }

    /**
     * 添加
     *
     * @param facilityCheckTemp
     * @return
     */
    @AutoLog(value = "设备检验项目模板-添加")
    @ApiOperation(value = "设备检验项目模板-添加", notes = "设备检验项目模板-添加")
    @PostMapping(value = "/add")
    public Result<?> add(@RequestBody FacilityCheckTemp facilityCheckTemp) {
        facilityCheckTempService.save(facilityCheckTemp);
        return Result.ok("添加成功！");
    }

    /**
     * 编辑
     *
     * @param facilityCheckTemp
     * @return
     */
    @AutoLog(value = "设备检验项目模板-编辑")
    @ApiOperation(value = "设备检验项目模板-编辑", notes = "设备检验项目模板-编辑")
    @PutMapping(value = "/edit")
    public Result<?> edit(@RequestBody FacilityCheckTemp facilityCheckTemp) {
        facilityCheckTempService.updateById(facilityCheckTemp);
        return Result.ok("编辑成功!");
    }

    /**
     * 通过id删除
     *
     * @param id
     * @return
     */
    @AutoLog(value = "设备检验项目模板-通过id删除")
    @ApiOperation(value = "设备检验项目模板-通过id删除", notes = "设备检验项目模板-通过id删除")
    @DeleteMapping(value = "/delete")
    public Result<?> delete(@RequestParam(name = "id", required = true) String id) {
        facilityCheckTempService.removeById(id);
        return Result.ok("删除成功!");
    }

    /**
     * 批量删除
     *
     * @param ids
     * @return
     */
    @AutoLog(value = "设备检验项目模板-批量删除")
    @ApiOperation(value = "设备检验项目模板-批量删除", notes = "设备检验项目模板-批量删除")
    @DeleteMapping(value = "/deleteBatch")
    public Result<?> deleteBatch(@RequestParam(name = "ids", required = true) String ids) {
        this.facilityCheckTempService.removeByIds(Arrays.asList(ids.split(",")));
        return Result.ok("批量删除成功!");
    }

    /**
     * 通过设备名称和模板类型查询项目和标准列表
     *
     * @param name
     * @param type
     * @return
     */
    @AutoLog(value = "设备检验项目模板-通过设备名称和模板类型查询项目和标准列表")
    @ApiOperation(value = "设备检验项目模板-通过设备名称和模板类型查询项目和标准列表", notes = "设备检验项目模板-通过设备名称和模板类型查询项目和标准列表")
    @GetMapping(value = "/queryByNameType")
    public Result<?> queryById(@RequestParam(name = "name", required = true) String name,
                               @RequestParam(name = "type", required = true) String type) {
        QueryWrapper<FacilityCheckTemp> queryWrapper = new QueryWrapper();
        queryWrapper.eq("facility_name", name);
        queryWrapper.eq("temp_type", type);
        List<FacilityCheckTemp> list = facilityCheckTempService.list(queryWrapper);
        if (list.size() == 0) {
            return Result.error("未找到对应数据");
        }
        return Result.ok(list);
    }

    @AutoLog(value = "设备检验项目模板-通过模板名称和模板分类查询项目和标准列表")
    @ApiOperation(value = "设备检验项目模板-通过模板名称和模板分类查询项目和标准列表", notes = "设备检验项目模板-通过模板名称和模板分类查询项目和标准列表")
    @GetMapping(value = "/queryByFacilityNameAndTempSort")
    public Result<?> queryByFacilityNameAndTempSort(@RequestParam(name = "facilityName") String facilityName,
                                                    @RequestParam(name = "tempSort") String tempSort) {
        return facilityCheckTempService.queryByFacilityNameAndTempSort(facilityName, tempSort);
    }

    /**
     * 通过id查询
     *
     * @param id
     * @return
     */
    @AutoLog(value = "设备检验项目模板-通过id查询")
    @ApiOperation(value = "设备检验项目模板-通过id查询", notes = "设备检验项目模板-通过id查询")
    @GetMapping(value = "/queryById")
    public Result<?> queryById(@RequestParam(name = "id", required = true) String id) {
        FacilityCheckTemp facilityCheckTemp = facilityCheckTempService.getById(id);
        if (facilityCheckTemp == null) {
            return Result.error("未找到对应数据");
        }
        return Result.ok(facilityCheckTemp);
    }

    /**
     * 导出excel
     *
     * @param request
     * @param facilityCheckTemp
     */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, FacilityCheckTemp facilityCheckTemp) {
        return super.exportXls(request, facilityCheckTemp, FacilityCheckTemp.class, "设备检验项目模板");
    }

    /**
     * 通过excel导入数据
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, FacilityCheckTemp.class);
    }

}
