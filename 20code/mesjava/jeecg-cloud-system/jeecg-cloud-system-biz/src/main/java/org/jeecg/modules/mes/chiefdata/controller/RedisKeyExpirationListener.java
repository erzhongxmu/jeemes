package org.jeecg.modules.mes.chiefdata.controller;

import lombok.extern.slf4j.Slf4j;
import org.jeecg.common.system.api.ISysBaseAPI;
import org.jeecg.modules.mes.chiefdata.entity.MesAncillarytoolHeat;
import org.jeecg.modules.mes.chiefdata.entity.MesAncillarytoolStir;
import org.jeecg.modules.mes.chiefdata.service.IMesAncillarytoolHeatService;
import org.jeecg.modules.mes.chiefdata.service.IMesAncillarytoolStirService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.connection.Message;
import org.springframework.data.redis.listener.KeyExpirationEventMessageListener;
import org.springframework.data.redis.listener.RedisMessageListenerContainer;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * 监听所有db的过期事件__keyevent@*__:expired"
 */
@Component
@Slf4j
public class RedisKeyExpirationListener extends KeyExpirationEventMessageListener {
    @Autowired
    private IMesAncillarytoolHeatService mesAncillarytoolHeatService;
    @Autowired
    private IMesAncillarytoolStirService mesAncillarytoolStirService;
    @Autowired
    ISysBaseAPI sysBaseAPI;


    public RedisKeyExpirationListener(RedisMessageListenerContainer listenerContainer) {
        super(listenerContainer);
    }
    /**
     * 针对redis数据失效事件，进行数据处理
     * @param message
     * @param pattern
     */
    @Override
    public void onMessage(Message message, byte[] pattern) {
        // 用户做自己的业务处理即可,注意message.toString()可以获取失效的key
        String expiredKey = message.toString();
        System.err.println(expiredKey);
        MesAncillarytoolHeat mesAncillarytoolHeat = mesAncillarytoolHeatService.getById(expiredKey);
        if(mesAncillarytoolHeat != null){
            System.err.println(mesAncillarytoolHeat.getCreateBy());
            sysBaseAPI.sendSysAnnouncement("admin",mesAncillarytoolHeat.getCreateBy(),"回温结束","已经到达辅料回温的预计结束时间！请注意查收！");
        }

        MesAncillarytoolStir mesAncillarytoolStir = mesAncillarytoolStirService.getById(expiredKey);
        if(mesAncillarytoolStir != null){
            System.err.println(mesAncillarytoolStir.getCreateBy());
            sysBaseAPI.sendSysAnnouncement("admin",mesAncillarytoolStir.getCreateBy(),"搅拌结束","已经到达辅料搅拌的预计结束时间！请注意查收！");
        }
    }
}
