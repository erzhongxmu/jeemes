package org.jeecg.modules.mes.chiefdata.service;

import org.jeecg.modules.mes.chiefdata.entity.MesMaterielOther;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.List;

/**
 * @Description: 物料—其他
 * @Author: jeecg-boot
 * @Date:   2020-10-16
 * @Version: V1.0
 */
public interface IMesMaterielOtherService extends IService<MesMaterielOther> {

	public List<MesMaterielOther> selectByMainId(String mainId);
}
