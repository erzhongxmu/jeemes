package org.jeecg.modules.mes.chiefdata.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.mes.chiefdata.entity.MesChiefdataLogisticalcompany;
import org.jeecg.modules.mes.chiefdata.service.IMesChiefdataLogisticalcompanyService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

 /**
 * @Description: 主数据—物流公司
 * @Author: jeecg-boot
 * @Date:   2020-09-17
 * @Version: V1.0
 */
@Api(tags="主数据—物流公司")
@RestController
@RequestMapping("/chiefdata/mesChiefdataLogisticalcompany")
@Slf4j
public class MesChiefdataLogisticalcompanyController extends JeecgController<MesChiefdataLogisticalcompany, IMesChiefdataLogisticalcompanyService> {
	@Autowired
	private IMesChiefdataLogisticalcompanyService mesChiefdataLogisticalcompanyService;
	
	/**
	 * 分页列表查询
	 *
	 * @param mesChiefdataLogisticalcompany
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "主数据—物流公司-分页列表查询")
	@ApiOperation(value="主数据—物流公司-分页列表查询", notes="主数据—物流公司-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(MesChiefdataLogisticalcompany mesChiefdataLogisticalcompany,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<MesChiefdataLogisticalcompany> queryWrapper = QueryGenerator.initQueryWrapper(mesChiefdataLogisticalcompany, req.getParameterMap());
		Page<MesChiefdataLogisticalcompany> page = new Page<MesChiefdataLogisticalcompany>(pageNo, pageSize);
		IPage<MesChiefdataLogisticalcompany> pageList = mesChiefdataLogisticalcompanyService.page(page, queryWrapper);
		return Result.ok(pageList);
	}
	
	/**
	 *   添加
	 *
	 * @param mesChiefdataLogisticalcompany
	 * @return
	 */
	@AutoLog(value = "主数据—物流公司-添加")
	@ApiOperation(value="主数据—物流公司-添加", notes="主数据—物流公司-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody MesChiefdataLogisticalcompany mesChiefdataLogisticalcompany) {
		mesChiefdataLogisticalcompanyService.save(mesChiefdataLogisticalcompany);
		return Result.ok("添加成功！");
	}
	
	/**
	 *  编辑
	 *
	 * @param mesChiefdataLogisticalcompany
	 * @return
	 */
	@AutoLog(value = "主数据—物流公司-编辑")
	@ApiOperation(value="主数据—物流公司-编辑", notes="主数据—物流公司-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody MesChiefdataLogisticalcompany mesChiefdataLogisticalcompany) {
		mesChiefdataLogisticalcompanyService.updateById(mesChiefdataLogisticalcompany);
		return Result.ok("编辑成功!");
	}
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "主数据—物流公司-通过id删除")
	@ApiOperation(value="主数据—物流公司-通过id删除", notes="主数据—物流公司-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		mesChiefdataLogisticalcompanyService.removeById(id);
		return Result.ok("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "主数据—物流公司-批量删除")
	@ApiOperation(value="主数据—物流公司-批量删除", notes="主数据—物流公司-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.mesChiefdataLogisticalcompanyService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.ok("批量删除成功!");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "主数据—物流公司-通过id查询")
	@ApiOperation(value="主数据—物流公司-通过id查询", notes="主数据—物流公司-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		MesChiefdataLogisticalcompany mesChiefdataLogisticalcompany = mesChiefdataLogisticalcompanyService.getById(id);
		if(mesChiefdataLogisticalcompany==null) {
			return Result.error("未找到对应数据");
		}
		return Result.ok(mesChiefdataLogisticalcompany);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param mesChiefdataLogisticalcompany
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, MesChiefdataLogisticalcompany mesChiefdataLogisticalcompany) {
        return super.exportXls(request, mesChiefdataLogisticalcompany, MesChiefdataLogisticalcompany.class, "主数据—物流公司");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, MesChiefdataLogisticalcompany.class);
    }

}
