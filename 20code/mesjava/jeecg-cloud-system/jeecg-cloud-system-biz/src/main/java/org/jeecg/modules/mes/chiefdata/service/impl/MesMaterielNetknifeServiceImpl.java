package org.jeecg.modules.mes.chiefdata.service.impl;

import org.jeecg.modules.mes.chiefdata.entity.MesMaterielNetknife;
import org.jeecg.modules.mes.chiefdata.mapper.MesMaterielNetknifeMapper;
import org.jeecg.modules.mes.chiefdata.service.IMesMaterielNetknifeService;
import org.springframework.stereotype.Service;
import java.util.List;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @Description: 主数据—辅料-钢网/刮刀
 * @Author: jeecg-boot
 * @Date:   2020-10-16
 * @Version: V1.0
 */
@Service
public class MesMaterielNetknifeServiceImpl extends ServiceImpl<MesMaterielNetknifeMapper, MesMaterielNetknife> implements IMesMaterielNetknifeService {
	
	@Autowired
	private MesMaterielNetknifeMapper mesMaterielNetknifeMapper;
	
	@Override
	public List<MesMaterielNetknife> selectByMainId(String mainId) {
		return mesMaterielNetknifeMapper.selectByMainId(mainId);
	}
}
