package org.jeecg.modules.mes.chiefdata.service;

import org.jeecg.modules.mes.chiefdata.entity.MesMaterielNetknife;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.List;

/**
 * @Description: 主数据—辅料-钢网/刮刀
 * @Author: jeecg-boot
 * @Date:   2020-10-16
 * @Version: V1.0
 */
public interface IMesMaterielNetknifeService extends IService<MesMaterielNetknife> {

	public List<MesMaterielNetknife> selectByMainId(String mainId);
}
