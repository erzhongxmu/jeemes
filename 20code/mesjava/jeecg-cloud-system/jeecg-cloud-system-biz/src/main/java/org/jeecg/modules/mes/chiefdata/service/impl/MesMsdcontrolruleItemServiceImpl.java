package org.jeecg.modules.mes.chiefdata.service.impl;

import org.jeecg.modules.mes.chiefdata.entity.MesMsdcontrolruleItem;
import org.jeecg.modules.mes.chiefdata.mapper.MesMsdcontrolruleItemMapper;
import org.jeecg.modules.mes.chiefdata.service.IMesMsdcontrolruleItemService;
import org.springframework.stereotype.Service;
import java.util.List;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @Description: 主数据—MSD管控规则明细
 * @Author: jeecg-boot
 * @Date:   2020-09-17
 * @Version: V1.0
 */
@Service
public class MesMsdcontrolruleItemServiceImpl extends ServiceImpl<MesMsdcontrolruleItemMapper, MesMsdcontrolruleItem> implements IMesMsdcontrolruleItemService {
	
	@Autowired
	private MesMsdcontrolruleItemMapper mesMsdcontrolruleItemMapper;
	
	@Override
	public List<MesMsdcontrolruleItem> selectByMainId(String mainId) {
		return mesMsdcontrolruleItemMapper.selectByMainId(mainId);
	}
}
