package org.jeecg.modules.mes.organize.service.impl;

import org.jeecg.modules.mes.organize.entity.MesOrganizeFactory;
import org.jeecg.modules.mes.organize.entity.MesOrganizeStoresite;
import org.jeecg.modules.mes.organize.mapper.MesOrganizeStoresiteMapper;
import org.jeecg.modules.mes.organize.mapper.MesOrganizeFactoryMapper;
import org.jeecg.modules.mes.organize.service.IMesOrganizeFactoryService;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import java.io.Serializable;
import java.util.List;
import java.util.Collection;

/**
 * @Description: 组织—工厂
 * @Author: jeecg-boot
 * @Date:   2020-10-19
 * @Version: V1.0
 */
@Service
public class MesOrganizeFactoryServiceImpl extends ServiceImpl<MesOrganizeFactoryMapper, MesOrganizeFactory> implements IMesOrganizeFactoryService {

	@Autowired
	private MesOrganizeFactoryMapper mesOrganizeFactoryMapper;
	@Autowired
	private MesOrganizeStoresiteMapper mesOrganizeStoresiteMapper;
	
	@Override
	@Transactional
	public void delMain(String id) {
		mesOrganizeStoresiteMapper.deleteByMainId(id);
		mesOrganizeFactoryMapper.deleteById(id);
	}

	@Override
	@Transactional
	public void delBatchMain(Collection<? extends Serializable> idList) {
		for(Serializable id:idList) {
			mesOrganizeStoresiteMapper.deleteByMainId(id.toString());
			mesOrganizeFactoryMapper.deleteById(id);
		}
	}
	
}
