package org.jeecg.modules.bi.entity;

import java.io.Serializable;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import java.util.Date;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @Description: onl_cgreport_item
 * @Author: wms-cloud
 * @Date:   2020-12-09
 * @Version: V1.0
 */
@ApiModel(value="onl_cgreport_head对象", description="onl_cgreport_head")
@Data
@TableName("onl_cgreport_item")
public class zdOnlCgreportItem implements Serializable {
    private static final long serialVersionUID = 1L;

	/**id*/
	@TableId(type = IdType.ID_WORKER_STR)
	@ApiModelProperty(value = "id")
	private String id;
	/**报表ID*/
	@ApiModelProperty(value = "报表ID")
	private String cgrheadId;
	/**字段名字*/
	@Excel(name = "字段名字", width = 15)
	@ApiModelProperty(value = "字段名字")
	private String fieldName;
	/**字段文本*/
	@Excel(name = "字段文本", width = 15)
	@ApiModelProperty(value = "字段文本")
	private String fieldTxt;
	/**fieldWidth*/
	@Excel(name = "fieldWidth", width = 15)
	@ApiModelProperty(value = "fieldWidth")
	private Integer fieldWidth;
	/**字段类型*/
	@Excel(name = "字段类型", width = 15)
	@ApiModelProperty(value = "字段类型")
	private String fieldType;
	/**查询模式*/
	@Excel(name = "查询模式", width = 15)
	@ApiModelProperty(value = "查询模式")
	private String searchMode;
	/**是否排序  0否,1是*/
	@Excel(name = "是否排序  0否,1是", width = 15)
	@ApiModelProperty(value = "是否排序  0否,1是")
	private Integer isOrder;
	/**是否查询  0否,1是*/
	@Excel(name = "是否查询  0否,1是", width = 15)
	@ApiModelProperty(value = "是否查询  0否,1是")
	private Integer isSearch;
	/**字典CODE*/
	@Excel(name = "字典CODE", width = 15)
	@ApiModelProperty(value = "字典CODE")
	private String dictCode;
	/**字段跳转URL*/
	@Excel(name = "字段跳转URL", width = 15)
	@ApiModelProperty(value = "字段跳转URL")
	private String fieldHref;
	/**是否显示  0否,1显示*/
	@Excel(name = "是否显示  0否,1显示", width = 15)
	@ApiModelProperty(value = "是否显示  0否,1显示")
	private Integer isShow;
	/**排序*/
	@Excel(name = "排序", width = 15)
	@ApiModelProperty(value = "排序")
	private Integer orderNum;
	/**取值表达式*/
	@Excel(name = "取值表达式", width = 15)
	@ApiModelProperty(value = "取值表达式")
	private String replaceVal;
	/**是否合计 0否,1是（仅对数值有效）*/
	@Excel(name = "是否合计 0否,1是（仅对数值有效）", width = 15)
	@ApiModelProperty(value = "是否合计 0否,1是（仅对数值有效）")
	private String isTotal;
	/**分组标题*/
	@Excel(name = "分组标题", width = 15)
	@ApiModelProperty(value = "分组标题")
	private String groupTitle;
	/**创建人*/
	@ApiModelProperty(value = "创建人")
	private String createBy;
	/**创建时间*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
	@ApiModelProperty(value = "创建时间")
	private Date createTime;
	/**修改人*/
	@ApiModelProperty(value = "修改人")
	private String updateBy;
	/**修改时间*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
	@ApiModelProperty(value = "修改时间")
	private Date updateTime;
}
