package org.jeecg.modules.mes.users.service;

import org.jeecg.modules.mes.users.entity.MesUserInfo;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 用户管理
 * @Author: jeecg-boot
 * @Date: 2021-02-24
 * @Version: V1.0
 */
public interface IMesUserInfoService extends IService<MesUserInfo> {

}
