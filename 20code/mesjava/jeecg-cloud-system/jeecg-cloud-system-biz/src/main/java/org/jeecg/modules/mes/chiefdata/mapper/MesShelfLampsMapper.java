package org.jeecg.modules.mes.chiefdata.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.mes.chiefdata.entity.MesShelfLamps;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 货架多色灯
 * @Author: jeecg-boot
 * @Date:   2020-10-12
 * @Version: V1.0
 */
public interface MesShelfLampsMapper extends BaseMapper<MesShelfLamps> {

}
