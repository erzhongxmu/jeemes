package org.jeecg.modules.biconf.service.impl;

import org.jeecg.modules.biconf.entity.BiVisual;
import org.jeecg.modules.biconf.mapper.BiVisualMapper;
import org.jeecg.modules.biconf.service.IBiVisualService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 大屏列表
 * @Author: jeecg-boot
 * @Date:   2020-09-09
 * @Version: V1.0
 */
@Service
public class BiVisualServiceImpl extends ServiceImpl<BiVisualMapper, BiVisual> implements IBiVisualService {

}
