package org.jeecg.modules.mes.order.service;

import org.jeecg.modules.mes.order.entity.ExpApplyBill;
import org.jeecg.modules.mes.order.entity.ExpApplyMain;
import com.baomidou.mybatisplus.extension.service.IService;
import org.jeecg.modules.mes.order.entity.ImpApplyMain;
import org.springframework.beans.factory.annotation.Autowired;
import java.io.Serializable;
import java.util.Collection;
import java.util.List;

/**
 * @Description: 出口申请单主表
 * @Author: jeecg-boot
 * @Date:   2021-04-07
 * @Version: V1.0
 */
public interface IExpApplyMainService extends IService<ExpApplyMain> {

	/**
	 * 删除一对多
	 */
	public void delMain (String id);
	
	/**
	 * 批量删除一对多
	 */
	public void delBatchMain (Collection<? extends Serializable> idList);

	/**
	 * 上传关务系统数据
	 * @param expApplyMain
	 */
	public void insertSqlServer(ExpApplyMain expApplyMain);
}
