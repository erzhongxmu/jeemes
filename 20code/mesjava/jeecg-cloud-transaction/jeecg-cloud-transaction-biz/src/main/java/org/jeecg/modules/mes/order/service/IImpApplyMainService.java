package org.jeecg.modules.mes.order.service;

import org.jeecg.modules.mes.order.entity.ImpApplyBill;
import org.jeecg.modules.mes.order.entity.ImpApplyMain;
import com.baomidou.mybatisplus.extension.service.IService;
import org.springframework.beans.factory.annotation.Autowired;
import java.io.Serializable;
import java.util.Collection;
import java.util.List;

/**
 * @Description: 进口申请单主表
 * @Author: jeecg-boot
 * @Date:   2021-04-07
 * @Version: V1.0
 */
public interface IImpApplyMainService extends IService<ImpApplyMain> {

	/**
	 * 删除一对多
	 */
	public void delMain (String id);
	
	/**
	 * 批量删除一对多
	 */
	public void delBatchMain (Collection<? extends Serializable> idList);

	/**
	 * 上传关务系统数据
	 * @param impApplyMain
	 */
	public void insertSqlServer(ImpApplyMain impApplyMain);
}
