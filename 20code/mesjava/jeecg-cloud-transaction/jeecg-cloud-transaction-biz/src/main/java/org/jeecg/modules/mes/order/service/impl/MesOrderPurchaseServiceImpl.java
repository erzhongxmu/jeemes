package org.jeecg.modules.mes.order.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.jeecg.common.api.vo.Result;
import org.jeecg.modules.mes.chiefdata.entity.MesChiefdataBomitem;
import org.jeecg.modules.mes.client.BomClient;
import org.jeecg.modules.mes.order.entity.*;
import org.jeecg.modules.mes.order.mapper.MesPurchaseItemMapper;
import org.jeecg.modules.mes.order.mapper.MesOrderPurchaseMapper;
import org.jeecg.modules.mes.order.service.IImpApplyBillService;
import org.jeecg.modules.mes.order.service.IImpApplyMainService;
import org.jeecg.modules.mes.order.service.IMesOrderPurchaseService;
import org.jeecg.modules.mes.order.service.IMesPurchaseItemService;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StopWatch;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import java.util.Collection;

/**
 * @Description: 订单管理—采购订单
 * @Author: jeecg-boot
 * @Date: 2020-10-19
 * @Version: V1.0
 */
@Slf4j
@Service
public class MesOrderPurchaseServiceImpl extends ServiceImpl<MesOrderPurchaseMapper, MesOrderPurchase> implements IMesOrderPurchaseService {

    @Autowired
    private MesOrderPurchaseMapper mesOrderPurchaseMapper;
    @Autowired
    private MesPurchaseItemMapper mesPurchaseItemMapper;
    @Autowired
    private IMesPurchaseItemService mesPurchaseItemService;
    @Autowired
    private IImpApplyBillService impApplyBillService;
    @Autowired
    private IImpApplyMainService impApplyMainService;
    @Autowired
    private BomClient bomClient;

    @Override
    @Transactional
    public void delMain(String id) {
        mesPurchaseItemMapper.deleteByMainId(id);
        mesOrderPurchaseMapper.deleteById(id);
    }

    @Override
    @Transactional
    public void delBatchMain(Collection<? extends Serializable> idList) {
        for (Serializable id : idList) {
            mesPurchaseItemMapper.deleteByMainId(id.toString());
            mesOrderPurchaseMapper.deleteById(id);
        }
    }

    @Override
    @Transactional
    public void saveImpUpdateState(MesOrderPurchase order, List<ImpApplyBill> billlist) {

        QueryWrapper<ImpApplyMain> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("purchase_id", order.getId());
        ImpApplyMain main = impApplyMainService.getOne(queryWrapper);
        //添加到进口申请单主表数据
        ImpApplyMain impApplyMain = new ImpApplyMain();
        if (main == null) {
            impApplyMain.setPurchaseId(order.getId());
            impApplyMain.setOrderName(order.getOrderName());
            impApplyMain.setOrderCode(order.getOrderCode());
            impApplyMain.setOrderType(order.getOrderType());
            impApplyMain.setTotalPrice(order.getTotalPrice());
            impApplyMain.setCurrency(order.getCurrency());
            impApplyMain.setState("未上传");
            impApplyMainService.save(impApplyMain);
        } else {
            impApplyMain.setId(main.getId());
        }
        //修改采购单状态
        mesOrderPurchaseMapper.updateById(order);

        //进口单子表添加
        for (ImpApplyBill bill : billlist) {
            bill.setImpMainId(impApplyMain.getId());
//			QueryWrapper<ImpApplyBill> queryWrapper1 = new QueryWrapper<>();
//			queryWrapper1.eq("imp_apply_main",impApplyMain.getId());
//			queryWrapper1.eq("material",bill.getMaterial());
//			List<ImpApplyBill> list = impApplyBillService.list(queryWrapper1);
//			if(list.size()==0){
            impApplyBillService.save(bill);
//			}
        }

    }

    @Override
    @Transactional
    public void addAll(MesOrderPurchase order) {
        System.out.println("order:" + order);
        //判断orderCode订单编号唯一判定
        QueryWrapper<MesOrderPurchase> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("order_code", order.getOrderCode());
        List<MesOrderPurchase> list = this.list(queryWrapper);
        if (list.size() > 0) {
            throw new RuntimeException("已存在该订单编号，请变更！！！");
        }
        //采购订单添加时，设置订单状态为：未完成，上传状态为0：未上传
        order.setImportState("0");
        order.setIfFinish("未完成");
        this.save(order);
        //判断物料id是否为空,不为空则根据物料bom生成采购子表
        if (StringUtils.isNotBlank(order.getMaterielId())) {
            if (StringUtils.isBlank(order.getGrossAccount())) {//总数量
                throw new RuntimeException("根据BOM成品创建采购单，必须输入总数量！会根据总数量生成每个用量对应的数据！");
            }
            List<MesChiefdataBomitem> mesChiefdataBomitems = bomClient.selectByMaterialId(order.getMaterielId());
            if (mesChiefdataBomitems.size() == 0) {
                throw new RuntimeException("选择的Bom成品物料未找到子料件数据！");
            }
            for (MesChiefdataBomitem bomitem : mesChiefdataBomitems) {
                if (StringUtils.isBlank(order.getGrossAccount())) {//用量
                    throw new RuntimeException(order.getMaterielName() + "中的物料料号" + bomitem.getMaterielCode() + "用量为空，请设置！！！");
                }
                BigDecimal gross = new BigDecimal(order.getGrossAccount());//总数量
                if ("0".equals(bomitem.getQuantity().trim())) {//用量为0则时替代料不用生成出来

                }
                BigDecimal quantity = new BigDecimal(bomitem.getQuantity());//用量
                BigDecimal multiply = gross.multiply(quantity);//采购数量
                MesPurchaseItem mesPurchaseItem = new MesPurchaseItem();
                mesPurchaseItem.setPurchaseId(order.getId());
                mesPurchaseItem.setPurchaseNum(multiply.toString());
                mesPurchaseItem.setUnreceiveNum(multiply.toString());
                mesPurchaseItem.setProductCode(bomitem.getProductCode());
                mesPurchaseItem.setMaterielId(bomitem.getMaterielId());
                mesPurchaseItem.setMaterielCode(bomitem.getMaterielCode());
                mesPurchaseItem.setMaterielGauge(bomitem.getMaterielGauge());
                mesPurchaseItem.setMaterielName(bomitem.getMaterielName());
                mesPurchaseItem.setOrderUnit(bomitem.getUnit());
                mesPurchaseItem.setClientCode(bomitem.getClientCode());//客户料号
                mesPurchaseItem.setProductCode(bomitem.getProductCode());
                mesPurchaseItem.setQuery5(bomitem.getPackup() == null ? "" : bomitem.getPackup());//封装
                mesPurchaseItem.setQuery6(bomitem.getVoltage() == null ? "" : bomitem.getVoltage());//伏数
                mesPurchaseItem.setRowNum(bomitem.getDifferences() == null ? "" : bomitem.getDifferences());//误差
                mesPurchaseItem.setIfFinish("未完成");
                //如果最小包装数为空根据默认生成
                if (StringUtils.isBlank(bomitem.getPackType())) {
                    //根据bomitem的品名、封装生成最小包装数，默认5000
                    if (StringUtils.isBlank(bomitem.getPackup()) || StringUtils.isBlank(bomitem.getMaterielName())) {
                        mesPurchaseItem.setQuery4("5000");
                    } else {
                        mesPurchaseItem.setQuery4(zxbzs(bomitem));
                    }
                    BigDecimal query4 = new BigDecimal(mesPurchaseItem.getQuery4());
                    if (multiply.compareTo(query4) == -1) {
                        mesPurchaseItem.setQuery4(multiply.toString());
                    }
                } else {//不为空直接设置
                    mesPurchaseItem.setQuery4(bomitem.getPackType());
                }
                mesPurchaseItem.setIfFinish("未完成");
                mesPurchaseItemMapper.insert(mesPurchaseItem);
            }
        }
    }

    @Override
    public Result<?> queryMesPurchaseItemListByMainId(String id, String gauge, String materielCode, String ifFinish) {
        log.info("订单管理—采购订单子表通过主表ID查询 queryMesPurchaseItemByMainId>>id>" + id + ">>gauge>" + gauge + ">>materielCode>" + materielCode + ">>ifFinish>" + ifFinish);
        if (StringUtils.isNotBlank(gauge)) {//如果规格不为空
            QueryWrapper<MesPurchaseItem> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("purchase_id", id).eq("if_finish", ifFinish).like("materiel_gauge", gauge);
            List<MesPurchaseItem> purchaseItemList = mesPurchaseItemService.list(queryWrapper);
            if (purchaseItemList.size() != 0) {
                return Result.ok(purchaseItemList);
            } else {
                return Result.error("没有找到数据！请检查规格是否正确！");
            }
        }
        if (StringUtils.isNotBlank(materielCode)) {
            QueryWrapper<MesPurchaseItem> wrapper = new QueryWrapper<>();
            wrapper.eq("purchase_id", id).eq("if_finish", ifFinish).eq("materiel_code", materielCode);
            List<MesPurchaseItem> mesPurchaseItemList = mesPurchaseItemService.list(wrapper);
            if (mesPurchaseItemList.size() == 0) {
                return Result.error("找不到对应数据!请检查料号是否正确！");
            }
            return Result.ok(mesPurchaseItemList);
        }

        if (StringUtils.isNotBlank(ifFinish)) {
            StopWatch stopWatch = new StopWatch();
            stopWatch.start();
            List<MesPurchaseItem> mesPurchaseItemList = mesPurchaseItemService.lambdaQuery().eq(MesPurchaseItem::getPurchaseId, id).eq(MesPurchaseItem::getIfFinish, ifFinish).list();
            stopWatch.stop();
            System.out.printf("执行时长：%d 毫秒.%n", stopWatch.getTotalTimeMillis());

            /*stopWatch.start();
            QueryWrapper<MesPurchaseItem> wrapper1 = new QueryWrapper<>();
            wrapper1.eq("purchase_id", id);
            List<MesPurchaseItem> mesPurchaseItemList2 = mesPurchaseItemService.list(wrapper1);
            stopWatch.stop();
            System.out.printf("3.2执行时长：%d 毫秒.%n", stopWatch.getTotalTimeMillis());*/

//            if (mesPurchaseItemList.size() == 0 && mesPurchaseItemList2.size() > 0) {
            if (com.epms.util.ObjectHelper.isEmpty(mesPurchaseItemList)) {
                StopWatch stopWatch1 = new StopWatch();
                stopWatch1.start();
                MesOrderPurchase mesOrderPurchase = this.getById(id);
                mesOrderPurchase.setIfFinish("收货完成");//未完成
                this.updateById(mesOrderPurchase);
                stopWatch1.stop();
                System.out.printf("1执行时长：%d 毫秒.%n", stopWatch1.getTotalTimeMillis());
                return Result.ok("收货已完成!");
            }

            StopWatch stopWatch2 = new StopWatch();
            stopWatch2.start();
            for (MesPurchaseItem purchaseItem : mesPurchaseItemList) {
                if ("0".equals(purchaseItem.getUnreceiveNum()) && !"收货完成".equals(purchaseItem.getIfFinish())) {
                    purchaseItem.setIfFinish("收货完成");
                    mesPurchaseItemService.updateById(purchaseItem);
                }
            }
            stopWatch2.stop();
            System.out.printf("2执行时长：%d 毫秒.%n", stopWatch2.getTotalTimeMillis());
            return Result.ok(mesPurchaseItemList);
        }

        List<MesPurchaseItem> mesPurchaseItemList = mesPurchaseItemService.selectByMainId(id);
        if (mesPurchaseItemList.size() > 0) {
            return Result.ok(mesPurchaseItemList);
        }
        return Result.error("找不到对应数据!请检查规格是否正确！");
    }

    @Override
    public Result<?> queryMesPurchaseItemsByMainId(String id, String gauge, String materielCode, String ifFinish, Page<MesPurchaseItem> page) {
        log.info("订单管理—采购订单子表通过主表ID查询 queryMesPurchaseItemByMainId>>id>" + id + ">>gauge>" + gauge + ">>materielCode>" + materielCode + ">>ifFinish>" + ifFinish);
        if (StringUtils.isNotBlank(gauge)) {
            QueryWrapper<MesPurchaseItem> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("purchase_id", id).eq("if_finish", ifFinish).like("materiel_gauge", gauge);
            IPage<MesPurchaseItem> pageList = mesPurchaseItemService.page(page, queryWrapper);
            List<MesPurchaseItem> purchaseItemList = pageList.getRecords();
            if (com.epms.util.ObjectHelper.isEmpty(purchaseItemList)) {
                return Result.error("没有找到数据！请检查规格是否正确！");
            }
            return Result.ok(pageList);
        }

        if (StringUtils.isNotBlank(materielCode)) {
            QueryWrapper<MesPurchaseItem> wrapper = new QueryWrapper<>();
            wrapper.eq("purchase_id", id).eq("if_finish", ifFinish).eq("materiel_code", materielCode);
            IPage<MesPurchaseItem> pageList = mesPurchaseItemService.page(page, wrapper);
            List<MesPurchaseItem> mesPurchaseItemList = pageList.getRecords();
            if (com.epms.util.ObjectHelper.isEmpty(mesPurchaseItemList)) {
                return Result.error("找不到对应数据!请检查料号是否正确！");
            }
            return Result.ok(pageList);
        }

        if (StringUtils.isNotBlank(ifFinish)) {
            StopWatch stopWatch = new StopWatch();
            stopWatch.start();
            IPage<MesPurchaseItem> pageList = mesPurchaseItemService.lambdaQuery().eq(MesPurchaseItem::getPurchaseId, id).eq(MesPurchaseItem::getIfFinish, ifFinish).page(page);
            List<MesPurchaseItem> mesPurchaseItemList = pageList.getRecords();
            stopWatch.stop();
            System.out.printf("执行时长：%d 毫秒.%n", stopWatch.getTotalTimeMillis());

            if (com.epms.util.ObjectHelper.isEmpty(mesPurchaseItemList)) {
                StopWatch stopWatch1 = new StopWatch();
                stopWatch1.start();
                MesOrderPurchase mesOrderPurchase = this.getById(id);
                mesOrderPurchase.setIfFinish("收货完成");//未完成
                this.updateById(mesOrderPurchase);
                stopWatch1.stop();
                System.out.printf("1执行时长：%d 毫秒.%n", stopWatch1.getTotalTimeMillis());
            }

            StopWatch stopWatch2 = new StopWatch();
            stopWatch2.start();
            for (MesPurchaseItem purchaseItem : mesPurchaseItemList) {
                if ("0".equals(purchaseItem.getUnreceiveNum()) && !"收货完成".equals(purchaseItem.getIfFinish())) {
                    purchaseItem.setIfFinish("收货完成");
                    mesPurchaseItemService.updateById(purchaseItem);
                }
            }
            stopWatch2.stop();
            System.out.printf("2执行时长：%d 毫秒.%n", stopWatch2.getTotalTimeMillis());
            return Result.ok(pageList);
        }

        IPage<MesPurchaseItem> pageList = mesPurchaseItemService.lambdaQuery().eq(MesPurchaseItem::getPurchaseId, id).page(page);
        List<MesPurchaseItem> mesPurchaseItemList = pageList.getRecords();
        if (com.epms.util.ObjectHelper.isEmpty(mesPurchaseItemList)) {
            return Result.error("找不到对应数据!请检查规格是否正确！");
        }
        return Result.ok(pageList);
    }

    public String zxbzs(MesChiefdataBomitem bomitem) {
        String msg = "5000";
        String materielName = bomitem.getMaterielName().trim();
        String packup = bomitem.getPackup().trim();
        if (materielName.contains("电阻")) {
            if (packup.contains("0603")) {
                msg = "5000";
            }
            if (packup.contains("0805")) {
                msg = "5000";
            }
            if (packup.contains("1206")) {
                msg = "5000";
            }
            if (packup.contains("0402")) {
                msg = "10000";
            }
            if (packup.contains("0201")) {
                msg = "10000";
            }
        }
        if (materielName.contains("电容")) {
            if (packup.contains("0603")) {
                msg = "4000";
            }
            if (packup.contains("0805")) {
                msg = "4000";
            }
            if (packup.contains("1204")) {
                msg = "4000";
            }
            if (packup.contains("0402")) {
                msg = "10000";
            }
            if (packup.contains("0201")) {
                msg = "10000";
            }
        }
        if (materielName.contains("三极管")) {
            msg = "4000";
        }
        if (materielName.contains("LED")) {
            msg = "3000";
        }
        if (materielName.contains("电感")) {
            msg = "4000";
        }
        if (materielName.contains("电解")) {
            msg = "1000";
        }
        if (materielName.contains("电容")) {
            msg = "1000";
        }
        return msg;
    }

}
