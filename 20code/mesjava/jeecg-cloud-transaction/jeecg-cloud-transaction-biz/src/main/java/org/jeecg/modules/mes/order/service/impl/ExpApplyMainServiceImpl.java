package org.jeecg.modules.mes.order.service.impl;

import cn.hutool.core.util.IdUtil;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.lang3.StringUtils;
import org.jeecg.common.util.ApplyBillDBUtil;
import org.jeecg.common.util.DateUtils;
import org.jeecg.modules.mes.order.entity.ExpApplyBill;
import org.jeecg.modules.mes.order.entity.ExpApplyMain;
import org.jeecg.modules.mes.order.mapper.ExpApplyBillMapper;
import org.jeecg.modules.mes.order.mapper.ExpApplyMainMapper;
import org.jeecg.modules.mes.order.service.IExpApplyMainService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.List;

/**
 * @Description: 出口申请单主表
 * @Author: jeecg-boot
 * @Date:   2021-04-07
 * @Version: V1.0
 */
@Service
public class ExpApplyMainServiceImpl extends ServiceImpl<ExpApplyMainMapper, ExpApplyMain> implements IExpApplyMainService {

	@Autowired
	private ExpApplyMainMapper expApplyMainMapper;
	@Autowired
	private ExpApplyBillMapper expApplyBillMapper;

	@Override
	@Transactional
	public void delMain(String id) {
		expApplyBillMapper.deleteByMainId(id);
		expApplyMainMapper.deleteById(id);
	}

	@Override
	@Transactional
	public void delBatchMain(Collection<? extends Serializable> idList) {
		for(Serializable id:idList) {
			expApplyBillMapper.deleteByMainId(id.toString());
			expApplyMainMapper.deleteById(id);
		}
	}
	@Override
	@Transactional
	public void insertSqlServer(ExpApplyMain expApplyMain) {

		List<ExpApplyBill> impApplyBills = expApplyBillMapper.selectByMainId(expApplyMain.getId());
		for (ExpApplyBill expApplyBill:impApplyBills) {
			if(StringUtils.isBlank(expApplyBill.getOutBillNo())||StringUtils.isBlank(expApplyBill.getImgExgFlag())||
					StringUtils.isBlank(expApplyBill.getMaterial())||StringUtils.isBlank(expApplyBill.getSectionNo())){
				throw new RuntimeException("出货单号、物料标记、批次号、料号不能为空");
			}
			if(StringUtils.isBlank(expApplyBill.getCompanyMergerKey())){
				expApplyBill.setCompanyMergerKey("");
			}
			expApplyBill.setUploadDate(DateUtils.getDate());//上传时间
			if(StringUtils.isBlank(expApplyBill.getCompanyCode())){
				expApplyBill.setCompanyCode("500666002U");
			}
			if (StringUtils.isBlank(expApplyBill.getGwId())){
				expApplyBill.setGwId(IdUtil.simpleUUID());
			}
			if (expApplyBill.getPalletNum()==null){
				expApplyBill.setPalletNum(0);
			}
			if(expApplyBill.getOptLock()==null){
				expApplyBill.setOptLock(0);
			}
			if(StringUtils.isBlank(expApplyBill.getInvoiceDate())){
				expApplyBill.setInvoiceDate(DateUtils.date2Str(new SimpleDateFormat("yyyy/MM/dd")));
			}
			//如果数量未0，则不上传关务系统
			BigDecimal b = new BigDecimal(expApplyBill.getQty());
			if(!(b.compareTo(BigDecimal.ZERO)==0)){
				try{
					//关务系统能连接上的时候打开
					ApplyBillDBUtil.insertSqlServer(expApplyBill);
				}catch(Exception e){
					//仅仅捕捉 SQLException
					throw new RuntimeException("关务系统连接失败，请检查！！！"+e);
				}
				expApplyBill.setExpApplyState("已上传");
				expApplyBillMapper.updateById(expApplyBill);
			}else {
				throw new RuntimeException("料号为："+expApplyBill.getMaterial()+"的企业数量为零！！！");
			}
		}
		//修改状态为已上传
		expApplyMain.setState("已上传");
		expApplyMainMapper.updateById(expApplyMain);
	}
}
