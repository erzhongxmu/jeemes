package org.jeecg.modules.mes.order.service.impl;

import org.jeecg.modules.mes.order.entity.MesProduceItem;
import org.jeecg.modules.mes.order.mapper.MesProduceItemMapper;
import org.jeecg.modules.mes.order.service.IMesProduceItemService;
import org.springframework.stereotype.Service;
import java.util.List;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @Description: 订单管理—生产订单子表
 * @Author: jeecg-boot
 * @Date:   2020-10-19
 * @Version: V1.0
 */
@Service
public class MesProduceItemServiceImpl extends ServiceImpl<MesProduceItemMapper, MesProduceItem> implements IMesProduceItemService {
	
	@Autowired
	private MesProduceItemMapper mesProduceItemMapper;
	
	@Override
	public List<MesProduceItem> selectByMainId(String mainId) {
		return mesProduceItemMapper.selectByMainId(mainId);
	}
}
