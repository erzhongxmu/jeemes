package org.jeecg.modules.mes.order.service.impl;

import org.jeecg.modules.mes.order.entity.MesSaleItem;
import org.jeecg.modules.mes.order.mapper.MesSaleItemMapper;
import org.jeecg.modules.mes.order.service.IMesSaleItemService;
import org.springframework.stereotype.Service;
import java.util.List;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @Description: 订单管理—销售订单子表
 * @Author: jeecg-boot
 * @Date:   2020-10-19
 * @Version: V1.0
 */
@Service
public class MesSaleItemServiceImpl extends ServiceImpl<MesSaleItemMapper, MesSaleItem> implements IMesSaleItemService {
	
	@Autowired
	private MesSaleItemMapper mesSaleItemMapper;
	
	@Override
	public List<MesSaleItem> selectByMainId(String mainId) {
		return mesSaleItemMapper.selectByMainId(mainId);
	}
}
