package org.jeecg.modules.mes.order.service;

import org.jeecg.modules.mes.order.entity.MesProduceProcess;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.List;

/**
 * @Description: 订单管理—生产订单工序子表
 * @Author: jeecg-boot
 * @Date:   2020-10-19
 * @Version: V1.0
 */
public interface IMesProduceProcessService extends IService<MesProduceProcess> {

	public List<MesProduceProcess> selectByMainId(String mainId);
}
