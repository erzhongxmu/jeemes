package org.jeecg.modules.mes.client;

import org.jeecg.common.constant.ServiceNameConstants;
import org.jeecg.modules.mes.order.entity.MesOrderProduce;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@Component
@FeignClient(contextId = "WareHouseServiceClient", value = ServiceNameConstants.WAREHOUSE_SERVICE)
public interface WareHouseClient {

    /**
     * 根据生产订单生成领料清单
     */
    @PostMapping("/storage/mesMaterielOccupy/addMaterielOccupyByProduce")
    String addMaterielOccupyByProduce(@RequestBody MesOrderProduce mesOrderProduce);

}
