package org.jeecg.modules.mes.order.mapper;

import java.util.List;
import org.jeecg.modules.mes.order.entity.MesProduceItem;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * @Description: 订单管理—生产订单子表
 * @Author: jeecg-boot
 * @Date:   2020-10-19
 * @Version: V1.0
 */
public interface MesProduceItemMapper extends BaseMapper<MesProduceItem> {

	public boolean deleteByMainId(@Param("mainId") String mainId);
    
	public List<MesProduceItem> selectByMainId(@Param("mainId") String mainId);

}
