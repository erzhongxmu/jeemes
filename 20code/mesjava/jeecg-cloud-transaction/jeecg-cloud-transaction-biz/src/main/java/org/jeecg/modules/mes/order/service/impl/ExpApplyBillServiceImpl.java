package org.jeecg.modules.mes.order.service.impl;

import org.jeecg.modules.mes.order.entity.ExpApplyBill;
import org.jeecg.modules.mes.order.mapper.ExpApplyBillMapper;
import org.jeecg.modules.mes.order.service.IExpApplyBillService;
import org.springframework.stereotype.Service;
import java.util.List;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @Description: 出口申请单
 * @Author: jeecg-boot
 * @Date:   2021-04-07
 * @Version: V1.0
 */
@Service
public class ExpApplyBillServiceImpl extends ServiceImpl<ExpApplyBillMapper, ExpApplyBill> implements IExpApplyBillService {
	
	@Autowired
	private ExpApplyBillMapper expApplyBillMapper;
	
	@Override
	public List<ExpApplyBill> selectByMainId(String mainId) {
		return expApplyBillMapper.selectByMainId(mainId);
	}
}
