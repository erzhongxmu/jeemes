package org.jeecg.modules.mes.order.service;

import org.jeecg.modules.mes.order.entity.ExpApplyBill;
import org.jeecg.modules.mes.order.entity.MesSaleItem;
import org.jeecg.modules.mes.order.entity.MesOrderSale;
import com.baomidou.mybatisplus.extension.service.IService;
import org.springframework.beans.factory.annotation.Autowired;
import java.io.Serializable;
import java.util.Collection;
import java.util.List;

/**
 * @Description: 订单管理—销售订单
 * @Author: jeecg-boot
 * @Date:   2020-10-19
 * @Version: V1.0
 */
public interface IMesOrderSaleService extends IService<MesOrderSale> {

	/**
	 * 删除一对多
	 */
	public void delMain (String id);
	
	/**
	 * 批量删除一对多
	 */
	public void delBatchMain (Collection<? extends Serializable> idList);

	/**
	 * 生成出口单得时候修改状态，并保存出口单
	 * @param sale
	 * @param bills
	 */
	public void saveExpUpdateState(MesOrderSale sale, List<ExpApplyBill> bills);

}
