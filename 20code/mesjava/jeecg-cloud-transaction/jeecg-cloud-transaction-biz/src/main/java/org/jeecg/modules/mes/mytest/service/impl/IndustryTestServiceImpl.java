package org.jeecg.modules.ems.service.impl;

import org.jeecg.modules.ems.entity.IndustryTest;
import org.jeecg.modules.ems.mapper.IndustryTestMapper;
import org.jeecg.modules.ems.service.IIndustryTestService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 测试表
 * @Author: jeecg-boot
 * @Date:   2020-08-31
 * @Version: V1.0
 */
@Service
public class IndustryTestServiceImpl extends ServiceImpl<IndustryTestMapper, IndustryTest> implements IIndustryTestService {

}
