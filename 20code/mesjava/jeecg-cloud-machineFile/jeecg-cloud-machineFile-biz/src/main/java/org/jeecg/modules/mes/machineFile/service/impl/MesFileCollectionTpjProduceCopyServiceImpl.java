package org.jeecg.modules.mes.machineFile.service.impl;

import org.jeecg.modules.mes.machineFile.mapper.MesFileCollectionTpjProduceCopyMapper;
import org.jeecg.modules.mes.machineFile.service.IMesFileCollectionTpjProduceCopyService;
import org.jeecg.modules.mes.produce.entity.MesFileCollectionTpjProduceCopy;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: mes_file_collection_tpj_produce_copy
 * @Author: jeecg-boot
 * @Date:   2021-05-31
 * @Version: V1.0
 */
@Service
public class MesFileCollectionTpjProduceCopyServiceImpl extends ServiceImpl<MesFileCollectionTpjProduceCopyMapper, MesFileCollectionTpjProduceCopy> implements IMesFileCollectionTpjProduceCopyService {

}
