package org.jeecg.modules.mes.machineFile.controller;

import io.swagger.annotations.ApiOperation;
import org.jeecg.common.api.vo.Result;
import org.jeecg.modules.mes.client.ProduceClient;
import org.jeecg.modules.mes.machineFile.service.IMesFileCollectionAoiService;
import org.jeecg.modules.mes.machineFile.service.IMesFileCollectionSpiService;
import org.jeecg.modules.mes.machineFile.service.IMesFileCollectionTpjService;
import org.jeecg.modules.mes.produce.entity.MesCourseScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import javax.servlet.http.HttpServletRequest;

/**
 * @Package org.jeecg.modules.mes.filecollection
 * @date 2021/4/16 14:05
 * @description
 */
@RestController
@RequestMapping("/produce/fileCollection")
public class MesFileCollectionController {

    @Value("")
    private String fileSavePath;
    @Autowired
    private IMesFileCollectionSpiService mesFileCollectionSpiService ;
    @Autowired
    private IMesFileCollectionTpjService mesFileCollectionTpjService;
    @Autowired
    private IMesFileCollectionAoiService mesFileCollectionAoiService;
    @Autowired
    private ProduceClient produceClient;


    /**
     * 直接更新物料消耗数据和其他数据
     * @param mesCourseScan
     * @return
     */
    @ApiOperation(value="直接更新物料消耗数据和其他数据", notes="直接更新物料消耗数据和其他数据")
    @PostMapping("/gethx")
    public Result<?> gethx(@RequestBody MesCourseScan mesCourseScan) {
        produceClient.getxh(mesCourseScan);
        return Result.ok("更新成功");
    }

    /**
     * 接收spi的txt文件
     * @param request
     * @param line
     * @return
     */
    @ApiOperation(value="接收spi的txt文件", notes="接收spi的txt文件")
    @PostMapping("/receiveSpiFile")
    public Result<?> receiveSpiFile(HttpServletRequest request,String line) {
        mesFileCollectionSpiService.receiveSpiFile(request,line);
        return Result.ok("上传成功");
    }

    /**
     * 接收贴片机文件
     * @param request
     * @param line
     * @return
     */
    @ApiOperation(value="接收贴片机文件", notes="接收贴片机文件")
    @PostMapping("/receiveTpjFile")
    public Result<?> receiveTpjFile(HttpServletRequest request,String line){
        mesFileCollectionTpjService.receiveTpjFile(request,line);
        return Result.ok("上传成功");
    }

    /**
     * 接收aoi文件
     * @param request
     * @param line
     * @return
     */
    @ApiOperation(value="接收aoi文件", notes="接收aoi文件")
    @PostMapping("/receiveAoiFile")
    public Result<?> receiveAoiFile(HttpServletRequest request,String line){
        mesFileCollectionAoiService.receiveAoiFile(request,line);
        return Result.ok("上传成功");
    }
}
