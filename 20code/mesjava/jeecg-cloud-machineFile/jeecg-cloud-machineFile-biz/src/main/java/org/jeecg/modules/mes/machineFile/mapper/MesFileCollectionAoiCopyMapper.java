package org.jeecg.modules.mes.machineFile.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.jeecg.modules.mes.produce.entity.MesFileCollectionAoiCopy;

/**
 * @Description: mes_file_collection_aoi_copy
 * @Author: jeecg-boot
 * @Date:   2021-06-01
 * @Version: V1.0
 */
public interface MesFileCollectionAoiCopyMapper extends BaseMapper<MesFileCollectionAoiCopy> {

}
