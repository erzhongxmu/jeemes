package org.jeecg.modules.mes.machineFile.service;

import org.jeecg.modules.mes.produce.entity.MesFileCollectionSpiDetail;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: mes_file_collection_spi_detail
 * @Author: jeecg-boot
 * @Date:   2021-04-16
 * @Version: V1.0
 */
public interface IMesFileCollectionSpiDetailService extends IService<MesFileCollectionSpiDetail> {

}
