package org.jeecg.modules.mes.machineFile.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.jeecg.modules.mes.produce.entity.MesFileCollectionTpjProduceCopy;

/**
 * @Description: mes_file_collection_tpj_produce_copy
 * @Author: jeecg-boot
 * @Date:   2021-05-31
 * @Version: V1.0
 */
public interface IMesFileCollectionTpjProduceCopyService extends IService<MesFileCollectionTpjProduceCopy> {

}
