package org.jeecg.modules.mes.storage.entity;

import lombok.Data;

import java.util.List;


public class MesShelfLight {
    private java.lang.String control_caty;
    private java.lang.Integer app_id;
    List<MesShelfData> data;

    public String getControl_caty() {
        return control_caty;
    }

    public void setControl_caty(String control_caty) {
        this.control_caty = control_caty;
    }

    public Integer getApp_id() {
        return app_id;
    }

    public void setApp_id(Integer app_id) {
        this.app_id = app_id;
    }

    public List<MesShelfData> getData() {
        return data;
    }

    public void setData(List<MesShelfData> data) {
        this.data = data;
    }
}
