package org.jeecg.modules.mes.chiefdata.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.jeecg.common.aspect.annotation.Dict;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;

/**
 * @Description: 设备检验项目模板
 * @Author: jeecg-boot
 * @Date:   2021-06-02
 * @Version: V1.0
 */
@Data
@TableName("facility_check_temp")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="facility_check_temp对象", description="设备检验项目模板")
public class FacilityCheckTemp implements Serializable {
    private static final long serialVersionUID = 1L;

    /**主键*/
    @TableId(type = IdType.ID_WORKER_STR)
    @ApiModelProperty(value = "主键")
    private java.lang.String id;
    /**创建人*/
    @ApiModelProperty(value = "创建人")
    private java.lang.String createBy;
    /**创建日期*/
    @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "创建日期")
    private java.util.Date createTime;
    /**更新人*/
    @ApiModelProperty(value = "更新人")
    private java.lang.String updateBy;
    /**更新日期*/
    @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "更新日期")
    private java.util.Date updateTime;
    /**所属部门*/
    @ApiModelProperty(value = "所属部门")
    private java.lang.String sysOrgCode;
    /**模板名称*/
    @Excel(name = "模板名称", width = 15, dicCode = "facility_name_temp")
    @Dict(dicCode = "facility_name_temp")
    @ApiModelProperty(value = "模板名称")
    private java.lang.String facilityName;
    /**模板类型*/
    @Excel(name = "模板类型", width = 15, dicCode = "facility_temp_type")
    @Dict(dicCode = "facility_temp_type")
    @ApiModelProperty(value = "模板类型")
    private java.lang.String tempType;
    /**模板分类*/
    @Excel(name = "模板分类", width = 15)
    @ApiModelProperty(value = "模板分类")
    private java.lang.String tempSort;
    /**项目*/
    @Excel(name = "项目", width = 15)
    @ApiModelProperty(value = "项目")
    private java.lang.String project;
    /**标准*/
    @Excel(name = "标准", width = 15)
    @ApiModelProperty(value = "标准")
    private java.lang.String standard;
}
