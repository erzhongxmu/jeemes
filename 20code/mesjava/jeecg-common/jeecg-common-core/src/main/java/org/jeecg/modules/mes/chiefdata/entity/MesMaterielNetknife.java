package org.jeecg.modules.mes.chiefdata.entity;

import java.io.Serializable;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import java.util.Date;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @Description: 主数据—辅料-钢网/刮刀
 * @Author: jeecg-boot
 * @Date:   2020-11-17
 * @Version: V1.0
 */
@ApiModel(value="mes_chiefdata_materiel对象", description="主数据—物料")
@Data
@TableName("mes_materiel_netknife")
public class MesMaterielNetknife implements Serializable {
    private static final long serialVersionUID = 1L;

	/**id*/
	@TableId(type = IdType.ID_WORKER_STR)
	@ApiModelProperty(value = "id")
	private java.lang.String id;
	/**创建人*/
	@ApiModelProperty(value = "创建人")
	private java.lang.String createBy;
	/**创建日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
	@ApiModelProperty(value = "创建日期")
	private java.util.Date createTime;
	/**更新人*/
	@ApiModelProperty(value = "更新人")
	private java.lang.String updateBy;
	/**更新日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
	@ApiModelProperty(value = "更新日期")
	private java.util.Date updateTime;
	/**所属部门*/
	@ApiModelProperty(value = "所属部门")
	private java.lang.String sysOrgCode;
	/**安全库存量*/
	@Excel(name = "安全库存量", width = 15)
	@ApiModelProperty(value = "安全库存量")
	private java.lang.String storageNum;
	/**生成规则*/
	@Excel(name = "生成规则", width = 15)
	@ApiModelProperty(value = "生成规则")
	private java.lang.String createRule;
	/**最大印刷次数*/
	@Excel(name = "最大印刷次数", width = 15)
	@ApiModelProperty(value = "最大印刷次数")
	private java.lang.String maxPrintnum;
	/**印刷周期上限*/
	@Excel(name = "印刷周期上限", width = 15)
	@ApiModelProperty(value = "印刷周期上限")
	private java.lang.String printCyclelimit;
	/**保养次数*/
	@Excel(name = "保养次数", width = 15)
	@ApiModelProperty(value = "保养次数")
	private java.lang.String maintainNum;
	/**保养周期*/
	@Excel(name = "保养周期", width = 15)
	@ApiModelProperty(value = "保养周期")
	private java.lang.String maintainCycle;
	/**保养提醒次数*/
	@Excel(name = "保养提醒次数", width = 15)
	@ApiModelProperty(value = "保养提醒次数")
	private java.lang.String maintainRemindnum;
	/**保养提醒天数*/
	@Excel(name = "保养提醒天数", width = 15)
	@ApiModelProperty(value = "保养提醒天数")
	private java.lang.String maintainReminddays;
	/**辅料制具id*/
	@ApiModelProperty(value = "辅料制具id")
	private java.lang.String materielId;
	/**备用1*/
	@Excel(name = "备用1", width = 15)
	@ApiModelProperty(value = "备用1")
	private java.lang.String query1;
	/**备用2*/
	@Excel(name = "备用2", width = 15)
	@ApiModelProperty(value = "备用2")
	private java.lang.String query2;
	/**备用3*/
	@Excel(name = "备用3", width = 15)
	@ApiModelProperty(value = "备用3")
	private java.lang.String query3;
	/**备用4*/
	@Excel(name = "备用4", width = 15)
	@ApiModelProperty(value = "备用4")
	private java.lang.String query4;
	/**备用5*/
	@Excel(name = "备用5", width = 15)
	@ApiModelProperty(value = "备用5")
	private java.lang.String query5;
	/**备用6*/
	@Excel(name = "备用6", width = 15)
	@ApiModelProperty(value = "备用6")
	private java.lang.String query6;
}
