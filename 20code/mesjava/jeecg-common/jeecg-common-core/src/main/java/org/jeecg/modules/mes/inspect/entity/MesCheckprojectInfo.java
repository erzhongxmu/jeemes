package org.jeecg.modules.mes.inspect.entity;

import java.io.Serializable;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import org.jeecg.common.aspect.annotation.Dict;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import java.util.Date;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @Description: 检测项目信息
 * @Author: jeecg-boot
 * @Date:   2020-10-15
 * @Version: V1.0
 */
@Data
@TableName("mes_checkproject_info")
@ApiModel(value="mes_materiel_checkprojrct对象", description="质检中心-物料检测项目")
public class MesCheckprojectInfo implements Serializable {
    private static final long serialVersionUID = 1L;

	/**id*/
	@TableId(type = IdType.ID_WORKER_STR)
	@ApiModelProperty(value = "id")
	private java.lang.String id;
	/**创建人*/
	@ApiModelProperty(value = "创建人")
	private java.lang.String createBy;
	/**创建日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
	@ApiModelProperty(value = "创建日期")
	private java.util.Date createTime;
	/**更新人*/
	@ApiModelProperty(value = "更新人")
	private java.lang.String updateBy;
	/**更新日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
	@ApiModelProperty(value = "更新日期")
	private java.util.Date updateTime;
	/**所属部门*/
	@ApiModelProperty(value = "所属部门")
	private java.lang.String sysOrgCode;
	/**检测阶别*/
	@Excel(name = "检测阶别", width = 15)
	@ApiModelProperty(value = "检测阶别")
	private java.lang.String checkGrade;
	/**项目代码*/
	@Excel(name = "项目代码", width = 15)
	@ApiModelProperty(value = "项目代码")
	private java.lang.String projectCode;
	/**项目名称*/
	@Excel(name = "项目名称", width = 15)
	@ApiModelProperty(value = "项目名称")
	private java.lang.String projectName;
	/**项目类型*/
	@Excel(name = "项目类型", width = 15)
	@ApiModelProperty(value = "项目类型")
	private java.lang.String projectType;
	/**管控方式*/
	@Excel(name = "管控方式", width = 15)
	@ApiModelProperty(value = "管控方式")
	private java.lang.String controlManner;
	/**上限值*/
	@Excel(name = "上限值", width = 15)
	@ApiModelProperty(value = "上限值")
	private java.lang.String upLimit;
	/**下限值*/
	@Excel(name = "下限值", width = 15)
	@ApiModelProperty(value = "下限值")
	private java.lang.String downLimit;
	/**单位*/
	@Excel(name = "单位", width = 15)
	@ApiModelProperty(value = "单位")
	private java.lang.String unit;
	/**检测方法*/
	@Excel(name = "检测方法", width = 15)
	@ApiModelProperty(value = "检测方法")
	private java.lang.String checkWay;
	/**检测顺序*/
	@Excel(name = "检测顺序", width = 15)
	@ApiModelProperty(value = "检测顺序")
	private java.lang.String checkSequence;
	/**默认方式*/
	@Excel(name = "默认方式", width = 15)
	@ApiModelProperty(value = "默认方式")
	private java.lang.String defaultManner;
	/**检测内容*/
	@Excel(name = "检测内容", width = 15)
	@ApiModelProperty(value = "检测内容")
	private java.lang.String checkContent;
	/**物料检测项目id*/
	@ApiModelProperty(value = "物料检测项目id")
	private java.lang.String mcheckprojectId;
	/**所属部门*/
	@Excel(name = "所属部门", width = 15)
	@ApiModelProperty(value = "所属部门")
	private java.lang.String query2;
	/**备用3*/
	@Excel(name = "备用3", width = 15)
	@ApiModelProperty(value = "备用3")
	private java.lang.String query3;
	/**备用4*/
	@Excel(name = "备用4", width = 15)
	@ApiModelProperty(value = "备用4")
	private java.lang.String query4;
	/**备用5*/
	@Excel(name = "备用5", width = 15)
	@ApiModelProperty(value = "备用5")
	private java.lang.String query5;
	/**备用6*/
	@Excel(name = "备用6", width = 15)
	@ApiModelProperty(value = "备用6")
	private java.lang.String query6;

}
