package org.jeecg.modules.mes.storage.entity;

import java.io.Serializable;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import java.util.Date;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @Description: 仓库管理—验收入库子表
 * @Author: jeecg-boot
 * @Date:   2020-09-14
 * @Version: V1.0
 */
@ApiModel(value="mes_storage_acceptance对象", description="仓库管理—验收入库")
@Data
@TableName("mes_acceptance_item")
public class MesAcceptanceItem implements Serializable {
    private static final long serialVersionUID = 1L;

	/**id*/
	@TableId(type = IdType.ID_WORKER_STR)
	@ApiModelProperty(value = "id")
	private java.lang.String id;
	/**创建人*/
	@ApiModelProperty(value = "创建人")
	private java.lang.String createBy;
	/**创建日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
	@ApiModelProperty(value = "创建日期")
	private java.util.Date createTime;
	/**更新人*/
	@ApiModelProperty(value = "更新人")
	private java.lang.String updateBy;
	/**更新日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
	@ApiModelProperty(value = "更新日期")
	private java.util.Date updateTime;
	/**所属部门*/
	@ApiModelProperty(value = "所属部门")
	private java.lang.String sysOrgCode;
	/**行号*/
	@Excel(name = "行号", width = 15)
	@ApiModelProperty(value = "行号")
	private java.lang.String rowNum;
	/**产品编号*/
	@Excel(name = "产品编号", width = 15)
	@ApiModelProperty(value = "产品编号")
	private java.lang.String productCode;
	/**产品名称*/
	@Excel(name = "产品名称", width = 15)
	@ApiModelProperty(value = "产品名称")
	private java.lang.String productName;
	/**收货数量*/
	@Excel(name = "收货数量", width = 15)
	@ApiModelProperty(value = "收货数量")
	private java.lang.String receiveNum;
	/**验收货数*/
	@Excel(name = "验收货数", width = 15)
	@ApiModelProperty(value = "验收货数")
	private java.lang.String checkNum;
	/**单位*/
	@Excel(name = "单位", width = 15)
	@ApiModelProperty(value = "单位")
	private java.lang.String unit;
	/**单价*/
	@Excel(name = "单价", width = 15)
	@ApiModelProperty(value = "单价")
	private java.lang.String univalentPrice;
	/**金额*/
	@Excel(name = "金额", width = 15)
	@ApiModelProperty(value = "金额")
	private java.lang.String grossAccount;
	/**税率*/
	@Excel(name = "税率", width = 15)
	@ApiModelProperty(value = "税率")
	private java.lang.String taxRate;
	/**含税价格*/
	@Excel(name = "含税价格", width = 15)
	@ApiModelProperty(value = "含税价格")
	private java.lang.String includeTaxprice;
	/**含税金额*/
	@Excel(name = "含税金额", width = 15)
	@ApiModelProperty(value = "含税金额")
	private java.lang.String includeTaxaccount;
	/**外币价格*/
	@Excel(name = "外币价格", width = 15)
	@ApiModelProperty(value = "外币价格")
	private java.lang.String foreignPrice;
	/**外币金额*/
	@Excel(name = "外币金额", width = 15)
	@ApiModelProperty(value = "外币金额")
	private java.lang.String foreignAccount;
	/**货币*/
	@Excel(name = "货币", width = 15)
	@ApiModelProperty(value = "货币")
	private java.lang.String currency;
	/**汇率*/
	@Excel(name = "汇率", width = 15)
	@ApiModelProperty(value = "汇率")
	private java.lang.String exchangeRate;
	/**成本中心*/
	@Excel(name = "成本中心", width = 15)
	@ApiModelProperty(value = "成本中心")
	private java.lang.String costCenter;
	/**仓库编码*/
	@Excel(name = "仓库编码", width = 15)
	@ApiModelProperty(value = "仓库编码")
	private java.lang.String wareCode;
	/**库位*/
	@Excel(name = "库位", width = 15)
	@ApiModelProperty(value = "库位")
	private java.lang.String wareSite;
	/**基本单据类型*/
	@Excel(name = "基本单据类型", width = 15)
	@ApiModelProperty(value = "基本单据类型")
	private java.lang.String baseDockettype;
	/**基本单号*/
	@Excel(name = "基本单号", width = 15)
	@ApiModelProperty(value = "基本单号")
	private java.lang.String baseCode;
	/**基本行号*/
	@Excel(name = "基本行号", width = 15)
	@ApiModelProperty(value = "基本行号")
	private java.lang.String baseRownum;
	/**未清数量*/
	@Excel(name = "未清数量", width = 15)
	@ApiModelProperty(value = "未清数量")
	private java.lang.String overdueNum;
	/**行状态*/
	@Excel(name = "行状态", width = 15)
	@ApiModelProperty(value = "行状态")
	private java.lang.String rowState;
	/**货主编号*/
	@Excel(name = "货主编号", width = 15)
	@ApiModelProperty(value = "货主编号")
	private java.lang.String shipperCode;
	/**物流单号*/
	@Excel(name = "物流单号", width = 15)
	@ApiModelProperty(value = "物流单号")
	private java.lang.String logisticalCode;
	/**物流单行号*/
	@Excel(name = "物流单行号", width = 15)
	@ApiModelProperty(value = "物流单行号")
	private java.lang.String logisticalRownum;
	/**验收id*/
	@ApiModelProperty(value = "验收id")
	private java.lang.String acceptanceId;
	/**备用2*/
	@Excel(name = "备用2", width = 15)
	@ApiModelProperty(value = "备用2")
	private java.lang.String query2;
	/**备用3*/
	@Excel(name = "备用3", width = 15)
	@ApiModelProperty(value = "备用3")
	private java.lang.String query3;
	/**备用4*/
	@Excel(name = "备用4", width = 15)
	@ApiModelProperty(value = "备用4")
	private java.lang.String query4;
	/**备用5*/
	@Excel(name = "备用5", width = 15)
	@ApiModelProperty(value = "备用5")
	private java.lang.String query5;
	/**备用6*/
	@Excel(name = "备用6", width = 15)
	@ApiModelProperty(value = "备用6")
	private java.lang.String query6;
}
