package org.jeecg.modules.mes.chiefdata.entity;

import java.io.Serializable;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @Description: 主数据—物料
 * @Author: jeecg-boot
 * @Date:   2020-11-17
 * @Version: V1.0
 */
@ApiModel(value="mes_chiefdata_materiel对象", description="主数据—物料")
@Data
@TableName("mes_chiefdata_materiel")
public class MesChiefdataMateriel implements Serializable {
    private static final long serialVersionUID = 1L;

	/**id*/
	@TableId(type = IdType.ID_WORKER_STR)
    @ApiModelProperty(value = "id")
    private java.lang.String id;
	/**创建人*/
    @ApiModelProperty(value = "创建人")
    private java.lang.String createBy;
	/**创建日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "创建日期")
    private java.util.Date createTime;
	/**更新人*/
    @ApiModelProperty(value = "更新人")
    private java.lang.String updateBy;
	/**更新日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "更新日期")
    private java.util.Date updateTime;
	/**所属部门*/
    @ApiModelProperty(value = "所属部门")
    private java.lang.String sysOrgCode;
	/**料号*/
	@Excel(name = "料号", width = 15)
    @ApiModelProperty(value = "料号")
    private java.lang.String materielCode;
    /**商品编码*/
    @Excel(name = "商品编码", width = 15)
    @ApiModelProperty(value = "商品编码")
    private java.lang.String productCode;
	/**线别*/
	@Excel(name = "线别", width = 15, dicCode = "mgroup_code")
    @Dict(dicCode = "mgroup_code")
    @ApiModelProperty(value = "线别")
    private java.lang.String mgroupCode;
	/**品名*/
	@Excel(name = "品名", width = 15)
    @ApiModelProperty(value = "品名")
    private java.lang.String productName;
	/**规格*/
	@Excel(name = "规格", width = 15)
    @ApiModelProperty(value = "规格")
    private java.lang.String gauge;
	/**物料类别*/
	@Excel(name = "物料类别", width = 15, dicCode = "materiel_sort")
    @Dict(dicCode = "materiel_sort")
    @ApiModelProperty(value = "物料类别")
    private java.lang.String materielSort;
	/**物料类型*/
	@Excel(name = "物料类型", width = 15, dicCode = "materiel_type")
    @Dict(dicCode = "materiel_type")
    @ApiModelProperty(value = "物料类型")
    private java.lang.String materielType;
	/**物料来源*/
	@Excel(name = "物料来源", width = 15, dicCode = "materiel_source")
    @Dict(dicCode = "materiel_source")
    @ApiModelProperty(value = "物料来源")
    private java.lang.String materielSource;
	/**单位*/
	@Excel(name = "单位", width = 15)
    @ApiModelProperty(value = "单位")
    private java.lang.String unit;
	/**产品条码规则*/
	@Excel(name = "产品条码规则", width = 15)
    @ApiModelProperty(value = "产品条码规则")
    private java.lang.String barcodeRule;
	/**客户id*/
//	@Excel(name = "客户id", width = 15)
    @ApiModelProperty(value = "客户id")
    private java.lang.String clientId;
	/**客户*/
	@Excel(name = "客户", width = 15)
    @ApiModelProperty(value = "客户")
    private java.lang.String client;
	/**供应商id*/
//	@Excel(name = "供应商id", width = 15)
    @ApiModelProperty(value = "供应商id")
    private java.lang.String supplierId;
	/**供应商*/
	@Excel(name = "供应商", width = 15)
    @ApiModelProperty(value = "供应商")
    private java.lang.String supplier;
	/**是否锁定*/
	@Excel(name = "是否锁定", width = 15, dicCode = "yn")
    @Dict(dicCode = "yn")
    @ApiModelProperty(value = "是否锁定")
    private java.lang.String ifLock;
	/**备注*/
	@Excel(name = "备注", width = 15)
    @ApiModelProperty(value = "备注")
    private java.lang.String note;
	/**客户料号*/
	@Excel(name = "客户料号", width = 15)
    @ApiModelProperty(value = "客户料号")
    private java.lang.String query2;
    /**单重*/
    @Excel(name = "单重", width = 15)
    @ApiModelProperty(value = "单重")
    private java.lang.String unitWeight;
    /**批次号*/
    @Excel(name = "批次号", width = 15)
    @ApiModelProperty(value = "批次号")
    private java.lang.String sectionNo;
    /**公司编码*/
    @Excel(name = "公司编码", width = 15)
    @ApiModelProperty(value = "公司编码")
    private java.lang.String companyCode;
	/**备用3*/
	@Excel(name = "", width = 15)
    @ApiModelProperty(value = "备用3")
    private java.lang.String query3;
	/**备用4*/
//	@Excel(name = "备用4", width = 15)
    @ApiModelProperty(value = "备用4")
    private java.lang.String query4;
	/**备用5*/
//	@Excel(name = "备用5", width = 15)
    @ApiModelProperty(value = "备用5")
    private java.lang.String query5;
	/**是否报关（默认为0）*/
	@Excel(name = "是否报关（默认为0）", width = 15)
    @ApiModelProperty(value = "是否报关（默认为0）")
    private java.lang.String importState;

    /**搅拌上限时间(分钟)*/
    @TableField(exist = false)
    @Excel(name = "搅拌上限时间(分钟)", width = 15)
    @ApiModelProperty(value = "搅拌上限时间(分钟)")
    private java.lang.String stirTimelimit;

    /**搅拌时间(分钟)*/
    @TableField(exist = false)
    @Excel(name = "搅拌时间(分钟)", width = 15)
    @ApiModelProperty(value = "搅拌时间(分钟)")
    private java.lang.String stirTime;

    /**回温时长(分钟)*/
    @TableField(exist = false)
    @Excel(name = "回温时长(分钟)", width = 15)
    @ApiModelProperty(value = "回温时长(分钟)")
    private java.lang.String heatTime;

    /**回温上限时间(分钟)*/
    @TableField(exist = false)
    @Excel(name = "回温上限时间(分钟)", width = 15)
    @ApiModelProperty(value = "回温上限时间(分钟)")
    private java.lang.String heatTimelimit;

    /**生产阶别*/
    @Excel(name = "生产阶别", width = 15)
    @ApiModelProperty(value = "生产阶别")
    private java.lang.String produceGrade;
}
