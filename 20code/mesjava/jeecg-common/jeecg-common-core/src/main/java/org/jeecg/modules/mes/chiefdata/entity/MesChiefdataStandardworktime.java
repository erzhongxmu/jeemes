package org.jeecg.modules.mes.chiefdata.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: 主数据—标准工时
 * @Author: jeecg-boot
 * @Date:   2020-09-17
 * @Version: V1.0
 */
@Data
@TableName("mes_chiefdata_standardworktime")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="mes_chiefdata_standardworktime对象", description="主数据—标准工时")
public class MesChiefdataStandardworktime implements Serializable {
    private static final long serialVersionUID = 1L;

	/**id*/
	@TableId(type = IdType.ID_WORKER_STR)
    @ApiModelProperty(value = "id")
    private java.lang.String id;
	/**创建人*/
    @ApiModelProperty(value = "创建人")
    private java.lang.String createBy;
	/**创建日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "创建日期")
    private java.util.Date createTime;
	/**更新人*/
    @ApiModelProperty(value = "更新人")
    private java.lang.String updateBy;
	/**更新日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "更新日期")
    private java.util.Date updateTime;
	/**所属部门*/
    @ApiModelProperty(value = "所属部门")
    private java.lang.String sysOrgCode;
	/**生产阶别*/
	@Excel(name = "生产阶别", width = 15, dicCode = "produce_grade")
	@Dict(dicCode = "produce_grade")
    @ApiModelProperty(value = "生产阶别")
    private java.lang.String produceGrade;
	/**机种名称*/
	@Excel(name = "机种名称", width = 15)
    @ApiModelProperty(value = "机种名称")
    private java.lang.String machineName;
	/**生产线别*/
	@Excel(name = "生产线别", width = 15)
    @ApiModelProperty(value = "生产线别")
    private java.lang.String produceLinesort;
	/**机种规格*/
	@Excel(name = "机种规格", width = 15)
    @ApiModelProperty(value = "机种规格")
    private java.lang.String machineGague;
	/**机种*/
	@Excel(name = "机种", width = 15)
    @ApiModelProperty(value = "机种")
    private java.lang.String machineSort;
	/**标准工时(S)*/
	@Excel(name = "标准工时(S)", width = 15)
    @ApiModelProperty(value = "标准工时(S)")
    private java.lang.String standardWorktime;
	/**加工面别*/
	@Excel(name = "加工面别", width = 15, dicCode = "process_face")
	@Dict(dicCode = "process_face")
    @ApiModelProperty(value = "加工面别")
    private java.lang.String processFace;
	/**换线时间(Min)*/
	@Excel(name = "换线时间(Min)", width = 15)
    @ApiModelProperty(value = "换线时间(Min)")
    private java.lang.String changeLinetime;
	/**生产工艺*/
	@Excel(name = "生产工艺", width = 15)
    @ApiModelProperty(value = "生产工艺")
    private java.lang.String produceCraft;
	/**排程计算标志*/
	@Excel(name = "排程计算标志", width = 15, dicCode = "yn")
	@Dict(dicCode = "yn")
    @ApiModelProperty(value = "排程计算标志")
    private java.lang.String eissionToken;
	/**SMT轨道*/
	@Excel(name = "SMT轨道", width = 15, dicCode = "smt_track")
	@Dict(dicCode = "smt_track")
    @ApiModelProperty(value = "SMT轨道")
    private java.lang.String smtTrack;
	/**备用1*/
	@Excel(name = "备用1", width = 15)
    @ApiModelProperty(value = "备用1")
    private java.lang.String query1;
	/**备用2*/
	@Excel(name = "备用2", width = 15)
    @ApiModelProperty(value = "备用2")
    private java.lang.String query2;
	/**备用3*/
	@Excel(name = "备用3", width = 15)
    @ApiModelProperty(value = "备用3")
    private java.lang.String query3;
	/**备用4*/
	@Excel(name = "备用4", width = 15)
    @ApiModelProperty(value = "备用4")
    private java.lang.String query4;
	/**备用5*/
	@Excel(name = "备用5", width = 15)
    @ApiModelProperty(value = "备用5")
    private java.lang.String query5;
	/**备用6*/
	@Excel(name = "备用6", width = 15)
    @ApiModelProperty(value = "备用6")
    private java.lang.String query6;
}
