package org.jeecg.modules.mes.order.entity;

import java.io.Serializable;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import org.jeecg.common.aspect.annotation.Dict;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import java.util.Date;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @Description: 订单管理—生产订单工序子表
 * @Author: jeecg-boot
 * @Date:   2020-11-06
 * @Version: V1.0
 */
@Data
@TableName("mes_produce_process")
@ApiModel(value="mes_order_produce对象", description="订单管理—生产订单")
public class MesProduceProcess implements Serializable {
    private static final long serialVersionUID = 1L;

	/**id*/
	@TableId(type = IdType.ID_WORKER_STR)
	@ApiModelProperty(value = "id")
	private java.lang.String id;
	/**创建人*/
	@ApiModelProperty(value = "创建人")
	private java.lang.String createBy;
	/**创建日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
	@ApiModelProperty(value = "创建日期")
	private java.util.Date createTime;
	/**更新人*/
	@ApiModelProperty(value = "更新人")
	private java.lang.String updateBy;
	/**更新日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
	@ApiModelProperty(value = "更新日期")
	private java.util.Date updateTime;
	/**所属部门*/
	@ApiModelProperty(value = "所属部门")
	private java.lang.String sysOrgCode;
	/**工序编号*/
	@Excel(name = "工序编号", width = 15)
	@ApiModelProperty(value = "工序编号")
	private java.lang.String processCode;
	/**工序名称*/
	@Excel(name = "工序名称", width = 15)
	@ApiModelProperty(value = "工序名称")
	private java.lang.String processName;
	/**开始时间*/
	@Excel(name = "开始时间", width = 20, format = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty(value = "开始时间")
	private java.util.Date startTime;
	/**工作中心*/
	@Excel(name = "工作中心", width = 15)
	@ApiModelProperty(value = "工作中心")
	private java.lang.String workCenter;
	/**工厂id*/
	@Excel(name = "工厂id", width = 15)
	@ApiModelProperty(value = "工厂id")
	private java.lang.String factoryId;
	/**工厂code*/
	@Excel(name = "工厂code", width = 15)
	@ApiModelProperty(value = "工厂code")
	private java.lang.String factoryCode;
	/**工厂名称*/
	@Excel(name = "工厂名称", width = 15)
	@ApiModelProperty(value = "工厂名称")
	private java.lang.String factoryName;
	/**控制码*/
	@Excel(name = "控制码", width = 15)
	@ApiModelProperty(value = "控制码")
	private java.lang.String controlCode;
	/**标文本*/
	@Excel(name = "标文本", width = 15)
	@ApiModelProperty(value = "标文本")
	private java.lang.String markText;
	/**工序短文本*/
	@Excel(name = "工序短文本", width = 15)
	@ApiModelProperty(value = "工序短文本")
	private java.lang.String processShortext;
	/**系统状态*/
	@Excel(name = "系统状态", width = 15)
	@ApiModelProperty(value = "系统状态")
	private java.lang.String systemState;
	/**结束时间*/
	@Excel(name = "结束时间", width = 20, format = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty(value = "结束时间")
	private java.util.Date endTime;
	/**工序数量*/
	@Excel(name = "工序数量", width = 15)
	@ApiModelProperty(value = "工序数量")
	private java.lang.String processNum;
	/**生产订单id*/
	@ApiModelProperty(value = "生产订单id")
	private java.lang.String proorderId;
	/**行号*/
	@Excel(name = "行号", width = 15)
	@ApiModelProperty(value = "行号")
	private java.lang.String rowNum;
	/**工序id*/
	@Excel(name = "工序id", width = 15)
	@ApiModelProperty(value = "工序id")
	private java.lang.String processId;
	/**备用3*/
	@Excel(name = "备用3", width = 15)
	@ApiModelProperty(value = "备用3")
	private java.lang.String query3;
	/**备用4*/
	@Excel(name = "备用4", width = 15)
	@ApiModelProperty(value = "备用4")
	private java.lang.String query4;
	/**备用5*/
	@Excel(name = "备用5", width = 15)
	@ApiModelProperty(value = "备用5")
	private java.lang.String query5;
	/**备用6*/
	@Excel(name = "备用6", width = 15)
	@ApiModelProperty(value = "备用6")
	private java.lang.String query6;
}
