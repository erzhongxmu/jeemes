package org.jeecg.modules.mes.order.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.jeecg.common.aspect.annotation.Dict;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;

/**
 * @Description: 订单管理—销售订单子表
 * @Author: jeecg-boot
 * @Date:   2020-11-06
 * @Version: V1.0
 */
@Data
@TableName("mes_sale_item")
@ApiModel(value="mes_order_sale对象", description="订单管理—销售订单")
public class MesSaleItem implements Serializable {
    private static final long serialVersionUID = 1L;

	/**id*/
	@TableId(type = IdType.ID_WORKER_STR)
	@ApiModelProperty(value = "id")
	private java.lang.String id;
	/**创建人*/
	@ApiModelProperty(value = "创建人")
	private java.lang.String createBy;
	/**创建日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
	@ApiModelProperty(value = "创建日期")
	private java.util.Date createTime;
	/**更新人*/
	@ApiModelProperty(value = "更新人")
	private java.lang.String updateBy;
	/**更新日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
	@ApiModelProperty(value = "更新日期")
	private java.util.Date updateTime;
	/**所属部门*/
	@ApiModelProperty(value = "所属部门")
	private java.lang.String sysOrgCode;
	/**物料id*/
	@Excel(name = "物料id", width = 15)
	@ApiModelProperty(value = "物料id")
	private java.lang.String materielId;
	/**料号*/
	@Excel(name = "料号", width = 15)
	@ApiModelProperty(value = "料号")
	private java.lang.String materielCode;
	/**物料*/
	@Excel(name = "物料", width = 15)
	@ApiModelProperty(value = "物料")
	private java.lang.String materielName;
	/**订单数量*/
	@Excel(name = "订单数量", width = 15)
	@ApiModelProperty(value = "订单数量")
	private java.lang.String orderNum;
	/**单位*/
	@Excel(name = "单位", width = 15)
	@ApiModelProperty(value = "单位")
	private java.lang.String unit;
	/**描述*/
	@Excel(name = "描述", width = 15)
	@ApiModelProperty(value = "描述")
	private java.lang.String description;
	/**客户物料编号*/
	@Excel(name = "客户物料编号", width = 15)
	@ApiModelProperty(value = "客户物料编号")
	private java.lang.String clientMaterielcode;
	/**类别*/
	@Excel(name = "类别", width = 15)
	@ApiModelProperty(value = "类别")
	private java.lang.String category;
	/**首个日期*/
	@Excel(name = "首个日期", width = 15, format = "yyyy-MM-dd")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
	@ApiModelProperty(value = "首个日期")
	private java.util.Date firstDate;
	/**工厂id*/
	@Excel(name = "工厂id", width = 15)
	@ApiModelProperty(value = "工厂id")
	private java.lang.String factoryId;
	/**工厂编号*/
	@Excel(name = "工厂编号", width = 15)
	@ApiModelProperty(value = "工厂编号")
	private java.lang.String factoryCode;
	/**工厂名称*/
	@Excel(name = "工厂名称", width = 15)
	@ApiModelProperty(value = "工厂名称")
	private java.lang.String factoryName;
	/**批次*/
	@Excel(name = "批次", width = 15)
	@ApiModelProperty(value = "批次")
	private java.lang.String batchNum;
	/**类型*/
	@Excel(name = "类型", width = 15)
	@ApiModelProperty(value = "类型")
	private java.lang.String type;
	/**金额*/
	@Excel(name = "金额", width = 15)
	@ApiModelProperty(value = "金额")
	private java.lang.String account;
	/**货币*/
	@Excel(name = "货币", width = 15)
    @Dict(dicCode = "current_money")
	@ApiModelProperty(value = "货币")
	private java.lang.String currency;
	/**销售订单id*/
	@ApiModelProperty(value = "销售订单id")
	private java.lang.String saleorderId;
	/**行号*/
	@Excel(name = "行号", width = 15)
	@ApiModelProperty(value = "行号")
	private java.lang.String rowNum;
	/**库存地点*/
	@Excel(name = "库存地点", width = 15)
	@ApiModelProperty(value = "库存地点")
	private java.lang.String storageSite;
	/**物料规格*/
	@Excel(name = "物料规格", width = 15)
	@ApiModelProperty(value = "物料规格")
	private java.lang.String materielGauge;
	/**未发货数量*/
	@Excel(name = "未发货数量", width = 15)
	@ApiModelProperty(value = "未发货数量")
	private java.lang.String undeliveryNum;
	/**状态*/
	@Excel(name = "状态", width = 15)
	@Dict(dicCode = "state")
	@ApiModelProperty(value = "状态")
	private java.lang.String state;
	/**物料标记*/
	@Excel(name = "物料标记", width = 15)
	@ApiModelProperty(value = "物料标记")
	private java.lang.String imgExgFlag;
	/**项号*/
	@Excel(name = "项号", width = 15)
	@ApiModelProperty(value = "项号")
	private java.lang.String itemNo;
	/**批次号*/
	@Excel(name = "批次号", width = 15)
	@ApiModelProperty(value = "批次号")
	private java.lang.String sectionNo;
	/**公司编码*/
	@Excel(name = "公司编码", width = 15)
	@ApiModelProperty(value = "公司编码")
	private java.lang.String companyCode;
	/**板数*/
	@Excel(name = "板数", width = 15)
	@ApiModelProperty(value = "板数")
	private java.lang.String palletNum;
	/**件数*/
	@Excel(name = "件数", width = 15)
	@ApiModelProperty(value = "件数")
	private java.lang.String pcs;
	/**包装种类*/
	@Excel(name = "包装种类", width = 15)
	@ApiModelProperty(value = "包装种类")
	private java.lang.String wrap;
	/**箱号*/
	@Excel(name = "箱号", width = 15)
	@ApiModelProperty(value = "箱号")
	private java.lang.String containerNo;
	/**净重*/
	@Excel(name = "净重", width = 15)
	@ApiModelProperty(value = "净重")
	private java.lang.String netWeight;
	/**毛重*/
	@Excel(name = "毛重", width = 15)
	@ApiModelProperty(value = "毛重")
	private java.lang.String grossWeight;
	/**立方数*/
	@Excel(name = "立方数", width = 15)
	@ApiModelProperty(value = "立方数")
	private java.lang.String zextendItemField1;
	/**目的国*/
	@Excel(name = "目的国", width = 15)
	@ApiModelProperty(value = "目的国")
	private java.lang.String destinationCountry;
	/**客户料号*/
	@Excel(name = "客户料号", width = 15)
	@ApiModelProperty(value = "客户料号")
	private java.lang.String clientCode;
	/**商品编码*/
	@Excel(name = "商品编码", width = 15)
	@ApiModelProperty(value = "商品编码")
	private java.lang.String productCode;
}
