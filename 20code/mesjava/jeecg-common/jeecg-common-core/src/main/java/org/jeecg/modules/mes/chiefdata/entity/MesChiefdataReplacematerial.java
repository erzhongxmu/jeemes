package org.jeecg.modules.mes.chiefdata.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.jeecg.common.aspect.annotation.Dict;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;

/**
 * @Description: 主数据—替代料
 * @Author: jeecg-boot
 * @Date:   2020-09-17
 * @Version: V1.0
 */
@Data
@TableName("mes_chiefdata_replacematerial")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="mes_chiefdata_replacematerial对象", description="主数据—替代料")
public class MesChiefdataReplacematerial implements Serializable {
    private static final long serialVersionUID = 1L;

	/**id*/
	@TableId(type = IdType.ID_WORKER_STR)
    @ApiModelProperty(value = "id")
    private java.lang.String id;
	/**创建人*/
    @ApiModelProperty(value = "创建人")
    private java.lang.String createBy;
	/**创建日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "创建日期")
    private java.util.Date createTime;
	/**更新人*/
    @ApiModelProperty(value = "更新人")
    private java.lang.String updateBy;
	/**更新日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "更新日期")
    private java.util.Date updateTime;
	/**所属部门*/
    @ApiModelProperty(value = "所属部门")
    private java.lang.String sysOrgCode;
	/**类型*/
//	@Excel(name = "类型", width = 15, dicCode = "fobid_type")
	@Dict(dicCode = "fobid_type")
    @ApiModelProperty(value = "类型")
    private java.lang.String type;
	/**有效标志*/
//	@Excel(name = "有效标志", width = 15, dicCode = "yn")
	@Dict(dicCode = "yn")
    @ApiModelProperty(value = "有效标志")
    private java.lang.String validToken;
	/**工单*/
//	@Excel(name = "工单", width = 15)
    @ApiModelProperty(value = "工单")
    private java.lang.String workDocket;
	/**成品料号*/
	@Excel(name = "成品料号", width = 15)
    @ApiModelProperty(value = "成品料号")
    private java.lang.String machineSort;
	/**成品规格*/
	@Excel(name = "成品规格", width = 15)
    @ApiModelProperty(value = "成品规格")
    private java.lang.String machineEdition;
	/**备注*/
//	@Excel(name = "备注", width = 15)
    @ApiModelProperty(value = "备注")
    private java.lang.String notes;
	/**主料料号*/
	@Excel(name = "主料料号", width = 15)
    @ApiModelProperty(value = "主料料号")
    private java.lang.String mainCode;
	/**主料名称*/
	@Excel(name = "主料名称", width = 15)
    @ApiModelProperty(value = "主料名称")
    private java.lang.String mainName;
	/**主料规格*/
	@Excel(name = "主料规格", width = 15)
    @ApiModelProperty(value = "主料规格")
    private java.lang.String mainGague;
	/**替代料号*/
	@Excel(name = "替代料号", width = 15)
    @ApiModelProperty(value = "替代料号")
    private java.lang.String replaceCode;
	/**替代料名称*/
	@Excel(name = "替代料名称", width = 15)
    @ApiModelProperty(value = "替代料名称")
    private java.lang.String replaceName;
	/**替代料规格*/
	@Excel(name = "替代料规格", width = 15)
    @ApiModelProperty(value = "替代料规格")
    private java.lang.String replaceGague;
	/**备用1*/
//	@Excel(name = "备用1", width = 15)
    @ApiModelProperty(value = "备用1")
    private java.lang.String query1;
	/**备用2*/
//	@Excel(name = "备用2", width = 15)
    @ApiModelProperty(value = "备用2")
    private java.lang.String query2;
	/**备用3*/
//	@Excel(name = "备用3", width = 15)
    @ApiModelProperty(value = "备用3")
    private java.lang.String query3;
	/**备用4*/
//	@Excel(name = "备用4", width = 15)
    @ApiModelProperty(value = "备用4")
    private java.lang.String query4;
	/**备用5*/
//	@Excel(name = "备用5", width = 15)
    @ApiModelProperty(value = "备用5")
    private java.lang.String query5;
	/**备用6*/
//	@Excel(name = "备用6", width = 15)
    @ApiModelProperty(value = "备用6")
    private java.lang.String query6;
}
