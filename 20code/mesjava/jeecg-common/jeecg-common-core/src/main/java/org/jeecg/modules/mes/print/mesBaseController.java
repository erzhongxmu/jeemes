package org.jeecg.modules.mes.print;

import cn.hutool.core.util.RandomUtil;
import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

import org.apache.http.Consts;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.CharsetUtils;
import org.apache.http.util.EntityUtils;
import org.apache.shiro.SecurityUtils;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.constant.CommonConstant;
import org.jeecg.common.system.api.ISysBaseAPI;
import org.jeecg.common.system.vo.LoginUser;
import org.jeecg.common.util.*;
import org.jeecg.common.util.oss.OssBootUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.math.BigDecimal;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.text.DecimalFormat;
import java.util.*;
import java.util.zip.ZipOutputStream;

/**
 * @Title: 合同生成器
 * @Description: 合同生成器
 * @Author zhaodui
 * @Date 2019-03-25
 * @Version V1.0
 */
@Slf4j
@RestController
@RequestMapping("/mes/base")
@Api(tags="基础控制器")
public class mesBaseController {


	@GetMapping(value = "/contract")
	@ApiOperation("批量生成合同")
	public Result<?> gencontract(conHead conHead,
            HttpServletRequest request, HttpServletResponse response) {
		Result<?> result = new Result<>();
		String msg = "打印成功";
		List<conmap> conmapList = new ArrayList<>();//打印字段
		conmap conmap = new conmap();
		conmap.setConKey("name");
		conmap.setConValue("xinmg");
		conmapList.add(conmap);
		conHead.setConmapList(conmapList);
        //todo 合同表格生成
        List<TableEntity> tlist = new ArrayList<>();//打印列表
        //生成的表格列数可根据此处的标题数量生成
		String[] strs = new String[]{"序号","名称","审批人","审批时间","审批意见"};
		    int xuhao=0;
			conHead.setStrings(strs);
 			TableEntity tab1 = new TableEntity();
			tab1.setQuery01("1");
			tab1.setQuery02("test");
			tab1.setQuery03("test");
			tab1.setQuery04("test");
			tab1.setQuery05("test");
			xuhao++;
			tlist.add(tab1);
		conHead.setTableList(tlist);
			String hturl = "";//06

			try{
				hturl = mesUtil.gencontract(conHead.getPrintserver(),conHead);
				msg = hturl;
			}catch (Exception e){
				msg = "打印失败";
			}
		result.setMessage(msg);
		return result;
	}


}
