package org.jeecg.modules.mes.storage.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: 仓库管理—拣配单
 * @Author: jeecg-boot
 * @Date:   2020-09-14
 * @Version: V1.0
 */
@Data
@TableName("mes_storage_assign")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="mes_storage_assign对象", description="仓库管理—拣配单")
public class MesStorageAssign implements Serializable {
    private static final long serialVersionUID = 1L;

	/**id*/
	@TableId(type = IdType.ID_WORKER_STR)
    @ApiModelProperty(value = "id")
    private java.lang.String id;
	/**创建人*/
    @ApiModelProperty(value = "创建人")
    private java.lang.String createBy;
	/**创建日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "创建日期")
    private java.util.Date createTime;
	/**更新人*/
    @ApiModelProperty(value = "更新人")
    private java.lang.String updateBy;
	/**更新日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "更新日期")
    private java.util.Date updateTime;
	/**所属部门*/
    @ApiModelProperty(value = "所属部门")
    private java.lang.String sysOrgCode;
	/**预配ID*/
	@Excel(name = "预配ID", width = 15)
    @ApiModelProperty(value = "预配ID")
    private java.lang.String preassignId;
	/**波次单号*/
	@Excel(name = "波次单号", width = 15)
    @ApiModelProperty(value = "波次单号")
    private java.lang.String waveCode;
	/**出库单号*/
	@Excel(name = "出库单号", width = 15)
    @ApiModelProperty(value = "出库单号")
    private java.lang.String outwareCode;
	/**拣配单号*/
	@Excel(name = "拣配单号", width = 15)
    @ApiModelProperty(value = "拣配单号")
    private java.lang.String assignCode;
	/**拣配行号*/
	@Excel(name = "拣配行号", width = 15)
    @ApiModelProperty(value = "拣配行号")
    private java.lang.String assignRownum;
	/**出库行号*/
	@Excel(name = "出库行号", width = 15)
    @ApiModelProperty(value = "出库行号")
    private java.lang.String outwareRownum;
	/**产品编号*/
	@Excel(name = "产品编号", width = 15)
    @ApiModelProperty(value = "产品编号")
    private java.lang.String productCode;
	/**产品名称*/
	@Excel(name = "产品名称", width = 15)
    @ApiModelProperty(value = "产品名称")
    private java.lang.String productName;
	/**货主编号*/
	@Excel(name = "货主编号", width = 15)
    @ApiModelProperty(value = "货主编号")
    private java.lang.String shipperNum;
	/**批号*/
	@Excel(name = "批号", width = 15)
    @ApiModelProperty(value = "批号")
    private java.lang.String batchNum;
	/**包装编码*/
	@Excel(name = "包装编码", width = 15)
    @ApiModelProperty(value = "包装编码")
    private java.lang.String packageCode;
	/**包装单位*/
	@Excel(name = "包装单位", width = 15)
    @ApiModelProperty(value = "包装单位")
    private java.lang.String packageUnit;
	/**预配数量*/
	@Excel(name = "预配数量", width = 15)
    @ApiModelProperty(value = "预配数量")
    private java.lang.String preassignNum;
	/**单位*/
	@Excel(name = "单位", width = 15)
    @ApiModelProperty(value = "单位")
    private java.lang.String unit;
	/**仓库编号*/
	@Excel(name = "仓库编号", width = 15)
    @ApiModelProperty(value = "仓库编号")
    private java.lang.String wareCode;
	/**区域码*/
	@Excel(name = "区域码", width = 15)
    @ApiModelProperty(value = "区域码")
    private java.lang.String regionCode;
	/**库位*/
	@Excel(name = "库位", width = 15)
    @ApiModelProperty(value = "库位")
    private java.lang.String wareSite;
	/**拣货人*/
	@Excel(name = "拣货人", width = 15)
    @ApiModelProperty(value = "拣货人")
    private java.lang.String sortPerson;
	/**审核人*/
	@Excel(name = "审核人", width = 15)
    @ApiModelProperty(value = "审核人")
    private java.lang.String verifyPerson;
	/**拣货时间*/
	@Excel(name = "拣货时间", width = 15, format = "yyyy-MM-dd")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "拣货时间")
    private java.util.Date sortTime;
	/**审核时间*/
	@Excel(name = "审核时间", width = 15, format = "yyyy-MM-dd")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "审核时间")
    private java.util.Date verifyTime;
	/**打包人*/
	@Excel(name = "打包人", width = 15)
    @ApiModelProperty(value = "打包人")
    private java.lang.String parcelPerson;
	/**发货人*/
	@Excel(name = "发货人", width = 15)
    @ApiModelProperty(value = "发货人")
    private java.lang.String sendPerson;
	/**打包时间*/
	@Excel(name = "打包时间", width = 15, format = "yyyy-MM-dd")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "打包时间")
    private java.util.Date parcelTime;
	/**发货时间*/
	@Excel(name = "发货时间", width = 15, format = "yyyy-MM-dd")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "发货时间")
    private java.util.Date sendTime;
	/**打印次数*/
	@Excel(name = "打印次数", width = 15)
    @ApiModelProperty(value = "打印次数")
    private java.lang.String printNum;
	/**越库类型*/
	@Excel(name = "越库类型", width = 15)
    @ApiModelProperty(value = "越库类型")
    private java.lang.String crossWaretype;
	/**快递单号*/
	@Excel(name = "快递单号", width = 15)
    @ApiModelProperty(value = "快递单号")
    private java.lang.String deliveryCode;
	/**包裹总重量*/
	@Excel(name = "包裹总重量", width = 15)
    @ApiModelProperty(value = "包裹总重量")
    private java.lang.String packageWeight;
	/**备用1*/
	@Excel(name = "备用1", width = 15)
    @ApiModelProperty(value = "备用1")
    private java.lang.String query1;
	/**备用2*/
	@Excel(name = "备用2", width = 15)
    @ApiModelProperty(value = "备用2")
    private java.lang.String query2;
	/**备用3*/
	@Excel(name = "备用3", width = 15)
    @ApiModelProperty(value = "备用3")
    private java.lang.String query3;
	/**备用4*/
	@Excel(name = "备用4", width = 15)
    @ApiModelProperty(value = "备用4")
    private java.lang.String query4;
	/**备用5*/
	@Excel(name = "备用5", width = 15)
    @ApiModelProperty(value = "备用5")
    private java.lang.String query5;
	/**备用6*/
	@Excel(name = "备用6", width = 15)
    @ApiModelProperty(value = "备用6")
    private java.lang.String query6;
}
