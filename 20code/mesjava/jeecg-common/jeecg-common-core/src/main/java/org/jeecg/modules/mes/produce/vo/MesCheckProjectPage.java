package org.jeecg.modules.mes.produce.vo;

import java.util.List;
import org.jeecg.modules.mes.produce.entity.MesCheckProject;
import org.jeecg.modules.mes.produce.entity.MesInstructHeadman;
import lombok.Data;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecgframework.poi.excel.annotation.ExcelEntity;
import org.jeecgframework.poi.excel.annotation.ExcelCollection;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import java.util.Date;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @Description: 制造中心-检测项目基本信息
 * @Author: jeecg-boot
 * @Date:   2020-10-13
 * @Version: V1.0
 */
@Data
@ApiModel(value="mes_check_projectPage对象", description="制造中心-检测项目基本信息")
public class MesCheckProjectPage {

	/**id*/
	@ApiModelProperty(value = "id")
	private java.lang.String id;
	/**创建人*/
	@ApiModelProperty(value = "创建人")
	private java.lang.String createBy;
	/**创建日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
	@ApiModelProperty(value = "创建日期")
	private java.util.Date createTime;
	/**更新人*/
	@ApiModelProperty(value = "更新人")
	private java.lang.String updateBy;
	/**更新日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
	@ApiModelProperty(value = "更新日期")
	private java.util.Date updateTime;
	/**所属部门*/
	@ApiModelProperty(value = "所属部门")
	private java.lang.String sysOrgCode;
	/**项目编码*/
	@Excel(name = "项目编码", width = 15)
	@ApiModelProperty(value = "项目编码")
	private java.lang.String projectCode;
	/**项目名称*/
	@Excel(name = "项目名称", width = 15)
	@ApiModelProperty(value = "项目名称")
	private java.lang.String projectName;
	/**项目类型*/
	@Excel(name = "项目类型", width = 15)
	@ApiModelProperty(value = "项目类型")
	private java.lang.String projectType;
	/**影响因素*/
	@Excel(name = "影响因素", width = 15)
	@ApiModelProperty(value = "影响因素")
	private java.lang.String affectFactor;
	/**生产阶别*/
	@Excel(name = "生产阶别", width = 15)
	@ApiModelProperty(value = "生产阶别")
	private java.lang.String produceGrade;
	/**备注*/
	@Excel(name = "备注", width = 15)
	@ApiModelProperty(value = "备注")
	private java.lang.String notes;
	/**备用1*/
	@Excel(name = "备用1", width = 15)
	@ApiModelProperty(value = "备用1")
	private java.lang.String query1;
	/**备用2*/
	@Excel(name = "备用2", width = 15)
	@ApiModelProperty(value = "备用2")
	private java.lang.String query2;
	/**备用3*/
	@Excel(name = "备用3", width = 15)
	@ApiModelProperty(value = "备用3")
	private java.lang.String query3;
	/**备用4*/
	@Excel(name = "备用4", width = 15)
	@ApiModelProperty(value = "备用4")
	private java.lang.String query4;
	/**备用5*/
	@Excel(name = "备用5", width = 15)
	@ApiModelProperty(value = "备用5")
	private java.lang.String query5;
	/**备用6*/
	@Excel(name = "备用6", width = 15)
	@ApiModelProperty(value = "备用6")
	private java.lang.String query6;
	
	@ExcelCollection(name="制造中心-检测项目(指示单项目责任人)")
	@ApiModelProperty(value = "制造中心-检测项目(指示单项目责任人)")
	private List<MesInstructHeadman> mesInstructHeadmanList;
	
}
