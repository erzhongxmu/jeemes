package org.jeecg.modules.mes.storage.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;

/**
 * @Description: 物料凭证抬头
 * @Author: jeecg-boot
 * @Date:   2020-11-11
 * @Version: V1.0
 */
@Data
@TableName("mes_certificate_perk")
@ApiModel(value="mes_certificate_perk对象", description="物料凭证抬头")
public class MesCertificatePerk implements Serializable {
    private static final long serialVersionUID = 1L;

	/**id*/
	@TableId(type = IdType.ID_WORKER_STR)
    @ApiModelProperty(value = "id")
    private java.lang.String id;
	/**创建人*/
    @ApiModelProperty(value = "创建人")
    private java.lang.String createBy;
	/**创建日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "创建日期")
    private java.util.Date createTime;
	/**更新人*/
    @ApiModelProperty(value = "更新人")
    private java.lang.String updateBy;
	/**更新日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "更新日期")
    private java.util.Date updateTime;
	/**所属部门*/
    @ApiModelProperty(value = "所属部门")
    private java.lang.String sysOrgCode;
	/**凭证日期*/
    @Excel(name = "凭证日期", width = 15, format = "yyyy-MM-dd")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "凭证日期")
    private java.util.Date cerDate;
	/**物料凭证年度*/
    @Excel(name = "物料凭证年度", width = 15)
    @ApiModelProperty(value = "物料凭证年度")
    private java.lang.String cerYear;
	/**录入日期*/
    @Excel(name = "录入日期", width = 15, format = "yyyy-MM-dd")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "录入日期")
    private java.util.Date enrollDate;
	/**过账日期*/
    @Excel(name = "过账日期", width = 15, format = "yyyy-MM-dd")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "过账日期")
    private java.util.Date accountDate;
	/**输入时间*/
    @Excel(name = "输入时间", width = 20, format = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "输入时间")
    private java.util.Date inputTime;
	/**事件编号*/
    @Excel(name = "事件编号", width = 15)
    @ApiModelProperty(value = "事件编号")
    private java.lang.String affairCode;
	/**事件类型*/
    @Excel(name = "事件类型", width = 15)
    @ApiModelProperty(value = "事件类型")
    private java.lang.String affairType;
	/**事件名称*/
    @Excel(name = "事件名称", width = 15)
    @ApiModelProperty(value = "事件名称")
    private java.lang.String affairName;
	/**事件描述*/
    @Excel(name = "事件描述", width = 15)
    @ApiModelProperty(value = "事件描述")
    private java.lang.String affairContent;
	/**采购订单id*/
    @Excel(name = "采购订单id", width = 15)
    @ApiModelProperty(value = "采购订单id")
    private java.lang.String purchaseId;
	/**是否质检完成*/
    @Excel(name = "是否质检完成", width = 15)
    @ApiModelProperty(value = "是否质检完成")
    private java.lang.String ifFinish;
	/**是否入库完成*/
    @Excel(name = "是否入库完成", width = 15)
    @ApiModelProperty(value = "是否入库完成")
    private java.lang.String ifInput;
	/**采购订单编号*/
    @Excel(name = "备用4", width = 15)
    @ApiModelProperty(value = "采购订单编号")
    private java.lang.String query4;
	/**备用5*/
    @Excel(name = "备用5", width = 15)
    @ApiModelProperty(value = "备用5")
    private java.lang.String query5;
	/**备用6*/
    @Excel(name = "备用6", width = 15)
    @ApiModelProperty(value = "备用6")
    private java.lang.String query6;
}
