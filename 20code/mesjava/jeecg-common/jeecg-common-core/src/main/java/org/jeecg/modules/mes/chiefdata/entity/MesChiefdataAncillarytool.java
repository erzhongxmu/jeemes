package org.jeecg.modules.mes.chiefdata.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: 主数据—辅料信息
 * @Author: jeecg-boot
 * @Date:   2020-09-17
 * @Version: V1.0
 */
@Data
@TableName("mes_chiefdata_ancillarytool")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="mes_chiefdata_ancillarytool对象", description="主数据—辅料信息")
public class MesChiefdataAncillarytool implements Serializable {
    private static final long serialVersionUID = 1L;

	/**id*/
	@TableId(type = IdType.ID_WORKER_STR)
    @ApiModelProperty(value = "id")
    private java.lang.String id;
	/**创建人*/
    @ApiModelProperty(value = "创建人")
    private java.lang.String createBy;
	/**创建日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "创建日期")
    private java.util.Date createTime;
	/**更新人*/
    @ApiModelProperty(value = "更新人")
    private java.lang.String updateBy;
	/**更新日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "更新日期")
    private java.util.Date updateTime;
	/**所属部门*/
    @ApiModelProperty(value = "所属部门")
    private java.lang.String sysOrgCode;
	/**新增方式*/
	@Excel(name = "新增方式", width = 15, dicCode = "gain_manner")
	@Dict(dicCode = "gain_manner")
    @ApiModelProperty(value = "新增方式")
    private java.lang.String gainManner;
	/**辅料SN*/
	@Excel(name = "辅料SN", width = 15)
    @ApiModelProperty(value = "辅料SN")
    private java.lang.String ancillarySn;
	/**批次数量*/
	@Excel(name = "批次数量", width = 15)
    @ApiModelProperty(value = "批次数量")
    private java.lang.String batchNum;
	/**辅料料号*/
	@Excel(name = "辅料料号", width = 15)
    @ApiModelProperty(value = "辅料料号")
    private java.lang.String ancillaryCode;
	/**供应商*/
	@Excel(name = "供应商", width = 15)
    @ApiModelProperty(value = "供应商")
    private java.lang.String supplier;
	/**生产批号*/
	@Excel(name = "生产批号", width = 15)
    @ApiModelProperty(value = "生产批号")
    private java.lang.String produceCode;
	/**重量(克)*/
	@Excel(name = "重量(克)", width = 15)
    @ApiModelProperty(value = "重量(克)")
    private java.lang.String weight;
	/**入库单号*/
	@Excel(name = "入库单号", width = 15)
    @ApiModelProperty(value = "入库单号")
    private java.lang.String inputwareCode;
	/**RoHs标志*/
	@Excel(name = "RoHs标志", width = 15, dicCode = "yn")
	@Dict(dicCode = "yn")
    @ApiModelProperty(value = "RoHs标志")
    private java.lang.String rohsToken;
	/**辅料类型*/
	@Excel(name = "辅料类型", width = 15)
    @ApiModelProperty(value = "辅料类型")
    private java.lang.String ancillaryType;
	/**辅料名称*/
	@Excel(name = "辅料名称", width = 15)
    @ApiModelProperty(value = "辅料名称")
    private java.lang.String ancillaryName;
	/**辅料规格*/
	@Excel(name = "辅料规格", width = 15)
    @ApiModelProperty(value = "辅料规格")
    private java.lang.String ancillaryGague;
	/**保质期(天)*/
	@Excel(name = "保质期(天)", width = 15)
    @ApiModelProperty(value = "保质期(天)")
    private java.lang.String shelfLife;
	/**品质到期时间*/
	@Excel(name = "品质到期时间", width = 20, format = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "品质到期时间")
    private java.util.Date dueDate;
	/**过炉时间(分钟)*/
	@Excel(name = "过炉时间(分钟)", width = 15)
    @ApiModelProperty(value = "过炉时间(分钟)")
    private java.lang.String crossStovetime;
	/**回温时长(分钟)*/
	@Excel(name = "回温时长(分钟)", width = 15)
    @ApiModelProperty(value = "回温时长(分钟)")
    private java.lang.String heatTime;
	/**开罐保质期(分钟)*/
	@Excel(name = "开罐保质期(分钟)", width = 15)
    @ApiModelProperty(value = "开罐保质期(分钟)")
    private java.lang.String openShelflife;
	/**回温次数上限*/
	@Excel(name = "回温次数上限", width = 15)
    @ApiModelProperty(value = "回温次数上限")
    private java.lang.String heatNumlimit;
	/**回温衰减(%)*/
	@Excel(name = "回温衰减(%)", width = 15)
    @ApiModelProperty(value = "回温衰减(%)")
    private java.lang.String heatDecline;
	/**搅拌时间(分钟)*/
	@Excel(name = "搅拌时间(分钟)", width = 15)
    @ApiModelProperty(value = "搅拌时间(分钟)")
    private java.lang.String stirTime;
	/**生产日期*/
	@Excel(name = "生产日期", width = 15, format = "yyyy-MM-dd")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "生产日期")
    private java.util.Date produceDate;
	/**回温上限时间(分钟)*/
	@Excel(name = "回温上限时间(分钟)", width = 15)
    @ApiModelProperty(value = "回温上限时间(分钟)")
    private java.lang.String heatTimelimit;
	/**搅拌上限时间(分钟)*/
	@Excel(name = "搅拌上限时间(分钟)", width = 15)
    @ApiModelProperty(value = "搅拌上限时间(分钟)")
    private java.lang.String stirTimelimit;
	/**库位*/
	@Excel(name = "库位", width = 15)
    @ApiModelProperty(value = "库位")
    private java.lang.String wareSite;
	/**备注*/
	@Excel(name = "备注", width = 15)
    @ApiModelProperty(value = "备注")
    private java.lang.String notes;
	/**备用1*/
	@Excel(name = "备用1", width = 15)
    @ApiModelProperty(value = "备用1")
    private java.lang.String query1;
	/**备用2*/
	@Excel(name = "备用2", width = 15)
    @ApiModelProperty(value = "备用2")
    private java.lang.String query2;
	/**备用3*/
	@Excel(name = "备用3", width = 15)
    @ApiModelProperty(value = "备用3")
    private java.lang.String query3;
	/**备用4*/
	@Excel(name = "备用4", width = 15)
    @ApiModelProperty(value = "备用4")
    private java.lang.String query4;
	/**备用5*/
	@Excel(name = "备用5", width = 15)
    @ApiModelProperty(value = "备用5")
    private java.lang.String query5;
	/**备用6*/
	@Excel(name = "备用6", width = 15)
    @ApiModelProperty(value = "备用6")
    private java.lang.String query6;
}
