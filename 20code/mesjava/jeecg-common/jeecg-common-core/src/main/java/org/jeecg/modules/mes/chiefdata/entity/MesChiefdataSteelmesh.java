package org.jeecg.modules.mes.chiefdata.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: 钢网建档
 * @Author: jeecg-boot
 * @Date:   2021-04-19
 * @Version: V1.0
 */
@Data
@TableName("mes_chiefdata_steelmesh")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="mes_chiefdata_steelmesh对象", description="钢网建档")
public class MesChiefdataSteelmesh implements Serializable {
    private static final long serialVersionUID = 1L;

	/**主键*/
	@TableId(type = IdType.ID_WORKER_STR)
    @ApiModelProperty(value = "主键")
    private java.lang.String id;
	/**创建人*/
    @ApiModelProperty(value = "创建人")
    private java.lang.String createBy;
	/**创建日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "创建日期")
    private java.util.Date createTime;
	/**更新人*/
    @ApiModelProperty(value = "更新人")
    private java.lang.String updateBy;
	/**更新日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "更新日期")
    private java.util.Date updateTime;
	/**所属部门*/
    @ApiModelProperty(value = "所属部门")
    private java.lang.String sysOrgCode;
	/**钢网SN*/
	@Excel(name = "钢网SN", width = 15)
    @ApiModelProperty(value = "钢网SN")
    private java.lang.String steelmeshSn;
	/**钢网名称*/
	@Excel(name = "钢网名称", width = 15)
    @ApiModelProperty(value = "钢网名称")
    private java.lang.String steelmeshName;
	/**钢网规格*/
	@Excel(name = "钢网规格", width = 15)
    @ApiModelProperty(value = "钢网规格")
    private java.lang.String steelmeshGague;
	/**钢网类型*/
	@Excel(name = "钢网类型", width = 15)
    @ApiModelProperty(value = "钢网类型")
    private java.lang.String steelmeshType;
	/**使用点数上限*/
	@Excel(name = "使用点数上限", width = 15)
    @ApiModelProperty(value = "使用点数上限")
    private java.lang.String pointNumLimit;
	/**当前使用点数*/
	@Excel(name = "当前使用点数", width = 15)
    @ApiModelProperty(value = "当前使用点数")
    private java.lang.String pointUsageNum;
}
