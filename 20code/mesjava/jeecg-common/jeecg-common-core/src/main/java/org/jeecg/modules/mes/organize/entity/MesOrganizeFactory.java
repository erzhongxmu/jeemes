package org.jeecg.modules.mes.organize.entity;

import java.io.Serializable;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import org.jeecgframework.poi.excel.annotation.Excel;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @Description: 组织—工厂
 * @Author: jeecg-boot
 * @Date:   2020-10-19
 * @Version: V1.0
 */
@Data
@TableName("mes_organize_factory")
@ApiModel(value="mes_organize_factory对象", description="组织—工厂")
public class MesOrganizeFactory implements Serializable {
    private static final long serialVersionUID = 1L;

	/**id*/
	@TableId(type = IdType.ID_WORKER_STR)
    @ApiModelProperty(value = "id")
    private java.lang.String id;
	/**创建人*/
    @ApiModelProperty(value = "创建人")
    private java.lang.String createBy;
	/**创建日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "创建日期")
    private java.util.Date createTime;
	/**更新人*/
    @ApiModelProperty(value = "更新人")
    private java.lang.String updateBy;
	/**更新日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "更新日期")
    private java.util.Date updateTime;
	/**所属部门*/
    @ApiModelProperty(value = "所属部门")
    private java.lang.String sysOrgCode;
	/**工厂编号*/
    @Excel(name = "工厂编号", width = 15)
    @ApiModelProperty(value = "工厂编号")
    private java.lang.String facCode;
	/**名称1*/
    @Excel(name = "名称1", width = 15)
    @ApiModelProperty(value = "名称1")
    private java.lang.String facNameone;
	/**名称2*/
    @Excel(name = "名称2", width = 15)
    @ApiModelProperty(value = "名称2")
    private java.lang.String facNametwo;
	/**语言代码*/
    @Excel(name = "语言代码", width = 15)
    @ApiModelProperty(value = "语言代码")
    private java.lang.String languageCode;
	/**门牌号/街*/
    @Excel(name = "门牌号/街", width = 15)
    @ApiModelProperty(value = "门牌号/街")
    private java.lang.String address;
	/**邮政信箱*/
    @Excel(name = "邮政信箱", width = 15)
    @ApiModelProperty(value = "邮政信箱")
    private java.lang.String postalBox;
	/**邮政编码*/
    @Excel(name = "邮政编码", width = 15)
    @ApiModelProperty(value = "邮政编码")
    private java.lang.String postalCode;
	/**城市*/
    @Excel(name = "城市", width = 15)
    @ApiModelProperty(value = "城市")
    private java.lang.String city;
	/**国家键值*/
    @Excel(name = "国家键值", width = 15)
    @ApiModelProperty(value = "国家键值")
    private java.lang.String countryKey;
	/**地区*/
    @Excel(name = "地区", width = 15)
    @ApiModelProperty(value = "地区")
    private java.lang.String region;
	/**县代码*/
    @Excel(name = "县代码", width = 15)
    @ApiModelProperty(value = "县代码")
    private java.lang.String countyCode;
	/**城市代码*/
    @Excel(name = "城市代码", width = 15)
    @ApiModelProperty(value = "城市代码")
    private java.lang.String cityCode;
	/**地区税务代码*/
    @Excel(name = "地区税务代码", width = 15)
    @ApiModelProperty(value = "地区税务代码")
    private java.lang.String regionTaxCode;
	/**日历id*/
    @Excel(name = "日历id", width = 15)
    @ApiModelProperty(value = "日历id")
    private java.lang.String calId;
	/**工厂日历*/
    @Excel(name = "工厂日历", width = 15)
    @ApiModelProperty(value = "工厂日历")
    private java.lang.String facCalendar;
	/**备用1*/
    @Excel(name = "备用1", width = 15)
    @ApiModelProperty(value = "备用1")
    private java.lang.String query1;
	/**备用2*/
    @Excel(name = "备用2", width = 15)
    @ApiModelProperty(value = "备用2")
    private java.lang.String query2;
	/**备用3*/
    @Excel(name = "备用3", width = 15)
    @ApiModelProperty(value = "备用3")
    private java.lang.String query3;
	/**备用4*/
    @Excel(name = "备用4", width = 15)
    @ApiModelProperty(value = "备用4")
    private java.lang.String query4;
	/**备用5*/
    @Excel(name = "备用5", width = 15)
    @ApiModelProperty(value = "备用5")
    private java.lang.String query5;
	/**备用6*/
    @Excel(name = "备用6", width = 15)
    @ApiModelProperty(value = "备用6")
    private java.lang.String query6;
}
