package org.jeecg.modules.mes.organize.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: 组织—销售
 * @Author: jeecg-boot
 * @Date:   2020-10-15
 * @Version: V1.0
 */
@Data
@TableName("mes_organize_sale")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="mes_organize_sale对象", description="组织—销售")
public class MesOrganizeSale implements Serializable {
    private static final long serialVersionUID = 1L;

	/**id*/
	@TableId(type = IdType.ID_WORKER_STR)
    @ApiModelProperty(value = "id")
    private java.lang.String id;
	/**创建人*/
    @ApiModelProperty(value = "创建人")
    private java.lang.String createBy;
	/**创建日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "创建日期")
    private java.util.Date createTime;
	/**更新人*/
    @ApiModelProperty(value = "更新人")
    private java.lang.String updateBy;
	/**更新日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "更新日期")
    private java.util.Date updateTime;
	/**所属部门*/
    @ApiModelProperty(value = "所属部门")
    private java.lang.String sysOrgCode;
	/**销售组织编号*/
	@Excel(name = "销售组织编号", width = 15)
    @ApiModelProperty(value = "销售组织编号")
    private java.lang.String saleCode;
	/**销售组织名称*/
	@Excel(name = "销售组织名称", width = 15)
    @ApiModelProperty(value = "销售组织名称")
    private java.lang.String saleName;
	/**地址文本名称*/
	@Excel(name = "地址文本名称", width = 15)
    @ApiModelProperty(value = "地址文本名称")
    private java.lang.String address;
	/**统计货币*/
	@Excel(name = "统计货币", width = 15, dicCode = "current_money")
	@Dict(dicCode = "current_money")
    @ApiModelProperty(value = "统计货币")
    private java.lang.String currency;
	/**日历id*/
	@Excel(name = "日历id", width = 15)
    @ApiModelProperty(value = "日历id")
    private java.lang.String calId;
	/**销售日历*/
	@Excel(name = "销售日历", width = 15)
    @ApiModelProperty(value = "销售日历")
    private java.lang.String saleCalendar;
	/**销售渠道*/
	@Excel(name = "销售渠道", width = 15, dicCode = "sale_channel")
	@Dict(dicCode = "sale_channel")
    @ApiModelProperty(value = "销售渠道")
    private java.lang.String saleChannel;
	/**备用2*/
	@Excel(name = "备用2", width = 15)
    @ApiModelProperty(value = "备用2")
    private java.lang.String query2;
	/**备用3*/
	@Excel(name = "备用3", width = 15)
    @ApiModelProperty(value = "备用3")
    private java.lang.String query3;
	/**备用4*/
	@Excel(name = "备用4", width = 15)
    @ApiModelProperty(value = "备用4")
    private java.lang.String query4;
	/**备用5*/
	@Excel(name = "备用5", width = 15)
    @ApiModelProperty(value = "备用5")
    private java.lang.String query5;
	/**备用6*/
	@Excel(name = "备用6", width = 15)
    @ApiModelProperty(value = "备用6")
    private java.lang.String query6;
}
