package org.jeecg.modules.mes.inspect.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: 宗申测试不合格反修卡
 * @Author: jeecg-boot
 * @Date:   2021-05-20
 * @Version: V1.0
 */
@Data
@TableName("zs_unqualified_repair_card")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="zs_unqualified_repair_card对象", description="宗申测试不合格反修卡")
public class ZsUnqualifiedRepairCard implements Serializable {
    private static final long serialVersionUID = 1L;

	/**主键*/
	@TableId(type = IdType.ID_WORKER_STR)
    @ApiModelProperty(value = "主键")
    private java.lang.String id;
	/**创建人*/
    @ApiModelProperty(value = "创建人")
    private java.lang.String createBy;
	/**创建日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "创建日期")
    private java.util.Date createTime;
	/**更新人*/
    @ApiModelProperty(value = "更新人")
    private java.lang.String updateBy;
	/**更新日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "更新日期")
    private java.util.Date updateTime;
	/**所属部门*/
    @ApiModelProperty(value = "所属部门")
    private java.lang.String sysOrgCode;
	/**客户编码*/
	@Excel(name = "客户编码", width = 15)
    @ApiModelProperty(value = "客户编码")
    private java.lang.String customerCode;
	/**集研编码*/
	@Excel(name = "集研编码", width = 15)
    @ApiModelProperty(value = "集研编码")
    private java.lang.String researchCode;
	/**测试工位*/
	@Excel(name = "测试工位", width = 15, dicCode = "test_station")
	@Dict(dicCode = "test_station")
    @ApiModelProperty(value = "测试工位")
    private java.lang.String testStation;
	/**发生日期*/
	@Excel(name = "发生日期", width = 20, format = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "发生日期")
    private java.util.Date happenDate;
	/**不合格现象*/
	@Excel(name = "不合格现象", width = 15)
    @ApiModelProperty(value = "不合格现象")
    private java.lang.String unqualifiedPhenomenon;
	/**测试人员*/
	@Excel(name = "测试人员", width = 15)
    @ApiModelProperty(value = "测试人员")
    private java.lang.String firstTestPersonnel;
	/**不合格原因*/
	@Excel(name = "不合格原因", width = 15)
    @ApiModelProperty(value = "不合格原因")
    private java.lang.String unqualifiedReason;
	/**维修方案*/
	@Excel(name = "维修方案", width = 15)
    @ApiModelProperty(value = "维修方案")
    private java.lang.String repairProgramme;
	/**维修人员*/
	@Excel(name = "维修人员", width = 15)
    @ApiModelProperty(value = "维修人员")
    private java.lang.String repairPersonnel;
	/**维修完成后在测试结果*/
	@Excel(name = "维修完成后在测试结果", width = 15)
    @ApiModelProperty(value = "维修完成后在测试结果")
    private java.lang.String retestResults;
	/**测试人员*/
	@Excel(name = "测试人员", width = 15)
    @ApiModelProperty(value = "测试人员")
    private java.lang.String retestPersonnel;
	/**对应条码(S)*/
	@Excel(name = "对应条码(S)", width = 15)
    @ApiModelProperty(value = "对应条码(S)")
    private java.lang.String correspondingBarcode;
	/**确认人员*/
	@Excel(name = "确认人员", width = 15)
    @ApiModelProperty(value = "确认人员")
    private java.lang.String confirmationPersonnel;
	/**备用1*/
	@Excel(name = "备用1", width = 15)
    @ApiModelProperty(value = "备用1")
    private java.lang.String query1;
	/**备用2*/
	@Excel(name = "备用2", width = 15)
    @ApiModelProperty(value = "备用2")
    private java.lang.String query2;
	/**备用3*/
	@Excel(name = "备用3", width = 15)
    @ApiModelProperty(value = "备用3")
    private java.lang.String query3;
	/**备用4*/
	@Excel(name = "备用4", width = 15)
    @ApiModelProperty(value = "备用4")
    private java.lang.String query4;
	/**备用5*/
	@Excel(name = "备用5", width = 15)
    @ApiModelProperty(value = "备用5")
    private java.lang.String query5;
	/**状态*/
	@Excel(name = "状态", width = 15, dicCode = "unqualified_repair_card")
	@Dict(dicCode = "unqualified_repair_card")
    @ApiModelProperty(value = "状态")
    private java.lang.String status;
	/**制令单号*/
	@Excel(name = "制令单号", width = 15)
    @ApiModelProperty(value = "制令单号")
    private java.lang.String commandbillCode;
	/**生产订单ID*/
	@Excel(name = "生产订单ID", width = 15)
    @ApiModelProperty(value = "生产订单ID")
    private java.lang.String produceId;
	/**类型*/
	@Excel(name = "类型", width = 15, dicCode = "repair_type")
	@Dict(dicCode = "repair_type")
    @ApiModelProperty(value = "类型")
    private java.lang.String type;
	/**班别*/
	@Excel(name = "班别", width = 15)
    @ApiModelProperty(value = "班别")
    private java.lang.String classType;
	/**线别*/
	@Excel(name = "线别", width = 15, dicCode = "mgroup_code")
	@Dict(dicCode = "mgroup_code")
    @ApiModelProperty(value = "线别")
    private java.lang.String lineType;
	/**站别*/
	@Excel(name = "站别", width = 15)
    @ApiModelProperty(value = "站别")
    private java.lang.String standType;
	/**机型*/
	@Excel(name = "机型", width = 15)
    @ApiModelProperty(value = "机型")
    private java.lang.String model;
	/**不良数量*/
	@Excel(name = "不良数量", width = 15)
    @ApiModelProperty(value = "不良数量")
    private java.lang.Integer badQuantity;
}
