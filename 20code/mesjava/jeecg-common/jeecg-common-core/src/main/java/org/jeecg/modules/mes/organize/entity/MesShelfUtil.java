package org.jeecg.modules.mes.organize.entity;

import lombok.extern.slf4j.Slf4j;
import org.jeecg.common.util.RestUtil;
import org.jeecg.modules.mes.print.conHead;
import org.jeecg.modules.mes.storage.entity.MesShelfLight;

@Slf4j
public class MesShelfUtil {
    public static String litShelfLight(String htserver,
                                     MesShelfLight mesShelfLight) {
        String hturl = "";
        String res = RestUtil.PostForEntity(htserver, mesShelfLight);
        hturl = res.toString();
        return hturl;

    }
}
