package org.jeecg.modules.mes.inspect.service.impl;

import org.jeecg.modules.mes.inspect.entity.MesFirstInspect;
import org.jeecg.modules.mes.inspect.mapper.MesFirstInspectMapper;
import org.jeecg.modules.mes.inspect.service.IMesFirstInspectService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 质检中心-首件检测
 * @Author: jeecg-boot
 * @Date:   2020-10-15
 * @Version: V1.0
 */
@Service
public class MesFirstInspectServiceImpl extends ServiceImpl<MesFirstInspectMapper, MesFirstInspect> implements IMesFirstInspectService {

}
