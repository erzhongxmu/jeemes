package org.jeecg.modules.mes.inspect.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.mes.inspect.entity.MesAlertInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 质检中心-报警信息
 * @Author: jeecg-boot
 * @Date:   2020-10-15
 * @Version: V1.0
 */
public interface MesAlertInfoMapper extends BaseMapper<MesAlertInfo> {

}
