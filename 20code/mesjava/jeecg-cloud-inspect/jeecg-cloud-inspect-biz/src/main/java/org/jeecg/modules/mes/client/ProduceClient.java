package org.jeecg.modules.mes.client;

import org.jeecg.common.api.vo.Result;
import org.jeecg.common.constant.ServiceNameConstants;
import org.jeecg.modules.mes.produce.entity.MesCommandbillInfo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

@Component
@FeignClient(contextId = "InstructionServiceClient", value = ServiceNameConstants.PRODUCE_SERVICE)
public interface ProduceClient {

    @GetMapping("produce/mesCommandbillInfo/getById")
    public MesCommandbillInfo getById(@RequestParam(name="id",required=true) String id);

    @GetMapping(value = "produce/mesCommandbillInfo/queryByCode")
    public MesCommandbillInfo queryByCode(@RequestParam(name="code",required=true) String code );

    @PutMapping(value = "produce/mesCommandbillInfo/editCommand")
    public boolean editCommand(@RequestBody MesCommandbillInfo mesCommandbillInfo);
}
