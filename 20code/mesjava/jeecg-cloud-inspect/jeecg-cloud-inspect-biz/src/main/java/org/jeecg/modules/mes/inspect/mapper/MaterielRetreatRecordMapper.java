package org.jeecg.modules.mes.inspect.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.mes.inspect.entity.MaterielRetreatRecord;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 物料退料记录表
 * @Author: jeecg-boot
 * @Date:   2021-04-25
 * @Version: V1.0
 */
public interface MaterielRetreatRecordMapper extends BaseMapper<MaterielRetreatRecord> {

}
