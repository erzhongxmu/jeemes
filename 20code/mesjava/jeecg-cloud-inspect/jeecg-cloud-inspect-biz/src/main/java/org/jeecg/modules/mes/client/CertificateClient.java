package org.jeecg.modules.mes.client;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.jeecg.common.constant.ServiceNameConstants;
import org.jeecg.modules.mes.storage.entity.MesCertificateItem;
import org.jeecg.modules.mes.storage.entity.MesCertificatePerk;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;

@Component
@FeignClient(contextId = "CertificateServiceClient", value = ServiceNameConstants.WAREHOUSE_SERVICE)
public interface CertificateClient {

    @GetMapping(value = "storage/mesCertificatePerk/getCertificateById")
    public MesCertificatePerk getCertificateById(@RequestParam(name="id",required=true) String id);

    @PutMapping(value = "storage/mesCertificatePerk/editCertificate")
    public String editCertificate(@RequestBody MesCertificatePerk mesCertificatePerk);

    @GetMapping("storage/mesCertificatePerk/queryCertificateItemById")
    public MesCertificateItem queryCertificateItemById(@RequestParam(name="id",required=true) String id);

    @PutMapping("storage/mesCertificatePerk/editCertificateItem")
    public String editCertificateItem(@RequestBody MesCertificateItem mesCertificateItem);

    @PostMapping("storage/mesCertificatePerk/queryCertificateItemByWrapper")
    public MesCertificateItem queryCertificateItemByWrapper(@RequestBody QueryWrapper<MesCertificateItem> w1);



}
