package org.jeecg.modules.mes.inspect.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.mes.inspect.entity.PatrolInspectTemplate;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 巡检模板表
 * @Author: jeecg-boot
 * @Date:   2021-03-20
 * @Version: V1.0
 */
public interface PatrolInspectTemplateMapper extends BaseMapper<PatrolInspectTemplate> {

}
