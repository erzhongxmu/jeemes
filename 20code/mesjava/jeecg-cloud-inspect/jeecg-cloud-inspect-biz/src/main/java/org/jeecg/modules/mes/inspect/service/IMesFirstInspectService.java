package org.jeecg.modules.mes.inspect.service;

import org.jeecg.modules.mes.inspect.entity.MesFirstInspect;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 质检中心-首件检测
 * @Author: jeecg-boot
 * @Date:   2020-10-15
 * @Version: V1.0
 */
public interface IMesFirstInspectService extends IService<MesFirstInspect> {

}
