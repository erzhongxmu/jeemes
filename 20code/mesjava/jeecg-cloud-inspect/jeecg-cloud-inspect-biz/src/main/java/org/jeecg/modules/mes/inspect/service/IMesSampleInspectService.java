package org.jeecg.modules.mes.inspect.service;

import org.jeecg.modules.mes.inspect.entity.MesSampleInspect;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 品质报表—抽检
 * @Author: jeecg-boot
 * @Date:   2021-01-20
 * @Version: V1.0
 */
public interface IMesSampleInspectService extends IService<MesSampleInspect> {

}
