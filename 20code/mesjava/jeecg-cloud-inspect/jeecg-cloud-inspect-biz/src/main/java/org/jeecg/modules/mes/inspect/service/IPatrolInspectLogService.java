package org.jeecg.modules.mes.inspect.service;

import org.jeecg.modules.mes.inspect.entity.PatrolInspectLog;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 巡检模块记录
 * @Author: jeecg-boot
 * @Date:   2021-03-21
 * @Version: V1.0
 */
public interface IPatrolInspectLogService extends IService<PatrolInspectLog> {

}
