package org.jeecg.modules.mes.inspect.service;

import org.jeecg.modules.mes.inspect.entity.MesBodLog;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 出货不良记录表
 * @Author: jeecg-boot
 * @Date:   2021-03-21
 * @Version: V1.0
 */
public interface IMesBodLogService extends IService<MesBodLog> {

}
