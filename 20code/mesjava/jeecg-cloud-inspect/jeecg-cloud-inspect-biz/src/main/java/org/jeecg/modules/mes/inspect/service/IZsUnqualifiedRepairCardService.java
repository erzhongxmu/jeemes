package org.jeecg.modules.mes.inspect.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.jeecg.common.api.vo.Result;
import org.jeecg.modules.mes.inspect.entity.ZsUnqualifiedRepairCard;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 宗申测试不合格反修卡
 * @Author: jeecg-boot
 * @Date:   2021-05-15
 * @Version: V1.0
 */
public interface IZsUnqualifiedRepairCardService extends IService<ZsUnqualifiedRepairCard> {

    Result<?> findUnqualifiedRepairByStatus(String status, Page<ZsUnqualifiedRepairCard> page);

    Result<?> findUnqualifiedRepairByStatusBarcode(String status, String barcode);

    Result<?> queryPageList(Page<ZsUnqualifiedRepairCard> page, QueryWrapper<ZsUnqualifiedRepairCard> queryWrapper);

    Result<?> queryById(String id);

    Result<?> add(ZsUnqualifiedRepairCard zsUnqualifiedRepairCard);

    Result<?> edit(ZsUnqualifiedRepairCard zsUnqualifiedRepairCard);

    Result<?> delete(String id);

    Result<?> deleteBatch(String ids);
}
