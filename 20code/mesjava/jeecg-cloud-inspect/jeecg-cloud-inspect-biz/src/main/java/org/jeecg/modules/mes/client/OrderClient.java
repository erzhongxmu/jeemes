package org.jeecg.modules.mes.client;

import org.jeecg.common.constant.ServiceNameConstants;
import org.jeecg.modules.mes.order.entity.MesOrderPurchase;
import org.jeecg.modules.mes.order.entity.MesPurchaseItem;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

@Component
@FeignClient(contextId = "OrderServiceClient", value = ServiceNameConstants.TRANSACTION_SERVICE)
public interface OrderClient {

    @GetMapping(value = "order/mesOrderPurchase/getPurchaseNameById")
    public MesOrderPurchase getPurchaseNameById(@RequestParam(name="id",required=true) String id);

    @GetMapping(value = "order/mesOrderPurchase/queryMesPurchaseItemById")
    public MesPurchaseItem queryMesPurchaseItemListById(@RequestParam(name="id",required=true) String id);

    /**
     * 远程调用，编辑采购订单子表数据
     * @param mesPurchaseItem
     * @return
     */
    @PutMapping(value = "order/mesOrderPurchase/editPurchaseItem")
    public String editPurchaseItem(@RequestBody MesPurchaseItem mesPurchaseItem);
}
