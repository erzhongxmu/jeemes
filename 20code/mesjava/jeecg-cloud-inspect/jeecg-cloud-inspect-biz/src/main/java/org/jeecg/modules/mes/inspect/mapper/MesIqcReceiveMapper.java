package org.jeecg.modules.mes.inspect.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.mes.inspect.entity.MesIqcReceive;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 品质报表—IQC来料检验
 * @Author: jeecg-boot
 * @Date:   2021-01-20
 * @Version: V1.0
 */
public interface MesIqcReceiveMapper extends BaseMapper<MesIqcReceive> {

}
