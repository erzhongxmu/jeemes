package org.jeecg.modules.mes.iqc.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.mes.iqc.entity.MesIqcMaterial;
import org.jeecg.modules.mes.iqc.service.IMesIqcMaterialService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

 /**
 * @Description: 来料检验物料详情
 * @Author: jeecg-boot
 * @Date:   2021-03-19
 * @Version: V1.0
 */
@Api(tags="来料检验物料详情")
@RestController
@RequestMapping("/iqc/mesIqcMaterial")
@Slf4j
public class MesIqcMaterialController extends JeecgController<MesIqcMaterial, IMesIqcMaterialService> {
	@Autowired
	private IMesIqcMaterialService mesIqcMaterialService;
	
	/**
	 * 分页列表查询
	 *
	 * @param mesIqcMaterial
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "来料检验物料详情-分页列表查询")
	@ApiOperation(value="来料检验物料详情-分页列表查询", notes="来料检验物料详情-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(MesIqcMaterial mesIqcMaterial,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<MesIqcMaterial> queryWrapper = QueryGenerator.initQueryWrapper(mesIqcMaterial, req.getParameterMap());
		Page<MesIqcMaterial> page = new Page<MesIqcMaterial>(pageNo, pageSize);
		IPage<MesIqcMaterial> pageList = mesIqcMaterialService.page(page, queryWrapper);
		return Result.ok(pageList);
	}
	
	/**
	 *   添加
	 *
	 * @param mesIqcMaterial
	 * @return
	 */
	@AutoLog(value = "来料检验物料详情-添加")
	@ApiOperation(value="来料检验物料详情-添加", notes="来料检验物料详情-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody MesIqcMaterial mesIqcMaterial) {
		mesIqcMaterialService.save(mesIqcMaterial);
		return Result.ok("添加成功！");
	}
	
	/**
	 *  编辑
	 *
	 * @param mesIqcMaterial
	 * @return
	 */
	@AutoLog(value = "来料检验物料详情-编辑")
	@ApiOperation(value="来料检验物料详情-编辑", notes="来料检验物料详情-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody MesIqcMaterial mesIqcMaterial) {
		mesIqcMaterialService.updateById(mesIqcMaterial);
		return Result.ok("编辑成功!");
	}
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "来料检验物料详情-通过id删除")
	@ApiOperation(value="来料检验物料详情-通过id删除", notes="来料检验物料详情-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		mesIqcMaterialService.removeById(id);
		return Result.ok("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "来料检验物料详情-批量删除")
	@ApiOperation(value="来料检验物料详情-批量删除", notes="来料检验物料详情-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.mesIqcMaterialService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.ok("批量删除成功!");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "来料检验物料详情-通过id查询")
	@ApiOperation(value="来料检验物料详情-通过id查询", notes="来料检验物料详情-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		MesIqcMaterial mesIqcMaterial = mesIqcMaterialService.getById(id);
		if(mesIqcMaterial==null) {
			return Result.error("未找到对应数据");
		}
		return Result.ok(mesIqcMaterial);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param mesIqcMaterial
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, MesIqcMaterial mesIqcMaterial) {
        return super.exportXls(request, mesIqcMaterial, MesIqcMaterial.class, "来料检验物料详情");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, MesIqcMaterial.class);
    }

}
