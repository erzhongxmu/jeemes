package org.jeecg.modules.mes.inspect.service;

import org.jeecg.modules.mes.inspect.entity.PatrolInspectLog;
import org.jeecg.modules.mes.inspect.entity.PatrolInspectMain;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * @Description: 巡检模块主表
 * @Author: jeecg-boot
 * @Date:   2021-03-21
 * @Version: V1.0
 */
public interface IPatrolInspectMainService extends IService<PatrolInspectMain> {

    public void saveMain(List<PatrolInspectLog> inspectLogS);
}
