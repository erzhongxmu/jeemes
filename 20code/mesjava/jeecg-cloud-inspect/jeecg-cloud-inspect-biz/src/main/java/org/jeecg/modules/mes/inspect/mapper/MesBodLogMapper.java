package org.jeecg.modules.mes.inspect.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.mes.inspect.entity.MesBodLog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 出货不良记录表
 * @Author: jeecg-boot
 * @Date:   2021-03-21
 * @Version: V1.0
 */
public interface MesBodLogMapper extends BaseMapper<MesBodLog> {

}
