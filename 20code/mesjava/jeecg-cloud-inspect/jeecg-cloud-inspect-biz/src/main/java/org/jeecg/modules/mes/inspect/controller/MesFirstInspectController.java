package org.jeecg.modules.mes.inspect.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.aspect.annotation.AutoLog;
import org.jeecg.common.system.base.controller.JeecgController;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.modules.mes.inspect.entity.MesFirstInspect;
import org.jeecg.modules.mes.inspect.service.IMesFirstInspectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;

 /**
 * @Description: 质检中心-首件检测
 * @Author: jeecg-boot
 * @Date:   2020-10-15
 * @Version: V1.0
 */
@Api(tags="质检中心-首件检测")
@RestController
@RequestMapping("/inspect/mesFirstInspect")
@Slf4j
public class MesFirstInspectController extends JeecgController<MesFirstInspect, IMesFirstInspectService> {
	@Autowired
	private IMesFirstInspectService mesFirstInspectService;
	/**
	 * 分页列表查询
	 *
	 * @param mesFirstInspect
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "质检中心-首件检测-分页列表查询")
	@ApiOperation(value="质检中心-首件检测-分页列表查询", notes="质检中心-首件检测-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(MesFirstInspect mesFirstInspect,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<MesFirstInspect> queryWrapper = QueryGenerator.initQueryWrapper(mesFirstInspect, req.getParameterMap());
		Page<MesFirstInspect> page = new Page<MesFirstInspect>(pageNo, pageSize);
		IPage<MesFirstInspect> pageList = mesFirstInspectService.page(page, queryWrapper);
		return Result.ok(pageList);
	}
	
	/**
	 *   添加
	 *
	 * @param mesFirstInspect
	 * @return
	 */
	@AutoLog(value = "质检中心-首件检测-添加")
	@ApiOperation(value="质检中心-首件检测-添加", notes="质检中心-首件检测-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody MesFirstInspect mesFirstInspect) {
		mesFirstInspectService.save(mesFirstInspect);
		return Result.ok("添加成功！");
	}
	
	/**
	 *  编辑
	 *
	 * @param mesFirstInspect
	 * @return
	 */
	@AutoLog(value = "质检中心-首件检测-编辑")
	@ApiOperation(value="质检中心-首件检测-编辑", notes="质检中心-首件检测-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody MesFirstInspect mesFirstInspect) {
		mesFirstInspectService.updateById(mesFirstInspect);
		return Result.ok("编辑成功!");
	}
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "质检中心-首件检测-通过id删除")
	@ApiOperation(value="质检中心-首件检测-通过id删除", notes="质检中心-首件检测-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		mesFirstInspectService.removeById(id);
		return Result.ok("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "质检中心-首件检测-批量删除")
	@ApiOperation(value="质检中心-首件检测-批量删除", notes="质检中心-首件检测-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.mesFirstInspectService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.ok("批量删除成功!");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "质检中心-首件检测-通过id查询")
	@ApiOperation(value="质检中心-首件检测-通过id查询", notes="质检中心-首件检测-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		MesFirstInspect mesFirstInspect = mesFirstInspectService.getById(id);
		if(mesFirstInspect==null) {
			return Result.error("未找到对应数据");
		}
		return Result.ok(mesFirstInspect);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param mesFirstInspect
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, MesFirstInspect mesFirstInspect) {
        return super.exportXls(request, mesFirstInspect, MesFirstInspect.class, "质检中心-首件检测");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, MesFirstInspect.class);
    }

}
