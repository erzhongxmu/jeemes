package org.jeecg.modules.mes.inspect.mapper;


import org.jeecg.modules.mes.inspect.entity.ZsUnqualifiedRepairCard;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 宗申测试不合格反修卡
 * @Author: jeecg-boot
 * @Date:   2021-05-15
 * @Version: V1.0
 */
public interface ZsUnqualifiedRepairCardMapper extends BaseMapper<ZsUnqualifiedRepairCard> {

}
