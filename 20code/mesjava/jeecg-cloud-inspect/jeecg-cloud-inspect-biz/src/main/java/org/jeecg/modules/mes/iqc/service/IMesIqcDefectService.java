package org.jeecg.modules.mes.iqc.service;

import org.jeecg.modules.mes.iqc.entity.MesIqcDefect;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 来料检验缺陷描述
 * @Author: jeecg-boot
 * @Date:   2021-03-19
 * @Version: V1.0
 */
public interface IMesIqcDefectService extends IService<MesIqcDefect> {

}
