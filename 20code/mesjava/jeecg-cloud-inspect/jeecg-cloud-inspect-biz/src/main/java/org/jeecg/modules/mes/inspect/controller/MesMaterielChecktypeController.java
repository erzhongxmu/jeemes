package org.jeecg.modules.mes.inspect.controller;

import org.jeecg.common.system.query.QueryGenerator;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;
import org.jeecg.common.system.base.controller.JeecgController;
import org.jeecg.common.api.vo.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.web.servlet.ModelAndView;
import java.util.Arrays;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.mes.inspect.entity.MesCheckprojectType;
import org.jeecg.modules.mes.inspect.entity.MesMaterielChecktype;
import org.jeecg.modules.mes.inspect.service.IMesMaterielChecktypeService;
import org.jeecg.modules.mes.inspect.service.IMesCheckprojectTypeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;
import org.apache.shiro.SecurityUtils;
import org.jeecg.common.system.vo.LoginUser;
import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

 /**
 * @Description: 质检中心-物料检测类型
 * @Author: jeecg-boot
 * @Date:   2020-10-15
 * @Version: V1.0
 */
@Api(tags="质检中心-物料检测类型")
@RestController
@RequestMapping("/inspect/mesMaterielChecktype")
@Slf4j
public class MesMaterielChecktypeController extends JeecgController<MesMaterielChecktype, IMesMaterielChecktypeService> {

	@Autowired
	private IMesMaterielChecktypeService mesMaterielChecktypeService;

	@Autowired
	private IMesCheckprojectTypeService mesCheckprojectTypeService;


	/*---------------------------------主表处理-begin-------------------------------------*/

	/**
	 * 分页列表查询
	 * @param mesMaterielChecktype
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "质检中心-物料检测类型-分页列表查询")
	@ApiOperation(value="质检中心-物料检测类型-分页列表查询", notes="质检中心-物料检测类型-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(MesMaterielChecktype mesMaterielChecktype,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<MesMaterielChecktype> queryWrapper = QueryGenerator.initQueryWrapper(mesMaterielChecktype, req.getParameterMap());
		Page<MesMaterielChecktype> page = new Page<MesMaterielChecktype>(pageNo, pageSize);
		IPage<MesMaterielChecktype> pageList = mesMaterielChecktypeService.page(page, queryWrapper);
		return Result.ok(pageList);
	}

	/**
     *   添加
     * @param mesMaterielChecktype
     * @return
     */
    @AutoLog(value = "质检中心-物料检测类型-添加")
    @ApiOperation(value="质检中心-物料检测类型-添加", notes="质检中心-物料检测类型-添加")
    @PostMapping(value = "/add")
    public Result<?> add(@RequestBody MesMaterielChecktype mesMaterielChecktype) {
        mesMaterielChecktypeService.save(mesMaterielChecktype);
        return Result.ok("添加成功！");
    }

    /**
     *  编辑
     * @param mesMaterielChecktype
     * @return
     */
    @AutoLog(value = "质检中心-物料检测类型-编辑")
    @ApiOperation(value="质检中心-物料检测类型-编辑", notes="质检中心-物料检测类型-编辑")
    @PutMapping(value = "/edit")
    public Result<?> edit(@RequestBody MesMaterielChecktype mesMaterielChecktype) {
        mesMaterielChecktypeService.updateById(mesMaterielChecktype);
        return Result.ok("编辑成功!");
    }

    /**
     * 通过id删除
     * @param id
     * @return
     */
    @AutoLog(value = "质检中心-物料检测类型-通过id删除")
    @ApiOperation(value="质检中心-物料检测类型-通过id删除", notes="质检中心-物料检测类型-通过id删除")
    @DeleteMapping(value = "/delete")
    public Result<?> delete(@RequestParam(name="id",required=true) String id) {
        mesMaterielChecktypeService.delMain(id);
        return Result.ok("删除成功!");
    }

    /**
     * 批量删除
     * @param ids
     * @return
     */
    @AutoLog(value = "质检中心-物料检测类型-批量删除")
    @ApiOperation(value="质检中心-物料检测类型-批量删除", notes="质检中心-物料检测类型-批量删除")
    @DeleteMapping(value = "/deleteBatch")
    public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
        this.mesMaterielChecktypeService.delBatchMain(Arrays.asList(ids.split(",")));
        return Result.ok("批量删除成功!");
    }

    /**
     * 导出
     * @return
     */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, MesMaterielChecktype mesMaterielChecktype) {
        return super.exportXls(request, mesMaterielChecktype, MesMaterielChecktype.class, "质检中心-物料检测类型");
    }

    /**
     * 导入
     * @return
     */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, MesMaterielChecktype.class);
    }
	/*---------------------------------主表处理-end-------------------------------------*/
	

    /*--------------------------------子表处理-检测类型信息-begin----------------------------------------------*/
	/**
	 * 通过主表ID查询
	 * @return
	 */
	@AutoLog(value = "检测类型信息-通过主表ID查询")
	@ApiOperation(value="检测类型信息-通过主表ID查询", notes="检测类型信息-通过主表ID查询")
	@GetMapping(value = "/listMesCheckprojectTypeByMainId")
    public Result<?> listMesCheckprojectTypeByMainId(MesCheckprojectType mesCheckprojectType,
                                                    @RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo,
                                                    @RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize,
                                                    HttpServletRequest req) {
        QueryWrapper<MesCheckprojectType> queryWrapper = QueryGenerator.initQueryWrapper(mesCheckprojectType, req.getParameterMap());
        Page<MesCheckprojectType> page = new Page<MesCheckprojectType>(pageNo, pageSize);
        IPage<MesCheckprojectType> pageList = mesCheckprojectTypeService.page(page, queryWrapper);
        return Result.ok(pageList);
    }

	/**
	 * 添加
	 * @param mesCheckprojectType
	 * @return
	 */
	@AutoLog(value = "检测类型信息-添加")
	@ApiOperation(value="检测类型信息-添加", notes="检测类型信息-添加")
	@PostMapping(value = "/addMesCheckprojectType")
	public Result<?> addMesCheckprojectType(@RequestBody MesCheckprojectType mesCheckprojectType) {
		mesCheckprojectTypeService.save(mesCheckprojectType);
		return Result.ok("添加成功！");
	}

    /**
	 * 编辑
	 * @param mesCheckprojectType
	 * @return
	 */
	@AutoLog(value = "检测类型信息-编辑")
	@ApiOperation(value="检测类型信息-编辑", notes="检测类型信息-编辑")
	@PutMapping(value = "/editMesCheckprojectType")
	public Result<?> editMesCheckprojectType(@RequestBody MesCheckprojectType mesCheckprojectType) {
		mesCheckprojectTypeService.updateById(mesCheckprojectType);
		return Result.ok("编辑成功!");
	}

	/**
	 * 通过id删除
	 * @param id
	 * @return
	 */
	@AutoLog(value = "检测类型信息-通过id删除")
	@ApiOperation(value="检测类型信息-通过id删除", notes="检测类型信息-通过id删除")
	@DeleteMapping(value = "/deleteMesCheckprojectType")
	public Result<?> deleteMesCheckprojectType(@RequestParam(name="id",required=true) String id) {
		mesCheckprojectTypeService.removeById(id);
		return Result.ok("删除成功!");
	}

	/**
	 * 批量删除
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "检测类型信息-批量删除")
	@ApiOperation(value="检测类型信息-批量删除", notes="检测类型信息-批量删除")
	@DeleteMapping(value = "/deleteBatchMesCheckprojectType")
	public Result<?> deleteBatchMesCheckprojectType(@RequestParam(name="ids",required=true) String ids) {
	    this.mesCheckprojectTypeService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.ok("批量删除成功!");
	}

    /**
     * 导出
     * @return
     */
    @RequestMapping(value = "/exportMesCheckprojectType")
    public ModelAndView exportMesCheckprojectType(HttpServletRequest request, MesCheckprojectType mesCheckprojectType) {
		 // Step.1 组装查询条件
		 QueryWrapper<MesCheckprojectType> queryWrapper = QueryGenerator.initQueryWrapper(mesCheckprojectType, request.getParameterMap());
		 LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();

		 // Step.2 获取导出数据
		 List<MesCheckprojectType> pageList = mesCheckprojectTypeService.list(queryWrapper);
		 List<MesCheckprojectType> exportList = null;

		 // 过滤选中数据
		 String selections = request.getParameter("selections");
		 if (oConvertUtils.isNotEmpty(selections)) {
			 List<String> selectionList = Arrays.asList(selections.split(","));
			 exportList = pageList.stream().filter(item -> selectionList.contains(item.getId())).collect(Collectors.toList());
		 } else {
			 exportList = pageList;
		 }

		 // Step.3 AutoPoi 导出Excel
		 ModelAndView mv = new ModelAndView(new JeecgEntityExcelView());
		 mv.addObject(NormalExcelConstants.FILE_NAME, "检测类型信息"); //此处设置的filename无效 ,前端会重更新设置一下
		 mv.addObject(NormalExcelConstants.CLASS, MesCheckprojectType.class);
		 mv.addObject(NormalExcelConstants.PARAMS, new ExportParams("检测类型信息报表", "导出人:" + sysUser.getRealname(), "检测类型信息"));
		 mv.addObject(NormalExcelConstants.DATA_LIST, exportList);
		 return mv;
    }

    /**
     * 导入
     * @return
     */
    @RequestMapping(value = "/importMesCheckprojectType/{mainId}")
    public Result<?> importMesCheckprojectType(HttpServletRequest request, HttpServletResponse response, @PathVariable("mainId") String mainId) {
		 MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
		 Map<String, MultipartFile> fileMap = multipartRequest.getFileMap();
		 for (Map.Entry<String, MultipartFile> entity : fileMap.entrySet()) {
			 MultipartFile file = entity.getValue();// 获取上传文件对象
			 ImportParams params = new ImportParams();
			 params.setTitleRows(2);
			 params.setHeadRows(1);
			 params.setNeedSave(true);
			 try {
				 List<MesCheckprojectType> list = ExcelImportUtil.importExcel(file.getInputStream(), MesCheckprojectType.class, params);
				 for (MesCheckprojectType temp : list) {
                    temp.setMchecktypeId(mainId);
				 }
				 long start = System.currentTimeMillis();
				 mesCheckprojectTypeService.saveBatch(list);
				 log.info("消耗时间" + (System.currentTimeMillis() - start) + "毫秒");
				 return Result.ok("文件导入成功！数据行数：" + list.size());
			 } catch (Exception e) {
				 log.error(e.getMessage(), e);
				 return Result.error("文件导入失败:" + e.getMessage());
			 } finally {
				 try {
					 file.getInputStream().close();
				 } catch (IOException e) {
					 e.printStackTrace();
				 }
			 }
		 }
		 return Result.error("文件导入失败！");
    }

    /*--------------------------------子表处理-检测类型信息-end----------------------------------------------*/




}
