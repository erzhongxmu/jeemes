package org.jeecg.modules.mes.produce.service;

import org.jeecg.modules.mes.produce.entity.MesCommandbillDlog;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 制令单上料记录
 * @Author: jeecg-boot
 * @Date: 2021-03-04
 * @Version: V1.0
 */
public interface IMesCommandbillDlogService extends IService<MesCommandbillDlog> {

}
