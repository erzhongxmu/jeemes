package org.jeecg.modules.mes.produce.controller;

import java.util.Arrays;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.modules.mes.produce.entity.TurnProduceRecord;
import org.jeecg.modules.mes.produce.service.ITurnProduceRecordService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

 /**
 * @Description: 转产记录表
 * @Author: jeecg-boot
 * @Date:   2021-04-21
 * @Version: V1.0
 */
@Api(tags="转产记录表")
@RestController
@RequestMapping("/produce/turnProduceRecord")
@Slf4j
public class TurnProduceRecordController extends JeecgController<TurnProduceRecord, ITurnProduceRecordService> {
	@Autowired
	private ITurnProduceRecordService turnProduceRecordService;
	
	/**
	 * 分页列表查询
	 *
	 * @param turnProduceRecord
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "转产记录表-分页列表查询")
	@ApiOperation(value="转产记录表-分页列表查询", notes="转产记录表-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(TurnProduceRecord turnProduceRecord,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<TurnProduceRecord> queryWrapper = QueryGenerator.initQueryWrapper(turnProduceRecord, req.getParameterMap());
		Page<TurnProduceRecord> page = new Page<TurnProduceRecord>(pageNo, pageSize);
		IPage<TurnProduceRecord> pageList = turnProduceRecordService.page(page, queryWrapper);
		return Result.ok(pageList);
	}
	
	/**
	 *   添加
	 *
	 * @param turnProduceRecord
	 * @return
	 */
	@AutoLog(value = "转产记录表-添加")
	@ApiOperation(value="转产记录表-添加", notes="转产记录表-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody TurnProduceRecord turnProduceRecord) {
		turnProduceRecordService.save(turnProduceRecord);
		return Result.ok("添加成功！");
	}

	 /**
	  *   远程调用 添加
	  *
	  * @param turnProduceRecord
	  * @return
	  */
	 @PostMapping(value = "/turnProduceRecordadd")
	 public boolean TurnProduceRecordadd(@RequestBody TurnProduceRecord turnProduceRecord) {
		 boolean mark=turnProduceRecordService.save(turnProduceRecord);
		 if(mark) {
			 return true;
		 }else {
			 return false;
		 }
	 }
	
	/**
	 *  编辑
	 *
	 * @param turnProduceRecord
	 * @return
	 */
	@AutoLog(value = "转产记录表-编辑")
	@ApiOperation(value="转产记录表-编辑", notes="转产记录表-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody TurnProduceRecord turnProduceRecord) {
		turnProduceRecordService.updateById(turnProduceRecord);
		return Result.ok("编辑成功!");
	}
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "转产记录表-通过id删除")
	@ApiOperation(value="转产记录表-通过id删除", notes="转产记录表-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		turnProduceRecordService.removeById(id);
		return Result.ok("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "转产记录表-批量删除")
	@ApiOperation(value="转产记录表-批量删除", notes="转产记录表-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.turnProduceRecordService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.ok("批量删除成功!");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "转产记录表-通过id查询")
	@ApiOperation(value="转产记录表-通过id查询", notes="转产记录表-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		TurnProduceRecord turnProduceRecord = turnProduceRecordService.getById(id);
		if(turnProduceRecord==null) {
			return Result.error("未找到对应数据");
		}
		return Result.ok(turnProduceRecord);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param turnProduceRecord
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, TurnProduceRecord turnProduceRecord) {
        return super.exportXls(request, turnProduceRecord, TurnProduceRecord.class, "转产记录表");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, TurnProduceRecord.class);
    }

}
