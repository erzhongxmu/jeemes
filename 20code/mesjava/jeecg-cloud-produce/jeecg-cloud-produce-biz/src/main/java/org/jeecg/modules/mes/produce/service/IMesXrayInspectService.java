package org.jeecg.modules.mes.produce.service;

import org.jeecg.modules.mes.produce.entity.MesXrayBadinfo;
import org.jeecg.modules.mes.produce.entity.MesXrayInspect;
import com.baomidou.mybatisplus.extension.service.IService;
import java.io.Serializable;
import java.util.Collection;
import java.util.List;

/**
 * @Description: 制造中心-X-Ray检测
 * @Author: jeecg-boot
 * @Date:   2020-10-13
 * @Version: V1.0
 */
public interface IMesXrayInspectService extends IService<MesXrayInspect> {

	/**
	 * 添加一对多
	 * 
	 */
	public void saveMain(MesXrayInspect mesXrayInspect,List<MesXrayBadinfo> mesXrayBadinfoList) ;
	
	/**
	 * 修改一对多
	 * 
	 */
	public void updateMain(MesXrayInspect mesXrayInspect,List<MesXrayBadinfo> mesXrayBadinfoList);
	
	/**
	 * 删除一对多
	 */
	public void delMain (String id);
	
	/**
	 * 批量删除一对多
	 */
	public void delBatchMain (Collection<? extends Serializable> idList);
	
}
