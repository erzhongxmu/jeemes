package org.jeecg.modules.mes.produce.service.impl;

import org.jeecg.modules.mes.produce.entity.MesModelDeployinfo;
import org.jeecg.modules.mes.produce.entity.MesModelCheckproject;
import org.jeecg.modules.mes.produce.mapper.MesModelCheckprojectMapper;
import org.jeecg.modules.mes.produce.mapper.MesModelDeployinfoMapper;
import org.jeecg.modules.mes.produce.service.IMesModelDeployinfoService;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import java.io.Serializable;
import java.util.List;
import java.util.Collection;

/**
 * @Description: 模版项目配置-基本信息
 * @Author: jeecg-boot
 * @Date:   2020-10-13
 * @Version: V1.0
 */
@Service
public class MesModelDeployinfoServiceImpl extends ServiceImpl<MesModelDeployinfoMapper, MesModelDeployinfo> implements IMesModelDeployinfoService {

	@Autowired
	private MesModelDeployinfoMapper mesModelDeployinfoMapper;
	@Autowired
	private MesModelCheckprojectMapper mesModelCheckprojectMapper;
	
	@Override
	@Transactional
	public void saveMain(MesModelDeployinfo mesModelDeployinfo, List<MesModelCheckproject> mesModelCheckprojectList) {
		mesModelDeployinfoMapper.insert(mesModelDeployinfo);
		if(mesModelCheckprojectList!=null && mesModelCheckprojectList.size()>0) {
			for(MesModelCheckproject entity:mesModelCheckprojectList) {
				//外键设置
				entity.setModelId(mesModelDeployinfo.getId());
				mesModelCheckprojectMapper.insert(entity);
			}
		}
	}

	@Override
	@Transactional
	public void updateMain(MesModelDeployinfo mesModelDeployinfo,List<MesModelCheckproject> mesModelCheckprojectList) {
		mesModelDeployinfoMapper.updateById(mesModelDeployinfo);
		
		//1.先删除子表数据
		mesModelCheckprojectMapper.deleteByMainId(mesModelDeployinfo.getId());
		
		//2.子表数据重新插入
		if(mesModelCheckprojectList!=null && mesModelCheckprojectList.size()>0) {
			for(MesModelCheckproject entity:mesModelCheckprojectList) {
				//外键设置
				entity.setModelId(mesModelDeployinfo.getId());
				mesModelCheckprojectMapper.insert(entity);
			}
		}
	}

	@Override
	@Transactional
	public void delMain(String id) {
		mesModelCheckprojectMapper.deleteByMainId(id);
		mesModelDeployinfoMapper.deleteById(id);
	}

	@Override
	@Transactional
	public void delBatchMain(Collection<? extends Serializable> idList) {
		for(Serializable id:idList) {
			mesModelCheckprojectMapper.deleteByMainId(id.toString());
			mesModelDeployinfoMapper.deleteById(id);
		}
	}
	
}
