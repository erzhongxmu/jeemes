package org.jeecg.modules.mes.produce.service;

import org.jeecg.modules.mes.produce.entity.FirstPieceDip;
import com.baomidou.mybatisplus.extension.service.IService;
import org.jeecg.modules.mes.produce.entity.FirstPieceSmt;

/**
 * @Description: DIP首件确认表
 * @Author: jeecg-boot
 * @Date:   2021-03-22
 * @Version: V1.0
 */
public interface IFirstPieceDipService extends IService<FirstPieceDip> {
    public void saveMain(FirstPieceDip firstPieceDip) ;
}
