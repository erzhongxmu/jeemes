package org.jeecg.modules.mes.produce.service.impl;

import org.jeecg.modules.mes.produce.entity.MesXrayBadinfo;
import org.jeecg.modules.mes.produce.mapper.MesXrayBadinfoMapper;
import org.jeecg.modules.mes.produce.service.IMesXrayBadinfoService;
import org.springframework.stereotype.Service;
import java.util.List;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @Description: X-Ray检测-不良信息
 * @Author: jeecg-boot
 * @Date:   2020-10-13
 * @Version: V1.0
 */
@Service
public class MesXrayBadinfoServiceImpl extends ServiceImpl<MesXrayBadinfoMapper, MesXrayBadinfo> implements IMesXrayBadinfoService {
	
	@Autowired
	private MesXrayBadinfoMapper mesXrayBadinfoMapper;
	
	@Override
	public List<MesXrayBadinfo> selectByMainId(String mainId) {
		return mesXrayBadinfoMapper.selectByMainId(mainId);
	}
}
