package org.jeecg.modules.mes.produce.mapper;

import java.util.List;
import org.jeecg.modules.mes.produce.entity.MesMechanismCheckproject;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * @Description: 机种项目配置-检查项目
 * @Author: jeecg-boot
 * @Date:   2020-10-13
 * @Version: V1.0
 */
public interface MesMechanismCheckprojectMapper extends BaseMapper<MesMechanismCheckproject> {

	public boolean deleteByMainId(@Param("mainId") String mainId);
    
	public List<MesMechanismCheckproject> selectByMainId(@Param("mainId") String mainId);
}
