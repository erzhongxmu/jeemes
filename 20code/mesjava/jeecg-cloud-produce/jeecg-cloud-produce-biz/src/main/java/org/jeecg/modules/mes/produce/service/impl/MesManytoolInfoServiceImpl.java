package org.jeecg.modules.mes.produce.service.impl;

import org.jeecg.modules.mes.produce.entity.MesManytoolInfo;
import org.jeecg.modules.mes.produce.mapper.MesManytoolInfoMapper;
import org.jeecg.modules.mes.produce.service.IMesManytoolInfoService;
import org.springframework.stereotype.Service;
import java.util.List;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @Description: 多个制具维保-制具信息
 * @Author: jeecg-boot
 * @Date:   2020-10-13
 * @Version: V1.0
 */
@Service
public class MesManytoolInfoServiceImpl extends ServiceImpl<MesManytoolInfoMapper, MesManytoolInfo> implements IMesManytoolInfoService {
	
	@Autowired
	private MesManytoolInfoMapper mesManytoolInfoMapper;
	
	@Override
	public List<MesManytoolInfo> selectByMainId(String mainId) {
		return mesManytoolInfoMapper.selectByMainId(mainId);
	}
}
