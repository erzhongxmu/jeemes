package org.jeecg.modules.mes.produce.service;

import org.jeecg.modules.mes.produce.entity.MesModelCheckproject;
import org.jeecg.modules.mes.produce.entity.MesModelDeployinfo;
import com.baomidou.mybatisplus.extension.service.IService;
import java.io.Serializable;
import java.util.Collection;
import java.util.List;

/**
 * @Description: 模版项目配置-基本信息
 * @Author: jeecg-boot
 * @Date:   2020-10-13
 * @Version: V1.0
 */
public interface IMesModelDeployinfoService extends IService<MesModelDeployinfo> {

	/**
	 * 添加一对多
	 * 
	 */
	public void saveMain(MesModelDeployinfo mesModelDeployinfo,List<MesModelCheckproject> mesModelCheckprojectList) ;
	
	/**
	 * 修改一对多
	 * 
	 */
	public void updateMain(MesModelDeployinfo mesModelDeployinfo,List<MesModelCheckproject> mesModelCheckprojectList);
	
	/**
	 * 删除一对多
	 */
	public void delMain (String id);
	
	/**
	 * 批量删除一对多
	 */
	public void delBatchMain (Collection<? extends Serializable> idList);
	
}
