package org.jeecg.modules.mes.produce.service.impl;

import org.jeecg.modules.mes.produce.entity.MesModelCheckproject;
import org.jeecg.modules.mes.produce.mapper.MesModelCheckprojectMapper;
import org.jeecg.modules.mes.produce.service.IMesModelCheckprojectService;
import org.springframework.stereotype.Service;
import java.util.List;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @Description: 模版项目配置-检查项目
 * @Author: jeecg-boot
 * @Date:   2020-10-13
 * @Version: V1.0
 */
@Service
public class MesModelCheckprojectServiceImpl extends ServiceImpl<MesModelCheckprojectMapper, MesModelCheckproject> implements IMesModelCheckprojectService {
	
	@Autowired
	private MesModelCheckprojectMapper mesModelCheckprojectMapper;
	
	@Override
	public List<MesModelCheckproject> selectByMainId(String mainId) {
		return mesModelCheckprojectMapper.selectByMainId(mainId);
	}
}
