package org.jeecg.modules.mes.produce.service.impl;

import org.jeecg.modules.mes.produce.entity.MesBackupWriteoff;
import org.jeecg.modules.mes.produce.mapper.MesBackupWriteoffMapper;
import org.jeecg.modules.mes.produce.service.IMesBackupWriteoffService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 制造中心-备品报废
 * @Author: jeecg-boot
 * @Date:   2020-10-13
 * @Version: V1.0
 */
@Service
public class MesBackupWriteoffServiceImpl extends ServiceImpl<MesBackupWriteoffMapper, MesBackupWriteoff> implements IMesBackupWriteoffService {

}
