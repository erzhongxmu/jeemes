package org.jeecg.modules.mes.client;

import org.jeecg.common.api.vo.Result;
import org.jeecg.common.constant.ServiceNameConstants;
import org.jeecg.modules.mes.chiefdata.entity.*;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Component
@FeignClient(contextId = "SystemServiceClient", value = ServiceNameConstants.SYSTEM_SERVICE)
public interface SystemClient {

    @GetMapping("mesapi/mesapp/getdocketcode")
    public String getdocketcode(@RequestParam(name = "docketype", required = false) String docketype);

    @GetMapping("mesapi/mesapp/heatFinishRemind")
    public String heatFinishRemind(@RequestParam(name = "userName", required = true) String userName);

    @GetMapping("mesapi/mesapp/sendRemindMessage")
    public String sendRemindMessage(@RequestParam(name = "userName", required = true) String userName,
                                    @RequestParam(name = "commandCode", required = true) String commandCode,
                                    @RequestParam(name = "productName", required = true) String productName);

    @GetMapping("mesapi/mesapp/sendRemindMessagess")
    public String sendCheckMessagess(@RequestParam(name="userName",required=true) String userName,
                                     @RequestParam(name="mName",required=true) String mName);

    @GetMapping("chiefdata/mesPrintModel/queryByPrintType")
    public MesPrintModel queryByPrintType(@RequestParam(name = "printType", required = true) String printType);


    @GetMapping("chiefdata/mesChiefdataMateriel/queryByProductCode")
    public MesChiefdataMateriel queryByProductCode(@RequestParam(name = "pCode") String pCode);

    @GetMapping("chiefdata/mesChiefdataAncillarytool/queryBypCode")
    public MesChiefdataAncillarytool queryBypCode(@RequestParam(name = "pCode") String pCode);

    @GetMapping("chiefdata/mesMaterielRedgum/getBypCode")
    public MesMaterielRedgum getBypCode(@RequestParam(name = "pCode") String pCode);

    @GetMapping("chiefdata/mesChiefdataFeeder/getfeeder")
    public MesChiefdataFeeder getfeederComitem(@RequestParam(name="sn",required=true) String sn);

    /**
     * 远程调用，修改feeder使用次数
     * @param sn 菲达sn
     * @param sjnum 使用次数
     * @return
     */
    @GetMapping(value = "chiefdata/mesChiefdataFeeder/editComitem")
    public boolean editfeederComitem(@RequestParam(name="sn",required=true) String sn,
                                    @RequestParam(name="sjnum",required=true) String sjnum);
    /**
     * 根据物料成品料号和详细物料料号，查询物料详细信息 远程调用
     * @param machinesortCode
     * @param materielCode
     * @return
     */
    @GetMapping(value = "chiefdata/mesChiefdataBom/getsfg")
    public MesChiefdataBomitem getsfg(@RequestParam(name = "machinesortCode", required = true)String machinesortCode,
                                      @RequestParam(name = "materielCode", required = true)String materielCode);

    @GetMapping("chiefdata/mesChiefdataProductline/getproductlineCode")
    public MesChiefdataProductline queryByLineCode(@RequestParam(name="productlineCode",required=true) String productlineCode);

    @GetMapping("chiefdata/mesChiefdataProductline/stopById")
    public Result<?> stopById(@RequestParam(name="id",required=true) String id, @RequestParam(name="flag",required=true) String flag) ;


    @GetMapping(value = "chiefdata/mesChiefdataMateriel/selectByMaterielPcb")
    public List<MesMaterielPcb> selectByMaterielPcb(@RequestParam(name = "materielCode", required = true) String materielCode);


    /**
     * 远程调用，get Steelmesh 根据Steelmesh sn获取
     *
     * @param sn
     * @return
     */
    @GetMapping(value = "chiefdata/mesChiefdataSteelmesh/getSteelmesh")
    public MesChiefdataSteelmesh getSteelmeshComitem(@RequestParam(name="sn",required=true) String sn);
    /**
     * 远程调用，编辑钢网
     *
     * @param mesChiefdataSteelmesh
     * @return
     */
    @PutMapping(value = "chiefdata/mesChiefdataSteelmesh/editSteelmesh")
    public String editSteelmeshComitem(@RequestBody MesChiefdataSteelmesh mesChiefdataSteelmesh);
    /**
     * 远程调用，get Scraper 根据Scraper sn获取
     *
     * @param sn
     * @return
     */
    @GetMapping(value = "chiefdata/mesChiefdataScraper/getScraper")
    public MesChiefdataScraper getScraperComitem(@RequestParam(name="sn",required=true) String sn);
    /**
     * 远程调用，编辑刮刀
     *
     * @param mesChiefdataScraper
     * @return
     */
    @PutMapping(value = "chiefdata/mesChiefdataScraper/editScraper")
    public String editScraperComitem(@RequestBody MesChiefdataScraper mesChiefdataScraper);

    /**
     * 远程调用，根据sn编码，增加使用次数
     *
     * @param steelmeshSn
     * @return
     */
    @GetMapping(value = "chiefdata/mesChiefdataSteelmesh/increaseSteelmeshNum")
    public boolean increaseSteelmeshNum(@RequestParam(name="steelmeshSn",required=true) String steelmeshSn);

    /**
     * 远程调用，根据sn编码，增加使用次数
     *
     * @param scraperSn
     * @return
     */
    @GetMapping(value = "chiefdata/mesChiefdataScraper/increaseScraperNum")
    public boolean increaseScraperNum(@RequestParam(name="scraperSn",required=true) String scraperSn);

    @GetMapping("chiefdata/mesChiefdataBom/getMCodebomitemForIn")
    public MesChiefdataBomitem getMCodebomitemForIn(@RequestParam(name = "materielCode", required = true) String materielCode);

    /**
     *
     * @param zmaterielCode 成品料号
     * @param materielCode 物料料号
     * @param zfstate 是正是反
     * @param fmnum 用量
     * @return
     */
    @GetMapping(value = "chiefdata/mesChiefdataBom/getMCodebomitemForInS")
    public boolean getMCodebomitemForInS(@RequestParam(name = "zmaterielCode", required = true) String zmaterielCode,
                                                     @RequestParam(name = "materielCode", required = true) String materielCode,
                                                     @RequestParam(name = "zfstate", required = true) String zfstate,
                                                     @RequestParam(name = "fmnum", required = true) String fmnum);

    /**
     * 根据主表物料料号查询子表信息
     */
    @GetMapping("chiefdata/mesChiefdataBom/queryByMcodeZu")
    public List<MesChiefdataBomitem> queryByMcodeZu(@RequestParam(name = "Mcode") String Mcode);

    /**
     * 根据成品料号和替代料料号查询 远程调用
     * @param machineSort
     * @param replaceCode
     * @return
     */
    @GetMapping(value = "chiefdata/mesChiefdataReplacematerial/queryReplaceCode")
    public List<MesChiefdataReplacematerial> queryReplaceCode(@RequestParam(name="machineSort",required=true) String machineSort,
                                                              @RequestParam(name="mainCode",required=false) String mainCode,
                                                              @RequestParam(name="replaceCode",required=true) String replaceCode);
    /**
     * 通过SN查询 设备
     */
    @GetMapping(value = "chiefdata/mesChiefdataDevicearchive/queryByDeviceSn")
    public MesChiefdataDevicearchive queryByDeviceSn(@RequestParam(name="deviceSn") String deviceSn);
}


