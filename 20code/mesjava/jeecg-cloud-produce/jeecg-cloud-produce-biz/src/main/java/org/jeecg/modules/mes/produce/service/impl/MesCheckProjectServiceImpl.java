package org.jeecg.modules.mes.produce.service.impl;

import org.jeecg.modules.mes.produce.entity.MesCheckProject;
import org.jeecg.modules.mes.produce.entity.MesInstructHeadman;
import org.jeecg.modules.mes.produce.mapper.MesInstructHeadmanMapper;
import org.jeecg.modules.mes.produce.mapper.MesCheckProjectMapper;
import org.jeecg.modules.mes.produce.service.IMesCheckProjectService;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import java.io.Serializable;
import java.util.List;
import java.util.Collection;

/**
 * @Description: 制造中心-检测项目基本信息
 * @Author: jeecg-boot
 * @Date:   2020-10-13
 * @Version: V1.0
 */
@Service
public class MesCheckProjectServiceImpl extends ServiceImpl<MesCheckProjectMapper, MesCheckProject> implements IMesCheckProjectService {

	@Autowired
	private MesCheckProjectMapper mesCheckProjectMapper;
	@Autowired
	private MesInstructHeadmanMapper mesInstructHeadmanMapper;
	
	@Override
	@Transactional
	public void saveMain(MesCheckProject mesCheckProject, List<MesInstructHeadman> mesInstructHeadmanList) {
		mesCheckProjectMapper.insert(mesCheckProject);
		if(mesInstructHeadmanList!=null && mesInstructHeadmanList.size()>0) {
			for(MesInstructHeadman entity:mesInstructHeadmanList) {
				//外键设置
				entity.setCheckId(mesCheckProject.getId());
				mesInstructHeadmanMapper.insert(entity);
			}
		}
	}

	@Override
	@Transactional
	public void updateMain(MesCheckProject mesCheckProject,List<MesInstructHeadman> mesInstructHeadmanList) {
		mesCheckProjectMapper.updateById(mesCheckProject);
		
		//1.先删除子表数据
		mesInstructHeadmanMapper.deleteByMainId(mesCheckProject.getId());
		
		//2.子表数据重新插入
		if(mesInstructHeadmanList!=null && mesInstructHeadmanList.size()>0) {
			for(MesInstructHeadman entity:mesInstructHeadmanList) {
				//外键设置
				entity.setCheckId(mesCheckProject.getId());
				mesInstructHeadmanMapper.insert(entity);
			}
		}
	}

	@Override
	@Transactional
	public void delMain(String id) {
		mesInstructHeadmanMapper.deleteByMainId(id);
		mesCheckProjectMapper.deleteById(id);
	}

	@Override
	@Transactional
	public void delBatchMain(Collection<? extends Serializable> idList) {
		for(Serializable id:idList) {
			mesInstructHeadmanMapper.deleteByMainId(id.toString());
			mesCheckProjectMapper.deleteById(id);
		}
	}
	
}
