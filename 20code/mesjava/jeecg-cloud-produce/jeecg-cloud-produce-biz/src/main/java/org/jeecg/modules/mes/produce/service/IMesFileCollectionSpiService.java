package org.jeecg.modules.mes.produce.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.jeecg.modules.mes.produce.entity.MesFileCollectionSpi;

import javax.servlet.http.HttpServletRequest;

/**
 * @Description: mes_file_collection_spi
 * @Author: jeecg-boot
 * @Date:   2021-04-16
 * @Version: V1.0
 */
public interface IMesFileCollectionSpiService extends IService<MesFileCollectionSpi> {

    /**
     * 接收spi文件
     * @param request
     * @param line
     */
    void receiveSpiFile(HttpServletRequest request, String line);


}
