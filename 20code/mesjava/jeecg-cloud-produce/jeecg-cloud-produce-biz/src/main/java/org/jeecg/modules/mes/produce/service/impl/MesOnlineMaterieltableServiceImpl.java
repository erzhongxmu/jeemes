package org.jeecg.modules.mes.produce.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.lang3.StringUtils;
import org.jeecg.modules.mes.client.SystemClient;
import org.jeecg.modules.mes.client.TransactionClient;
import org.jeecg.modules.mes.produce.entity.MesCommandbillInfo;
import org.jeecg.modules.mes.produce.entity.MesOnlineMaterieltable;
import org.jeecg.modules.mes.produce.mapper.MesOnlineMaterieltableMapper;
import org.jeecg.modules.mes.produce.service.IMesCommandbillInfoService;
import org.jeecg.modules.mes.produce.service.IMesOnlineMaterieltableService;
import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * @Description: 制造中心-在线料表
 * @Author: jeecg-boot
 * @Date:   2020-10-13
 * @Version: V1.0
 */
@Service
public class MesOnlineMaterieltableServiceImpl extends ServiceImpl<MesOnlineMaterieltableMapper, MesOnlineMaterieltable> implements IMesOnlineMaterieltableService {

    @Autowired
    private TransactionClient transactionClient;
    @Autowired
    private SystemClient systemClient;

    @Autowired
    private MesOnlineMaterieltableMapper mesOnlineMaterieltableMapper;

    @Autowired
    private IMesCommandbillInfoService mesCommandbillInfoService;

    @Override
    public List<MesOnlineMaterieltable> selectByMainId(String mainId) {
        return mesOnlineMaterieltableMapper.selectByMainId(mainId);
    }


    @Transactional
    public boolean importExcel(HttpServletRequest request, HttpServletResponse response) {
        MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
        Map<String, MultipartFile> fileMap = multipartRequest.getFileMap();
        for (Map.Entry<String, MultipartFile> entity : fileMap.entrySet()) {
            MultipartFile file = entity.getValue();// 获取上传文件对象
            ImportParams params = new ImportParams();
            params.setTitleRows(2);
            params.setHeadRows(1);
            params.setNeedSave(true);
            try {
                List<MesOnlineMaterieltable> list = ExcelImportUtil.importExcel(file.getInputStream(),  MesOnlineMaterieltable.class, params);
                for(MesOnlineMaterieltable temp:list){
                    if( StringUtils.isBlank(temp.getQuery5())){
                        temp.setQuery5("正面");
                    }
                    if(StringUtils.isBlank(temp.getProductCode())||
                            StringUtils.isBlank(temp.getMaterielCode())||
                            StringUtils.isBlank(temp.getPointNum())||
                            StringUtils.isBlank(temp.getLineType())){
                        throw new RuntimeException("成品料号、物料料号、点数、线别等不能为空！");

                    }

                    //根据成品料号去生产订单查询，是否有该生产订单；
                    /*List<MesOrderProduce> orderProduceList = transactionClient.getOrderProduceList(temp.getProductCode(), temp.getLineType());
                    if (orderProduceList.size()==0){
                        throw new RuntimeException("未找到该成品料号和线别的制令单！");
                    }*/
                    QueryWrapper<MesCommandbillInfo> queryWrapper = new QueryWrapper();
                    queryWrapper.eq("mechanism_code",temp.getProductCode());
                    queryWrapper.eq("line_type",temp.getLineType());
                    List<MesCommandbillInfo> list1 = mesCommandbillInfoService.list(queryWrapper);
                    if (list1.size()==0){
                        throw new RuntimeException("未找到该成品料号和线别的制令单！");
                    }
                    save(temp);
                }
                //远程调用bom，更新正反面用量
                for(MesOnlineMaterieltable temp:list){
                    //根据成品料号去生产订单查询，是否有该生产订单；
                    if(StringUtils.isNotBlank(temp.getPointNum())&&!"0".equals(temp.getPointNum().trim())){
                        boolean mark=systemClient.getMCodebomitemForInS(temp.getProductCode(),temp.getMaterielCode(),temp.getQuery5(),temp.getPointNum());
                        if (!mark){
                            throw new RuntimeException("未在bom中找到该成品料号和该物料信息，无法同步正反面用量！");
                        }
                    }
                }
                return true;
            } catch (Exception e) {
                log.error(e.getMessage(), e);
                throw new RuntimeException("文件导入失败:" + e.getMessage());
            } finally {
                try {
                    file.getInputStream().close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return false;
    }


}
