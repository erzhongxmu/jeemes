package org.jeecg.modules.mes.produce.service.impl;

import org.jeecg.modules.mes.produce.entity.MesIcburnResume;
import org.jeecg.modules.mes.produce.mapper.MesIcburnResumeMapper;
import org.jeecg.modules.mes.produce.service.IMesIcburnResumeService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 制造中心-IC烧录
 * @Author: jeecg-boot
 * @Date:   2020-10-13
 * @Version: V1.0
 */
@Service
public class MesIcburnResumeServiceImpl extends ServiceImpl<MesIcburnResumeMapper, MesIcburnResume> implements IMesIcburnResumeService {

}
