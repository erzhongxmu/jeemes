package org.jeecg.modules.mes.produce.controller;

import java.util.Arrays;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.modules.mes.produce.entity.BindingRecordInfo;
import org.jeecg.modules.mes.produce.service.IBindingRecordInfoService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecg.modules.mes.produce.service.IMesCommandbillInfoService;
import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

/**
 * @Description: 钢网、刮刀绑定记录
 * @Author: jeecg-boot
 * @Date: 2021-04-19
 * @Version: V1.0
 */
@Api(tags = "钢网、刮刀绑定记录")
@RestController
@RequestMapping("/produce/bindingRecordInfo")
@Slf4j
public class BindingRecordInfoController extends JeecgController<BindingRecordInfo, IBindingRecordInfoService> {
    @Autowired
    private IBindingRecordInfoService bindingRecordInfoService;
    @Autowired
    private IMesCommandbillInfoService mesCommandBillInfoService;

    /**
     * 分页列表查询
     *
     * @param bindingRecordInfo
     * @param pageNo
     * @param pageSize
     * @param req
     * @return
     */
    @AutoLog(value = "钢网、刮刀绑定记录-分页列表查询")
    @ApiOperation(value = "钢网、刮刀绑定记录-分页列表查询", notes = "钢网、刮刀绑定记录-分页列表查询")
    @GetMapping(value = "/list")
    public Result<?> queryPageList(BindingRecordInfo bindingRecordInfo,
                                   @RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo,
                                   @RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize,
                                   HttpServletRequest req) {
        QueryWrapper<BindingRecordInfo> queryWrapper = QueryGenerator.initQueryWrapper(bindingRecordInfo, req.getParameterMap());
        Page<BindingRecordInfo> page = new Page<BindingRecordInfo>(pageNo, pageSize);
        return bindingRecordInfoService.queryPageList(page, queryWrapper);
    }

    /**
     * 添加
     *
     * @param bindingRecordInfo
     * @return
     */
    @AutoLog(value = "钢网、刮刀绑定记录-添加")
    @ApiOperation(value = "钢网、刮刀绑定记录-添加", notes = "钢网、刮刀绑定记录-添加")
    @PostMapping(value = "/add")
    public Result<?> add(@RequestBody BindingRecordInfo bindingRecordInfo) {
        boolean mark = bindingRecordInfoService.stateSave(bindingRecordInfo);
        if (mark) {
            return Result.ok("添加成功！");
        } else {
            mesCommandBillInfoService.recordErrorOperation("/produce/bindingRecordInfo/add", "制令单上料", "钢网、刮刀绑定记录-添加", "添加失败！", bindingRecordInfo.getCommandbillCode());
            return Result.error("添加失败！");
        }
    }

    /**
     * 编辑
     *
     * @param bindingRecordInfo
     * @return
     */
    @AutoLog(value = "钢网、刮刀绑定记录-编辑")
    @ApiOperation(value = "钢网、刮刀绑定记录-编辑", notes = "钢网、刮刀绑定记录-编辑")
    @PutMapping(value = "/edit")
    public Result<?> edit(@RequestBody BindingRecordInfo bindingRecordInfo) {
        bindingRecordInfoService.updateById(bindingRecordInfo);
        return Result.ok("编辑成功!");
    }

    /**
     * 通过id删除
     *
     * @param id
     * @return
     */
    @AutoLog(value = "钢网、刮刀绑定记录-通过id删除")
    @ApiOperation(value = "钢网、刮刀绑定记录-通过id删除", notes = "钢网、刮刀绑定记录-通过id删除")
    @DeleteMapping(value = "/delete")
    public Result<?> delete(@RequestParam(name = "id", required = true) String id) {
        bindingRecordInfoService.removeById(id);
        return Result.ok("删除成功!");
    }

    /**
     * 批量删除
     *
     * @param ids
     * @return
     */
    @AutoLog(value = "钢网、刮刀绑定记录-批量删除")
    @ApiOperation(value = "钢网、刮刀绑定记录-批量删除", notes = "钢网、刮刀绑定记录-批量删除")
    @DeleteMapping(value = "/deleteBatch")
    public Result<?> deleteBatch(@RequestParam(name = "ids", required = true) String ids) {
        this.bindingRecordInfoService.removeByIds(Arrays.asList(ids.split(",")));
        return Result.ok("批量删除成功!");
    }

    /**
     * 通过id查询
     *
     * @param id
     * @return
     */
    @AutoLog(value = "钢网、刮刀绑定记录-通过id查询")
    @ApiOperation(value = "钢网、刮刀绑定记录-通过id查询", notes = "钢网、刮刀绑定记录-通过id查询")
    @GetMapping(value = "/queryById")
    public Result<?> queryById(@RequestParam(name = "id", required = true) String id) {
        BindingRecordInfo bindingRecordInfo = bindingRecordInfoService.getById(id);
        if (bindingRecordInfo == null) {
            return Result.error("未找到对应数据");
        }
        return Result.ok(bindingRecordInfo);
    }

    /**
     * 导出excel
     *
     * @param request
     * @param bindingRecordInfo
     */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, BindingRecordInfo bindingRecordInfo) {
        return super.exportXls(request, bindingRecordInfo, BindingRecordInfo.class, "钢网、刮刀绑定记录");
    }

    /**
     * 通过excel导入数据
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, BindingRecordInfo.class);
    }

}
