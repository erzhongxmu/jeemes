package org.jeecg.modules.mes.produce.service.impl;

import org.jeecg.modules.mes.produce.entity.MesBackupArchive;
import org.jeecg.modules.mes.produce.mapper.MesBackupArchiveMapper;
import org.jeecg.modules.mes.produce.service.IMesBackupArchiveService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 制造中心-备品建档
 * @Author: jeecg-boot
 * @Date:   2020-10-13
 * @Version: V1.0
 */
@Service
public class MesBackupArchiveServiceImpl extends ServiceImpl<MesBackupArchiveMapper, MesBackupArchive> implements IMesBackupArchiveService {

}
