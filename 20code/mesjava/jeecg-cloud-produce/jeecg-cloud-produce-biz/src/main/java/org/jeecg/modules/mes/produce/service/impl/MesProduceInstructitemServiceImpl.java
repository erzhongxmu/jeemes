package org.jeecg.modules.mes.produce.service.impl;

import org.jeecg.modules.mes.produce.entity.MesProduceInstructitem;
import org.jeecg.modules.mes.produce.mapper.MesProduceInstructitemMapper;
import org.jeecg.modules.mes.produce.service.IMesProduceInstructitemService;
import org.springframework.stereotype.Service;
import java.util.List;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @Description: 生产指示单-明细信息
 * @Author: jeecg-boot
 * @Date:   2020-10-19
 * @Version: V1.0
 */
@Service
public class MesProduceInstructitemServiceImpl extends ServiceImpl<MesProduceInstructitemMapper, MesProduceInstructitem> implements IMesProduceInstructitemService {
	
	@Autowired
	private MesProduceInstructitemMapper mesProduceInstructitemMapper;
	
	@Override
	public List<MesProduceInstructitem> selectByMainId(String mainId) {
		return mesProduceInstructitemMapper.selectByMainId(mainId);
	}
}
