package org.jeecg.modules.mes.produce.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.mes.produce.entity.MesProduceInstructinfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 生产指示单-基本信息
 * @Author: jeecg-boot
 * @Date:   2020-10-19
 * @Version: V1.0
 */
public interface MesProduceInstructinfoMapper extends BaseMapper<MesProduceInstructinfo> {

}
