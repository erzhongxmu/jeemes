package org.jeecg.modules.mes.produce.mapper;

import org.jeecg.modules.mes.produce.entity.MesMaintenanceRecordMain;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 维护保养点检记录主表
 * @Author: jeecg-boot
 * @Date:   2021-06-10
 * @Version: V1.0
 */
public interface MesMaintenanceRecordMainMapper extends BaseMapper<MesMaintenanceRecordMain> {

}
