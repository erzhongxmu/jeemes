package org.jeecg.modules.mes.produce.api;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.aspect.annotation.AutoLog;
import org.jeecg.common.system.vo.LoginUser;
import org.jeecg.modules.mes.produce.entity.MesCourseScanLog;
import org.jeecg.modules.mes.produce.service.IMesCourseScanLogService;
import org.jeecg.modules.mes.produce.vo.ScanMaterVo;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

@Api(tags = "生产报表api")
@RestController
@RequestMapping("/produce/reportapi")
@Slf4j
public class ReportApi {

    @Autowired
    private IMesCourseScanLogService mesCourseScanLogService;

    @AutoLog(value = "生产报表api-扫描物料详情表")
    @ApiOperation(value = "生产报表api-扫描物料详情表", notes = "生产报表api-扫描物料详情表")
    @GetMapping(value = "/scanMaterReport")
    public Result<?> scanMaterReport(MesCourseScanLog mesCourseScanLog,
                                     @RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo,
                                     @RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize) {
        QueryWrapper<MesCourseScanLog> w1= new QueryWrapper<>();
        w1.select("id","passage","materiel_code","materiel_name","ware_code","ware_name","ware_site","quantity","unit","commandbill_code","commandbill_id","line_type","storage_id","feeder_sn","pcb_id","create_by","create_time");
        if(StringUtils.isNotEmpty(mesCourseScanLog.getCommandbillId())){
            //制令单id
            w1.eq("commandbill_id",mesCourseScanLog.getCommandbillId());
        }
        if(StringUtils.isNotEmpty(mesCourseScanLog.getStorageId())){
            //物料二维码
            w1.eq("storage_id",mesCourseScanLog.getStorageId());
        }
        if(StringUtils.isNotEmpty(mesCourseScanLog.getMaterielCode())){
            //物料代码
            w1.eq("materiel_code",mesCourseScanLog.getMaterielCode());
        }
        if(StringUtils.isNotEmpty(mesCourseScanLog.getFeederSn())){
            //飞达序列号
            w1.eq("feeder_sn",mesCourseScanLog.getFeederSn());
        }
        if(StringUtils.isNotEmpty(mesCourseScanLog.getLineType())){
            //产线
            w1.eq("line_type",mesCourseScanLog.getLineType());
        }
        if(StringUtils.isNotEmpty(mesCourseScanLog.getPcbId())){
            //pcb板id
            w1.eq("pcb_id",mesCourseScanLog.getPcbId());
        }
        if(StringUtils.isNotEmpty(mesCourseScanLog.getPassage())){
            //通道号
            w1.eq("passage",mesCourseScanLog.getPassage());
        }
        if(org.apache.commons.lang3.StringUtils.isNotEmpty(mesCourseScanLog.getCreateBy())){
            //开始时间
            w1.ge("create_time",mesCourseScanLog.getCreateBy());
        }
        if(org.apache.commons.lang3.StringUtils.isNotEmpty(mesCourseScanLog.getUpdateBy())){
            //结束时间
            w1.le("create_time",mesCourseScanLog.getUpdateBy());
        }
        w1.orderByDesc("create_time");
        Page<MesCourseScanLog> page = new Page<MesCourseScanLog>(pageNo, pageSize);
        IPage<MesCourseScanLog> pageList = mesCourseScanLogService.page(page, w1);
        return Result.ok(pageList);
    }


    @AutoLog(value = "生产报表api-扫描物料详情表导出")
    @ApiOperation(value = "生产报表api-扫描物料详情表导出", notes = "生产报表api-扫描物料详情表导出")
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, MesCourseScanLog mesCourseScanLog, HttpServletResponse response) {
        // Step.1 组装查询条件查询数据
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        QueryWrapper<MesCourseScanLog> w1= new QueryWrapper<>();
        w1.select("id","passage","materiel_code","materiel_name","ware_code","ware_name","ware_site","quantity","unit","commandbill_code","commandbill_id","line_type","storage_id","feeder_sn","pcb_id","create_by","create_time");
        if(StringUtils.isNotEmpty(mesCourseScanLog.getCommandbillId())){
            //制令单id
            w1.eq("commandbill_id",mesCourseScanLog.getCommandbillId());
        }
        if(StringUtils.isNotEmpty(mesCourseScanLog.getStorageId())){
            //物料二维码
            w1.eq("storage_id",mesCourseScanLog.getStorageId());
        }
        if(StringUtils.isNotEmpty(mesCourseScanLog.getMaterielCode())){
            //物料代码
            w1.eq("materiel_code",mesCourseScanLog.getMaterielCode());
        }
        if(StringUtils.isNotEmpty(mesCourseScanLog.getFeederSn())){
            //飞达序列号
            w1.eq("feeder_sn",mesCourseScanLog.getFeederSn());
        }
        if(StringUtils.isNotEmpty(mesCourseScanLog.getLineType())){
            //产线
            w1.eq("line_type",mesCourseScanLog.getLineType());
        }
        if(StringUtils.isNotEmpty(mesCourseScanLog.getPcbId())){
            //pcb板id
            w1.eq("pcb_id",mesCourseScanLog.getPcbId());
        }
        if(StringUtils.isNotEmpty(mesCourseScanLog.getPassage())){
            //通道号
            w1.eq("passage",mesCourseScanLog.getPassage());
        }
        if(org.apache.commons.lang3.StringUtils.isNotEmpty(mesCourseScanLog.getCreateBy())){
            //开始时间
            w1.ge("create_time",mesCourseScanLog.getCreateBy());
        }
        if(org.apache.commons.lang3.StringUtils.isNotEmpty(mesCourseScanLog.getUpdateBy())){
            //结束时间
            w1.le("create_time",mesCourseScanLog.getUpdateBy());
        }
        w1.orderByDesc("create_time");

        //Step.2 获取导出数据
        List<MesCourseScanLog> queryList = mesCourseScanLogService.list(w1);

        List<ScanMaterVo> exlist = new ArrayList<ScanMaterVo>();
        for(MesCourseScanLog m : queryList){
            ScanMaterVo m1 =new ScanMaterVo();
            BeanUtils.copyProperties(m,m1);
            exlist.add(m1);
        }

        // Step.4 AutoPoi 导出Excel
        ModelAndView mv = new ModelAndView(new JeecgEntityExcelView());
        mv.addObject(NormalExcelConstants.FILE_NAME, "扫描物料详情表");
        mv.addObject(NormalExcelConstants.CLASS, ScanMaterVo.class);
        mv.addObject(NormalExcelConstants.PARAMS, new ExportParams("扫描物料详情表", "导出人:"+sysUser.getRealname(), "扫描物料详情表"));
        mv.addObject(NormalExcelConstants.DATA_LIST, exlist);
        return mv;
    }
}
