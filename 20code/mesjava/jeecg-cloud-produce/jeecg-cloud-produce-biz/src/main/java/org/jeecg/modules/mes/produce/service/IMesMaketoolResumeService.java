package org.jeecg.modules.mes.produce.service;

import org.jeecg.modules.mes.produce.entity.MesMaketoolResume;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 制造中心-制具履历
 * @Author: jeecg-boot
 * @Date:   2020-10-13
 * @Version: V1.0
 */
public interface IMesMaketoolResumeService extends IService<MesMaketoolResume> {

}
