package org.jeecg.modules.mes.produce.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.jeecg.common.system.vo.LoginUser;
import org.apache.shiro.SecurityUtils;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.mes.produce.entity.MesMechanismCheckproject;
import org.jeecg.modules.mes.produce.entity.MesMechanismDeployinfo;
import org.jeecg.modules.mes.produce.vo.MesMechanismDeployinfoPage;
import org.jeecg.modules.mes.produce.service.IMesMechanismDeployinfoService;
import org.jeecg.modules.mes.produce.service.IMesMechanismCheckprojectService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

 /**
 * @Description: 机种项目配置-基本信息
 * @Author: jeecg-boot
 * @Date:   2020-10-13
 * @Version: V1.0
 */
@Api(tags="机种项目配置-基本信息")
@RestController
@RequestMapping("/produce/mesMechanismDeployinfo")
@Slf4j
public class MesMechanismDeployinfoController {
	@Autowired
	private IMesMechanismDeployinfoService mesMechanismDeployinfoService;
	@Autowired
	private IMesMechanismCheckprojectService mesMechanismCheckprojectService;
	
	/**
	 * 分页列表查询
	 *
	 * @param mesMechanismDeployinfo
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "机种项目配置-基本信息-分页列表查询")
	@ApiOperation(value="机种项目配置-基本信息-分页列表查询", notes="机种项目配置-基本信息-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(MesMechanismDeployinfo mesMechanismDeployinfo,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<MesMechanismDeployinfo> queryWrapper = QueryGenerator.initQueryWrapper(mesMechanismDeployinfo, req.getParameterMap());
		Page<MesMechanismDeployinfo> page = new Page<MesMechanismDeployinfo>(pageNo, pageSize);
		IPage<MesMechanismDeployinfo> pageList = mesMechanismDeployinfoService.page(page, queryWrapper);
		return Result.ok(pageList);
	}
	
	/**
	 *   添加
	 *
	 * @param mesMechanismDeployinfoPage
	 * @return
	 */
	@AutoLog(value = "机种项目配置-基本信息-添加")
	@ApiOperation(value="机种项目配置-基本信息-添加", notes="机种项目配置-基本信息-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody MesMechanismDeployinfoPage mesMechanismDeployinfoPage) {
		MesMechanismDeployinfo mesMechanismDeployinfo = new MesMechanismDeployinfo();
		BeanUtils.copyProperties(mesMechanismDeployinfoPage, mesMechanismDeployinfo);
		mesMechanismDeployinfoService.saveMain(mesMechanismDeployinfo, mesMechanismDeployinfoPage.getMesMechanismCheckprojectList());
		return Result.ok("添加成功！");
	}
	
	/**
	 *  编辑
	 *
	 * @param mesMechanismDeployinfoPage
	 * @return
	 */
	@AutoLog(value = "机种项目配置-基本信息-编辑")
	@ApiOperation(value="机种项目配置-基本信息-编辑", notes="机种项目配置-基本信息-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody MesMechanismDeployinfoPage mesMechanismDeployinfoPage) {
		MesMechanismDeployinfo mesMechanismDeployinfo = new MesMechanismDeployinfo();
		BeanUtils.copyProperties(mesMechanismDeployinfoPage, mesMechanismDeployinfo);
		MesMechanismDeployinfo mesMechanismDeployinfoEntity = mesMechanismDeployinfoService.getById(mesMechanismDeployinfo.getId());
		if(mesMechanismDeployinfoEntity==null) {
			return Result.error("未找到对应数据");
		}
		mesMechanismDeployinfoService.updateMain(mesMechanismDeployinfo, mesMechanismDeployinfoPage.getMesMechanismCheckprojectList());
		return Result.ok("编辑成功!");
	}
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "机种项目配置-基本信息-通过id删除")
	@ApiOperation(value="机种项目配置-基本信息-通过id删除", notes="机种项目配置-基本信息-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		mesMechanismDeployinfoService.delMain(id);
		return Result.ok("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "机种项目配置-基本信息-批量删除")
	@ApiOperation(value="机种项目配置-基本信息-批量删除", notes="机种项目配置-基本信息-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.mesMechanismDeployinfoService.delBatchMain(Arrays.asList(ids.split(",")));
		return Result.ok("批量删除成功！");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "机种项目配置-基本信息-通过id查询")
	@ApiOperation(value="机种项目配置-基本信息-通过id查询", notes="机种项目配置-基本信息-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		MesMechanismDeployinfo mesMechanismDeployinfo = mesMechanismDeployinfoService.getById(id);
		if(mesMechanismDeployinfo==null) {
			return Result.error("未找到对应数据");
		}
		return Result.ok(mesMechanismDeployinfo);

	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "机种项目配置-检查项目通过主表ID查询")
	@ApiOperation(value="机种项目配置-检查项目主表ID查询", notes="机种项目配置-检查项目-通主表ID查询")
	@GetMapping(value = "/queryMesMechanismCheckprojectByMainId")
	public Result<?> queryMesMechanismCheckprojectListByMainId(@RequestParam(name="id",required=true) String id) {
		List<MesMechanismCheckproject> mesMechanismCheckprojectList = mesMechanismCheckprojectService.selectByMainId(id);
		return Result.ok(mesMechanismCheckprojectList);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param mesMechanismDeployinfo
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, MesMechanismDeployinfo mesMechanismDeployinfo) {
      // Step.1 组装查询条件查询数据
      QueryWrapper<MesMechanismDeployinfo> queryWrapper = QueryGenerator.initQueryWrapper(mesMechanismDeployinfo, request.getParameterMap());
      LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();

      //Step.2 获取导出数据
      List<MesMechanismDeployinfo> queryList = mesMechanismDeployinfoService.list(queryWrapper);
      // 过滤选中数据
      String selections = request.getParameter("selections");
      List<MesMechanismDeployinfo> mesMechanismDeployinfoList = new ArrayList<MesMechanismDeployinfo>();
      if(oConvertUtils.isEmpty(selections)) {
          mesMechanismDeployinfoList = queryList;
      }else {
          List<String> selectionList = Arrays.asList(selections.split(","));
          mesMechanismDeployinfoList = queryList.stream().filter(item -> selectionList.contains(item.getId())).collect(Collectors.toList());
      }

      // Step.3 组装pageList
      List<MesMechanismDeployinfoPage> pageList = new ArrayList<MesMechanismDeployinfoPage>();
      for (MesMechanismDeployinfo main : mesMechanismDeployinfoList) {
          MesMechanismDeployinfoPage vo = new MesMechanismDeployinfoPage();
          BeanUtils.copyProperties(main, vo);
          List<MesMechanismCheckproject> mesMechanismCheckprojectList = mesMechanismCheckprojectService.selectByMainId(main.getId());
          vo.setMesMechanismCheckprojectList(mesMechanismCheckprojectList);
          pageList.add(vo);
      }

      // Step.4 AutoPoi 导出Excel
      ModelAndView mv = new ModelAndView(new JeecgEntityExcelView());
      mv.addObject(NormalExcelConstants.FILE_NAME, "机种项目配置-基本信息列表");
      mv.addObject(NormalExcelConstants.CLASS, MesMechanismDeployinfoPage.class);
      mv.addObject(NormalExcelConstants.PARAMS, new ExportParams("机种项目配置-基本信息数据", "导出人:"+sysUser.getRealname(), "机种项目配置-基本信息"));
      mv.addObject(NormalExcelConstants.DATA_LIST, pageList);
      return mv;
    }

    /**
    * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
      MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
      Map<String, MultipartFile> fileMap = multipartRequest.getFileMap();
      for (Map.Entry<String, MultipartFile> entity : fileMap.entrySet()) {
          MultipartFile file = entity.getValue();// 获取上传文件对象
          ImportParams params = new ImportParams();
          params.setTitleRows(2);
          params.setHeadRows(1);
          params.setNeedSave(true);
          try {
              List<MesMechanismDeployinfoPage> list = ExcelImportUtil.importExcel(file.getInputStream(), MesMechanismDeployinfoPage.class, params);
              for (MesMechanismDeployinfoPage page : list) {
                  MesMechanismDeployinfo po = new MesMechanismDeployinfo();
                  BeanUtils.copyProperties(page, po);
                  mesMechanismDeployinfoService.saveMain(po, page.getMesMechanismCheckprojectList());
              }
              return Result.ok("文件导入成功！数据行数:" + list.size());
          } catch (Exception e) {
              log.error(e.getMessage(),e);
              return Result.error("文件导入失败:"+e.getMessage());
          } finally {
              try {
                  file.getInputStream().close();
              } catch (IOException e) {
                  e.printStackTrace();
              }
          }
      }
      return Result.ok("文件导入失败！");
    }

}
