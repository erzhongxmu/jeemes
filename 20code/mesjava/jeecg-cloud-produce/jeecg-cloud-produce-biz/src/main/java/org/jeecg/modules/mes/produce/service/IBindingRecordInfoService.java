package org.jeecg.modules.mes.produce.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.jeecg.common.api.vo.Result;
import org.jeecg.modules.mes.produce.entity.BindingRecordInfo;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 钢网、刮刀绑定记录
 * @Author: jeecg-boot
 * @Date:   2021-04-19
 * @Version: V1.0
 */
public interface IBindingRecordInfoService extends IService<BindingRecordInfo> {

    boolean stateSave(BindingRecordInfo recordInfo);

    Result<?> queryPageList(Page<BindingRecordInfo> page, QueryWrapper<BindingRecordInfo> queryWrapper);
}
