package org.jeecg.modules.mes.produce.service.impl;

import org.jeecg.modules.mes.produce.entity.MesInstructHeadman;
import org.jeecg.modules.mes.produce.mapper.MesInstructHeadmanMapper;
import org.jeecg.modules.mes.produce.service.IMesInstructHeadmanService;
import org.springframework.stereotype.Service;
import java.util.List;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @Description: 制造中心-检测项目(指示单项目责任人)
 * @Author: jeecg-boot
 * @Date:   2020-10-13
 * @Version: V1.0
 */
@Service
public class MesInstructHeadmanServiceImpl extends ServiceImpl<MesInstructHeadmanMapper, MesInstructHeadman> implements IMesInstructHeadmanService {
	
	@Autowired
	private MesInstructHeadmanMapper mesInstructHeadmanMapper;
	
	@Override
	public List<MesInstructHeadman> selectByMainId(String mainId) {
		return mesInstructHeadmanMapper.selectByMainId(mainId);
	}
}
