const routes = [{
		path: "/pages/login/login",
		name: 'login',
		meta: {
			title: '登录',
		},
	},
	{
		//注意：path必须跟pages.json中的地址对应，最前面别忘了加'/'哦
		path: '/pages/index/index',
		name: 'index',
		meta: {
			title: '主页',
		},
	},
	{
		path: '/pages/home/home',
		name: 'home',
		meta: {
			title: '首页',
		},
	},
	{
		path: '/pages/home/materiel',
		name: 'materiel',
		meta: {
			title: '物料',
		},
	},
	{
		path: '/pages/home/materielDetail',
		name: 'materielDetail',
		meta: {
			title: '物料详情',
		},
	},
	{
		path: '/pages/home/receive',
		name: 'receive',
		meta: {
			title: '收货',
		},
	},
	{
		path: '/pages/home/receiveDetail',
		name: 'receiveDetail',
		meta: {
			title: '收货详情',
		},
	},
	{
		path: '/pages/home/deviceInformation',
		name: 'deviceInformation',
		meta: {
			title: '设备信息',
		},
	},
	{
		path: '/pages/home/temperatureCurve',
		name: 'temperatureCurve',
		meta: {
			title: '温度曲线',
		},
	},
	{
		path: '/pages/home/temperatureCurveAdd',
		name: 'temperatureCurveAdd',
		meta: {
			title: '新增温度曲线',
		},
	},
	{
		path: '/pages/home/toolsClean',
		name: 'toolsClean',
		meta: {
			title: '制具清洗',
		},
	},
	{
		path: '/pages/home/toolsCleanAdd',
		name: 'toolsCleanAdd',
		meta: {
			title: '新增制具清洗',
		},
	},
	{
		path: '/pages/home/stir',
		name: 'stir',
		meta: {
			title: '搅拌',
		},
	},
	{
		path: '/pages/home/stirAdd',
		name: 'stirAdd',
		meta: {
			title: '新增搅拌',
		},
	},
	{
		path: '/pages/home/rewarming',
		name: 'rewarming',
		meta: {
			title: '辅料回温	',
		},
	},
	{
		path: '/pages/home/rewarmingAdd',
		name: 'rewarmingAdd',
		meta: {
			title: '新增辅料回温',
		},
	},
	{
		path: '/pages/home/maintenancePlan',
		name: 'maintenancePlan',
		meta: {
			title: '保养计划',
		},
	},
	{
		path: '/pages/home/ESOP',
		name: 'ESOP',
		meta: {
			title: 'ESOP',
		},
	},
	{
		path: '/pages/home/webView',
		name: 'webView',
		meta: {
			title: 'webView',
		},
	},
	{
		path: '/pages/home/locking',
		name: 'locking',
		meta: {
			title: '物料锁定',
		},
	},
	{
		path: '/pages/home/receiveDetailSaoMiao',
		name: 'receiveDetailSaoMiao',
		meta: {
			title: '扫描入库',
		},
	},
	{
		path: '/pages/home/inspect',
		name: 'inspect',
		meta: {
			title: '质检',
		},
	},
	{
		path: '/pages/home/inspectDetail',
		name: 'inspectDetail',
		meta: {
			title: '质检项目',
		},
	},
	{
		path: '/pages/home/inspectQuality',
		name: 'inspectQuality',
		meta: {
			title: '质检保存',
		},
	},
	{
		path: '/pages/home/warehouse',
		name: 'warehouse',
		meta: {
			title: '入库',
		},
	},
	{
		path: '/pages/home/warehouseDetail',
		name: 'warehouseDetail',
		meta: {
			title: '入库详情',
		},
	},
	{
		path: '/pages/home/warehouseDetailSaomiao',
		name: 'warehouseDetailSaomiao',
		meta: {
			title: '扫描入库',
		},
	},
	{
		path: '/pages/home/release',
		name: 'release',
		meta: {
			title: '发货',
		},
	},
	{
		path: '/pages/home/releaseDetail',
		name: 'releaseDetail',
		meta: {
			title: '发货子订单',
		},
	},
	{
		path: '/pages/home/releaseDetailSaoMiao',
		name: 'releaseDetailSaoMiao',
		meta: {
			title: '扫描发货',
		},
	},
	{
		path: '/pages/home/sendmateriel',
		name: 'sendmateriel',
		meta: {
			title: '首件发料',
		},
	},
	{
		path: '/pages/home/sendmaterielDetail',
		name: 'sendmaterielDetail',
		meta: {
			title: '首件发料项目',
		},
	},
	{
		path: '/pages/home/sendmaterielDetailSaoMiao',
		name: 'sendmaterielDetailSaoMiao',
		meta: {
			title: '扫描首件发料',
		},
	},
	{
		path: '/pages/home/firstPieceLoading',
		name: 'firstPieceLoading',
		meta: {
			title: '首件上料',
		},
	},
	{
		path: '/pages/home/firstPieceLoadingDetail',
		name: 'firstPieceLoadingDetail',
		meta: {
			title: '首件上料项目',
		},
	},
	{
		path: '/pages/home/firstPieceLoadingIdDetailSaoMiao',
		name: 'firstPieceLoadingIdDetailSaoMiao',
		meta: {
			title: '首件上料项目扫描',
		},
	},
	{
		path: '/pages/home/instructionSheetLoading',
		name: 'instructionSheetLoading',
		meta: {
			title: '制令单上料',
		},
	},
	{
		path: '/pages/home/instructionSheetLoadingDetail',
		name: 'instructionSheetLoadingDetail',
		meta: {
			title: '制令单上料项目',
		},
	},
	{
		path: '/pages/home/instructionSheetLoadingDetailSaoMiao',
		name: 'instructionSheetLoadingDetailSaoMiao',
		meta: {
			title: '制令单扫描上料',
		},
	},
	{
		path: '/pages/home/workOrderMaterialPreparation',
		name: 'workOrderMaterialPreparation',
		meta: {
			title: '工单备料',
		},
	},
	{
		path: '/pages/home/workOrderMaterialPreparationDetail',
		name: 'workOrderMaterialPreparationDetail',
		meta: {
			title: '工单备料项目',
		},
	},
	{
		path: '/pages/home/workOrderMaterialPreparationDetailSaoMiao',
		name: 'workOrderMaterialPreparationDetailSaoMiao',
		meta: {
			title: '工单备料扫描发料',
		},
	},
	{
		path: '/pages/home/SsuingOfInstructionSheet',
		name: 'SsuingOfInstructionSheet',
		meta: {
			title: '制令单发料',
		},
	},
	{
		path: '/pages/home/SsuingOfInstructionSheetDetail',
		name: 'SsuingOfInstructionSheetDetail',
		meta: {
			title: '制令单发料',
		},
	},
	{
		path: '/pages/home/SsuingOfInstructionSheetDetailSaoMiao',
		name: 'SsuingOfInstructionSheetDetailSaoMiao',
		meta: {
			title: '制令单扫描发料',
		},
	},
	{
		path: '/pages/home/instructionReceipt',
		name: 'instructionReceipt',
		meta: {
			title: '制令单入库',
		},
	},
	{
		path: '/pages/home/instructionReceiptSaoMiao',
		name: 'instructionReceiptSaoMiao',
		meta: {
			title: '制令单扫描入库',
		},
	},
	{
		path: '/pages/home/repairAndDelivery',
		name: 'repairAndDelivery',
		meta: {
			title: '返修出库',
		},
	},
	{
		path: '/pages/home/repairAndDeliverySaoMiao',
		name: 'repairAndDeliverySaoMiao',
		meta: {
			title: '扫描返修出库',
		},
	},
	{
		path: '/pages/home/storageOfAuxiliaryMaterials',
		name: 'storageOfAuxiliaryMaterials',
		meta: {
			title: '辅料入库',
		},
	},
	{
		path: '/pages/home/storageOfAuxiliaryMaterialsDetail',
		name: 'storageOfAuxiliaryMaterialsDetail',
		meta: {
			title: '辅料入库详情',
		},
	},
	{
		path: '/pages/home/storageOfAuxiliaryMaterialsDetailSaoMiao',
		name: 'storageOfAuxiliaryMaterialsDetailSaoMiao',
		meta: {
			title: '辅料采购扫描入库',
		},
	},
	{
		path: '/pages/home/toolsWarehousing',
		name: 'toolsWarehousing',
		meta: {
			title: '制具入库',
		},
	},
	{
		path: '/pages/home/toolsWarehousingDetail',
		name: 'toolsWarehousingDetail',
		meta: {
			title: '制具入库详情',
		},
	},
	{
		path: '/pages/home/toolsWarehousingDetailSaoMiao',
		name: 'toolsWarehousingDetailSaoMiao',
		meta: {
			title: '制具采购扫描入库',
		},
	},
	{
		path: '/pages/home/materialList',
		name: 'materialList',
		meta: {
			title: '料表',
		},
	},
	{
		path: '/pages/home/warehousingInspection',
		name: 'warehousingInspection',
		meta: {
			title: '入库检验',
		},
	},
	{
		path: '/pages/home/warehousingInspectionDetail',
		name: 'warehousingInspectionDetail',
		meta: {
			title: '入库检验项目',
		},
	},
	{
		path: '/pages/home/warehousingInspectionQuality',
		name: 'warehousingInspectionQuality',
		meta: {
			title: '入库检验质检',
		},
	},
	{
		path: '/pages/home/returnOfAuxiliaryMaterials',
		name: 'returnOfAuxiliaryMaterials',
		meta: {
			title: '辅料退库',
		},
	},
	{
		path: '/pages/home/returnOfAuxiliaryMaterialsDateil',
		name: 'returnOfAuxiliaryMaterialsDateil',
		meta: {
			title: '辅料领用项目',
		},
	},
	{
		path: '/pages/home/returnOfAuxiliaryMaterialsDetailSaoMiao',
		name: 'returnOfAuxiliaryMaterialsDetailSaoMiao',
		meta: {
			title: '扫描辅料领用项目退料',
		},
	},
	{
		path: '/pages/home/equipmentShutdown',
		name: 'equipmentShutdown',
		meta: {
			title: '设备停机',
		},
	},
	{
		path: '/pages/home/equipmentShutdownAdd',
		name: 'equipmentShutdownAdd',
		meta: {
			title: '添加设备停机',
		},
	},
	{
		path: '/pages/home/invalidTime',
		name: 'invalidTime',
		meta: {
			title: '无效时间',
		},
	},
	{
		path: '/pages/home/invalidTimeAdd',
		name: 'invalidTimeAdd',
		meta: {
			title: '添加无效时间',
		},
	},
	{
		path: '/pages/home/applicationOfAuxiliaryMaterials',
		name: 'applicationOfAuxiliaryMaterials',
		meta: {
			title: '辅料领用',
		},
	},
	{
		path: '/pages/home/applicationOfAuxiliaryMaterialsDetail',
		name: 'applicationOfAuxiliaryMaterialsDetail',
		meta: {
			title: '辅料领用项目',
		},
	},
	{
		path: '/pages/home/applicationOfAuxiliaryMaterialsSaoMiao',
		name: 'applicationOfAuxiliaryMaterialsSaoMiao',
		meta: {
			title: '辅料领用扫描',
		},
	},
	{
		path: '/pages/home/onSiteInspection',
		name: 'onSiteInspection',
		meta: {
			title: '巡检',
		},
	},
	{
		path: '/pages/home/onSiteInspectionQuality',
		name: 'onSiteInspectionQuality',
		meta: {
			title: '巡检项目',
		},
	},
	{
		path: '/pages/home/firstPieceQualityInspection',
		name: 'firstPieceQualityInspection',
		meta: {
			title: '首件质检',
		},
	},
	{
		path: '/pages/home/firstPieceQualityInspectionQuality',
		name: 'firstPieceQualityInspectionQuality',
		meta: {
			title: '首件质检项目',
		},
	},
	{
		path: '/pages/home/firstPieceQualityInspectionDIPQuality',
		name: 'firstPieceQualityInspectionDIPQuality',
		meta: {
			title: '首件质检项目',
		},
	},
	{
		path: '/pages/home/mesCourseScan',
		name: 'mesCourseScan',
		meta: {
			title: '现场扫描',
		},
	},
	{
		path: '/pages/home/processInspection',
		name: 'processInspection',
		meta: {
			title: '',
		},
	},
	{
		path: '/pages/home/processInspectionQuality',
		name: 'processInspectionQuality',
		meta: {
			title: '制程检验项目',
		},
	},
	{
		path: '/pages/home/instructionWithdraw',
		name: 'instructionWithdraw',
		meta: {
			title: '制令单退料',
		},
	},
	{
		path: '/pages/home/changeMaterial',
		name: 'changeMaterial',
		meta: {
			title: '制令单换料',
		},
	},
	{
		path: '/pages/home/instructionWithTurn',
		name: 'instructionWithTurn',
		meta: {
			title: '制令单转线',
		},
	},
	{
		path: '/pages/home/instructionWithdrawDetail',
		name: 'instructionWithdrawDetail',
		meta: {
			title: '制令单退料项目',
		},
	},
	{
		path: '/pages/home/instructionWithdrawDetailSaomiao',
		name: 'instructionWithdrawDetailSaomiao',
		meta: {
			title: '扫描制令单退料',
		},
	},
	{
		path: '/pages/home/maintain',
		name: 'maintain',
		meta: {
			title: 'feeder保养',
		},
	},
	{
		path: '/pages/home/productionLineRestart',
		name: 'productionLineRestart',
		meta: {
			title: '生产线重启',
		},
	},
	{
		path: '/pages/home/chooseCheck',
		name: 'chooseCheck',
		meta: {
			title: '抽检',
		},
	},
	{
		path: '/pages/home/chooseCheckDetail',
		name: 'chooseCheckDetail',
		meta: {
			title: '检验项目',
		},
	},
	{
		path: '/pages/home/chooseCheckDetailQuality',
		name: 'chooseCheckDetailQuality',
		meta: {
			title: '检验保存',
		},
	},
	{
		path: '/pages/msg/message',
		name: 'message',
		meta: {
			title: '消息',
		},
	},
	{
		path: '/pages/user/people',
		name: 'people',
		meta: {
			title: '个人中心',
		},
	},
	{
		path: '/pages/user/userdetail',
		name: 'userdetail',
		meta: {
			title: '个人详情',
		},
	},
	{
		path: '/pages/user/useredit',
		name: 'useredit',
		meta: {
			title: '个人编辑',
		},
	},
	{
		path: '/pages/user/userexit',
		name: 'userexit',
		meta: {
			title: '退出',
		},
	},
	{
		path: '/pages/common/exit',
		name: 'exit',
		meta: {
			title: '退出',
		},
	},
	{
		path: '/pages/common/success',
		name: 'success',
		meta: {
			title: 'success',
		},
	},
	{
		path: '/pages/home/zsFirstPatrol',
		name: 'zsFirstPatrol',
		meta: {
			title: '宗申质检',
		},
	},
	{
		path: '/pages/home/zsPatrol',
		name: 'zsPatrol',
		meta: {
			title: 'SMT质检',
		},
	},
	{
		path: '/pages/home/zsTest',
		name: 'zsTest',
		meta: {
			title: '测试',
		},
	},
	{
		path: '/pages/home/testList',
		name: 'testList',
		meta: {
			title: '测试列表',
		},
	},
	{
		path: '/pages/home/repairReson',
		name: 'repairReson',
		meta: {
			title: '报修原因',
		},
	},
	{
		path: '/pages/home/zrepairList',
		name: 'zsRepair',
		meta: {
			title: '维修',
		},
	},
	{
		path: '/pages/home/repairComplete',
		name: 'repairComplete',
		meta: {
			title: '维修完成',
		},
	},
	// {
	// 	path: '/pages/home/zrepairList',
	// 	name: 'zrepairList',
	// 	meta: {
	// 		title: '维修列表',
	// 	},
	// },
	{
		path: '/pages/home/disqualification',
		name: 'disqualification',
		meta: {
			title: '不合格退料',
		},
	},
	{
		path: '/pages/home/splitBoard',
		name: 'splitBoard',
		meta: {
			title: '拆分板',
		},
	},
	{
		path: '/pages/home/splitMaterial',
		name: 'splitMaterial',
		meta: {
			title: '拆分物料',
		},
	},
	{
		path: '/pages/reports/checkInfo',
		name: 'checkInfo',
		meta: {
			title: '点检',
		},
	},
	{
		path: '/pages/reports/otherEquipmentMaintenance',
		name: 'otherEquipmentMaintenance',
		meta: {
			title: '其它设备维保',
		},
	},
	{
		path: '/pages/reports/printingCheckInfo',
		name: 'printingCheckInfo',
		meta: {
			title: '点检',
		},
	},
	{
		path: '/pages/reports/reflowCheckInfo',
		name: 'reflowCheckInfo',
		meta: {
			title: '点检',
		},
	},
	{
		path: '/pages/home/traceBack',
		name: 'traceBack',
		meta: {
			title: '物料追踪',
		},
	},
	{
		path: '/pages/home/changeMaterial',
		name: 'changeMaterial',
		meta: {
			title: '制令单换料',
		},
	},
	{
		path: '/pages/home/scanningRefueling',
		name: 'scanningRefueling',
		meta: {
			title: '制令单扫描换料',
		},
	}
]
export default routes
